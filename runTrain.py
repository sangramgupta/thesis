################
#
# Deep Flow Prediction - N. Thuerey, K. Weissenov, H. Mehrotra, N. Mainali, L. Prantl, X. Hu (TUM)
# adapted by A. Kumpf (TUM)
# contains code from S. Weiss (TUM)
# Main training script
#
################

import os, sys, random
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torch.optim as optim
import matplotlib.pyplot as plt
import pickle

import argparse
import copy

from DfpNet import TurbNetG, weights_init
from VecLearnNet import UNetFrame, ModifiedUNetFrame
from Unetpp_arch import PreProcessNestedUNet

from models.enhancenet import EnhanceNet

import dataset
import utils
from utils import str2bool
import math
from dotmap import DotMap

from GridNetUtils import GridNetFrame as gnf

from tensorboardX import SummaryWriter
import torchviz

from tqdm import tqdm
from console_progressbar import ProgressBar

import losses


import pdb


######## Settings ########
showDebugInfo = False

torch.set_num_threads(4)

# number of training iterations
iterations = 50000
# batch size
#batch_size = 32
# learning rate, generator
lrG = 0.0006    # 0.0006
lrDiscr = 0.0001
# decay learning rate?
decayLr = True
# learning rate exponential factor
lrExFak = -1 #0.1

batch_normalization=True

# channel exponent to control network size
expo = 5 # 5
# data set config
prop=None # by default, use all from "../data/train"
#prop=[1000,0.75,0,0.25] # mix data from multiple directories
# save txt files with per epoch loss?
saveL1 = False
exportAsPDF = False
exportAsPNG = True
exportFigsToTensorFlowInEachEpoch = True
iterativeTraining = False
fileSequence = [12000, 24000]

# mode can be 'stamped vec', 'stamped scalar', 'noise poly',....
# only 'noise poly' and 'stamped vec' are implemented so far...
modeForTraining = 'pcrender'#'pcrender'#'stamped vec'#'frame' #'stamped vec'# 'noise poly' 'frame', 'pcrender'
#if modeForTraining == 'noise poly':

#dataDir = "/home/kumpf/projects/Neural_networks/physics_unet/data/Temperature_old2/"
#dataDir = "/home/kumpf/projects/Neural_networks/physics_unet/data/Masks_on_10m_wind/"
#dataDir = "/home/kumpf/net/gpusrv01st/FrameData/ECMWF_Testset/"
#dataDir = "/home/kumpf/projects/Neural_networks/physics_unet/data/FrameSkip1/YveT/"
#dataDir = "/met3dDataTmp/FrameSkip1/YveT/"
#dataDir = "/home/kumpf/net/gpusrv01st/Alexander/PCRData/Dragon_10x200_splat_z/"
dataDir = "/home/robotics/masterArbeit/Dataset/Nov7/Train/"
dataDirTest = []
includeSubDirs = False
maxNrFiles = 10
startFileID = 0
useHDF5Files = True
filterByFileName = ""#""Sec1"

useRawInput=True
useGradientOfInput=True
useLinearInterpInput=True
useWarpedInput=False
useRawFeatureMaps=False
useWarpedFeatureMaps=False

useNormals = True
useDepth = True
useSparseSplatsDepth=False
useTargetNormals = False
useTarSplatsNormals = True
useTargetDepth = False
useTarSplatsDepth = True
useLearnDepthMask = False
useLearnDepthMaskSplats = False
useGTMask = False


useSparseSplatsNormal=False
useNormalsScreen =False
useTarNormalsScreen =False
useTarSplatsNormalsScreen =False
useSparseSplatsNormalScreen=False

renderedDataType='shaded'
useColor=True
bSparseSplatsColor=False
useTargetColor=False
useTarSplatsColor = True


# Dataaugmentation and permutation of inputChannels
flipXNow=True
flipYNow=True

loadModel = False
resetLearningRate = False
doLoad     = ""
modelName = "[]"
runNumber = -1
restoreEpoch = -1

gridSizex = 256
gridSizey = 256
centerValidationPatch = True

channelsInNetwork = 10
dropout    = 0.

# Only used for GridNet
netRows = 3
netCols = 4

# For U-Net
partialConv = False
twoConvsPerLev = False


useDataDirAlt = False
readDataOnTheFly = True
figsDirectory = "fig_train_output/"

resultsDir = "results/tensorboard_frame/"

drawLossBorder = True

checkpointEveryNThEpoch = 2

lossBorder = 14
lb = lossBorder


runModelToTest = False
##########################

# parse arguments#
parser = argparse.ArgumentParser(description='Variables which can be specified.')
parser.add_argument('--netModel', type=str, default = 'UNetPP', help='Network Architecture to use. Options: GridNet, GridNetConcat, UNet, UNetPartial, UNetMax, ResUNet, Enhancenet')
parser.add_argument('--partialConv', type=str, default = 'First', help='Do not use this parameter! Values: First, Second, Both, None')
parser.add_argument('--deformConv', type=str, default = 'None', help='Do not use this parameter! Values: First, Second, Both, None')
parser.add_argument('--iterations', type=int, help='number of iterations the network should run for. Default 2500')
parser.add_argument('--batch_size', type=int, default=1, help='batch size for the network. Default 2')
parser.add_argument('--lrG', type=float, help='Learning rate of the generator (Adam optimizer). Default: 0.0006')
parser.add_argument('--lrDiscr', type=float, help='Learning rate of the discriminator (Adam optimizer). Default: 0.00006')
parser.add_argument('--decayLr', type=str, help='Should the learning rate decay? Default: True')
parser.add_argument('--lrStep', type=int, default =1000, help='Only applied when lrExFak == -1 and therefore stepwise decay. Defines every *th epoch the stepsize is multiplied by lrGamma.')
parser.add_argument('--lrGamma', type=float, default = 0.5, help='Only applied when lrExFak == -1 and therefore stepwise decay. Defines every *th epoch the stepsize is multiplied by lrGamma.')
parser.add_argument('--lrExFak', type=float, help='Exponential factor for the learning rate. -1 for taking stepwise learning rate decay.')
parser.add_argument('--channelsInNetwork', type=int, help='Number of channels in first layer of network. Default: 2')
parser.add_argument('--dropoutFactor', type=float, help='Probability for total dropout of residual block. Default: 0')
parser.add_argument('--enableTotalDropoutGridNet', type=str, default='True', help='Enable total dropout for GridNet.')

parser.add_argument('--netRows', type=int, help='Number of rows in network. So far, only used for GridNet. Default: 3')
parser.add_argument('--netCols', type=int, help='Number of columns in network. So far, only used for GridNet. Default: 6')
parser.add_argument('--dilation', type=int, default = 1, help='Dilation for horizontal residual connections. So far, only used for GridNet. Default: 1')
parser.add_argument('--kernelsizew', type=int, default = 3, help='Kernel size (w) for residual connections. So far, only used for GridNet. Default: 3')
parser.add_argument('--kernelsizeh', type=int, default = 3, help='Kernel size (h) for residual connections. So far, only used for GridNet. Default: 3')
parser.add_argument('--kernelsizeUNet', type=str, default = '7,5,3,3,3,3,3', help='Kernel size for U-Net. Can be integer or list of integers. Insert komma separated string.')
parser.add_argument('--padding', type=int, default = 1, help='Padding for resiudal connections. So far, only used for GridNet. Default: 1')

parser.add_argument('--twoConvsPerLev', type=str, default = 'True', help='Should one or 2 conv blocks be used per U-Net level?')


parser.add_argument('--gridSizex', type=int, help='Size of grid in x direction. Default: 256')
parser.add_argument('--gridSizey', type=int, help='Size of grid in y direction. Default: 256')
parser.add_argument('--dataResolutionX', type=int, default=1024, help='Size of input data in x direction.')
parser.add_argument('--dataResolutionY', type=int, default=1024, help='Size of input data in y direction.')
parser.add_argument('--checkDataForErrors', type=str, default = 'False', help='Checks the data in the dataloader, e.g., whether it is divided by 0. Destroys performance.')

parser.add_argument('--centerValidationPatch', type=str, help='Should the validation patch always contain the same region? Default: True')
parser.add_argument('--maxValFiles', type=int, default = 50, help='Maximal number of files used for validation? 0 Means that no validation is performed. Default: 50')



parser.add_argument('--exportAsPDF', type=str, help='Export one validation file (trained vs untrained) as .pdf. Default: False')
parser.add_argument('--exportAsPNG', type=str, help='Export one validation file (trained vs untrained) as .png. Default: False')
parser.add_argument('--exportFigsToTensorFlowInEachEpoch', type=str, help='Export figures to tensorflow in each epoch, even if it is not stored? Default: True')

parser.add_argument('--modeForTraining', type=str, help='Training mode. For different modes, different network architectures are used. For details, look at the code.'
                                                      'Options are "frame" (GridNet), "stamped vec"(UNet), "noise poly" (UNet). Enter name with single quotation marks maybe. Default: frame')
parser.add_argument('--dataDir', type=str, help='Directory for the training dataset. Default: ...')
parser.add_argument('--dataDirTest', type=str, help='Directory for the test dataset. Default: ...')
parser.add_argument('--dataDirAlt', type=str, help='Alternative directory for the dataset (e.g. ram drive). Default: ')
parser.add_argument('--useDataDirAlt', type=str, help='Should the alternative directory be used?. Default: False')
parser.add_argument('--includeSubDirs', type=str, help='Include data sub-directories. Default: True')
parser.add_argument('--maxNrFiles', type=int, help='Maximum number of files to be loaded: 10000')
parser.add_argument('--startFileID', type=int, default = 0, help='used to use a specific range of the files: 0')
parser.add_argument('--useHDF5Files', type=str, help='Option to read from hdf5 instead of .npz. Default: False')
parser.add_argument('--filterByFileName', type=str, help='Filter files by name. Default: ""')
parser.add_argument('--shuffleData', type=int, default = 0, help='Shuffle data before generating validation set. For .npz, this value also defines how often the files are shuffled.')


# Input data arguments
parser.add_argument('--renderedDataType', type=str, help='Normals, depth etc. (no, normal) or new datasets with shadings (shaded)? Then, the keywords are set differently.')
parser.add_argument('--useColor', type=str, help='Use color as input. Only for the new datasets.')
parser.add_argument('--bSparseSplatsColor', type=str, help='Use color of sparse splats as input. Only for the new datasets.')

parser.add_argument('--useTargetColor', type=str, help='Use color as input. Only for the new datasets.')
parser.add_argument('--useTarSplatsColor', type=str, help='Use color of splats as input. Only for the new datasets.')

parser.add_argument('--useRawInput', type=str, help='Use RAW input fields. Default: False')
parser.add_argument('--useGradientOfInput', type=str, help='Use gradient of raw input fields.')
parser.add_argument('--useLinearInterpInput', type=str, help='Use linearly interpolated intermediate level as input.')
parser.add_argument('--useWarpedInput', type=str, help='Use WARPED input fields. Default: False')
parser.add_argument('--useRawFeatureMaps', type=str, help='Use RAW FEATURE fields. Default: False')
parser.add_argument('--useWarpedFeatureMaps', type=str, help='Use WARPED FEATURE fields. Default: False')

parser.add_argument('--useNormals', type=str, help='Use normals of points. ')
parser.add_argument('--useNormalsScreen', type=str, help='Use screen normals of points. ')
parser.add_argument('--useSparseSplatsNormal', type=str, help='Use normals of splats. ')
parser.add_argument('--useSparseSplatsNormalScreen', type=str, help='Use screen normals of splats. ')

parser.add_argument('--useDepth', type=str, help='Use screen depth information of points. ')
parser.add_argument('--useSparseSplatsDepth', type=str,help='Use screen depth information of splats. ')

parser.add_argument('--useTargetNormals', type=str, help='Use normals of points of ground truth. ')
parser.add_argument('--useTarSplatsNormals', type=str, help='Use normals of splats of ground truth. ')

parser.add_argument('--useTarNormalsScreen', type=str, help='Use screen normals of points of ground truth. ')
parser.add_argument('--useTarSplatsNormalsScreen', type=str, help='Use screen normals of splats of ground truth. ')


parser.add_argument('--useTargetDepth', type=str, help='Use screen depth of points of ground truth. ')
parser.add_argument('--useTarSplatsDepth', type=str, help='Use screen depth of splats of ground truth. ')

parser.add_argument('--useLearnDepthMask', type=str, help='Learn which depth values are also there in the ground truth.')
parser.add_argument('--useLearnDepthMaskSplats', type=str, help='Learn which depth values are also there in the ground truth splats.')

parser.add_argument('--useGTMask', type=str, help='Use which depth values are also there in the ground truth. This is cheating, so only use for testing purposes!')




parser.add_argument('--flipXNow', type=str, help='Data augmentation. Allow flip in x direction. Default: True')
parser.add_argument('--flipYNow', type=str, help='Data augmentation. Allow flip in y direction. Default: True')
parser.add_argument('--cropInputBasedOnChannel', type=int, default = 0, help='If >= 0, crops the input field for values >0. Always retains the minimal size needed for training.')
parser.add_argument('--batch_normalization',  type=str, default='True', help='Use batch normalization? Not implemented for all models, e.g. for GridNet')

parser.add_argument('--loadModel', type=str, help='Load existing model and continue training. Default: False')
parser.add_argument('--resetLearningRate', type=str, help='Load learning rate saved in model. Default: False')
parser.add_argument('--runNumber', type=int, help='Number of run to load.')
parser.add_argument('--modelName', type=str, help='Name of model to load (without number). Default: modelG')
parser.add_argument('--modelNameOut', type=str, help='Name of model to save. Default: modelG')
parser.add_argument('--resultsDir', type=str, help='Directory for all results (model, figures etc.). Default: curr_dir')
parser.add_argument('--restoreEpoch', type=int, help='Epoch to restore. Default: -1')

parser.add_argument('--pretrained', default="no", type=str, help="Path to a pretrained generator for GAN")
parser.add_argument('--pretrainedDiscr', default="no", type=str, help="Path to a pretrained discriminator for GAN")
#/home/kumpf/projects/Neural_networks/physics_unet/results/tensorboard_frame/run00168/[]_epoch_400.pth
parser.add_argument('--checkpointEveryNThEpoch', type=int, help='Save checkpoint every n-th epoch. Default: 1')


parser.add_argument('--figsDirectory', type=str, help='Name of model to save. Default: fig_train_output')

parser.add_argument('--readDataOnTheFly', type=str, help='Read data/normalize it etc. on the fly. Default: False')
parser.add_argument('--runModelToTest', type=str, help='Run the model in test mode. Default: False')


parser.add_argument('--cudaDevice', type=str, help='Specify gpu to use. Default: cuda:0 (with single quotation marks)')


# Arguments for adversarial training
parser.add_argument('--pathToTrainedDiscr', type=str, help='Path to trained discriminator, including the filename. Default: ')


parser.add_argument('--doNotTrainDiscrIfAboveThreshold', type=float, default = 0.2, help='Stop the training of the discriminator if above a threshold. Continue, if it falls again below it.')
parser.add_argument('--trainOnlyDiscriminator', type=str, default = "False", help='If set to True, the generator is not updated anymore. Default: False')
parser.add_argument('--pretrainDiscriminatorForNEpochs', type=int, default = 5, help='Number of epochs to pretrain discriminator. Default: 0')


# Arguments for losses, copied from Sebastian

parser.add_argument('--upsample', type=str, default='bilinear', help='Upsampling for EnhanceNet: nearest, bilinear, bicubic, or pixelShuffle')


parser.add_argument('--losses', type=str, default = '!6!ssim:1&!1!l1:1,bceLogits:0.1', help="""
#!3!cos_angle:1.0,l1:0.5,perceptual:0.1&!1!l1:1&!1!l1:40
# !3!l1:0.5,perceptual:0.1,cos_angle:0.4&!1!l1:1&!1!bce:1000
Comma-separated list of loss functions: mse,perceptual,texture,adv. 
Optinally, the weighting factor can be specified with a colon.
Example: "--losses perceptual:0.1,texture:1e2,adv:10,grad_loss:1, cos_angle:1"
Opionally, multiple loss functions (e.g. if some channels are handled differently) can be used by separating with &, e.g. !1!l1:1&!3!l1:1. The number of channels should be within !1! and right after &)
""")

parser.add_argument('--perceptualLossLayers',
                    type=str,
                     # defaults found with VGGAnalysis.py
                    default='conv_1:0.026423,conv_2:0.009285,conv_3:0.006710,conv_4:0.004898,conv_5:0.003910,conv_6:0.003956,conv_7:0.003813,conv_8:0.002968,conv_9:0.002997,conv_10:0.003631,conv_11:0.004147,conv_12:0.005765,conv_13:0.007442,conv_14:0.009666,conv_15:0.012586,conv_16:0.013377',
                    help="""
Comma-separated list of layer names for the perceptual loss. 
Note that the convolution layers are numbered sequentially: conv_1, conv2_, ... conv_19.
Optinally, the weighting factor can be specified with a colon: "conv_4:1.0", if omitted, 1 is used.
""")
parser.add_argument('--textureLossLayers', type=str, default='conv_1,conv_3,conv_5', help="""
Comma-separated list of layer names for the perceptual loss. 
Note that the convolution layers are numbered sequentially: conv_1, conv2_, ... conv_19.
Optinally, the weighting factor can be specified with a colon: "conv_4:1.0", if omitted, 1 is used.
""")
parser.add_argument('--discriminator', type=str, default='enhanceNetLarge', help="""
Network architecture for the discriminator.
Possible values: enhanceNetSmall, enhanceNetLarge, tecoGAN
""")
parser.add_argument('--advDiscrThreshold', type=float, default=0.1, help="""
Adverserial training:
If the cross entropy loss of the discriminator falls below that threshold, the training for the discriminator is stopped.
Set this to zero to disable the check and use a fixed number of iterations, see --advDiscrMaxSteps, instead.
""")
parser.add_argument('--advDiscrMaxSteps', type=int, default=2, help="""
Adverserial training:
Maximal number of iterations for the discriminator training.
Set this to -1 to disable the check.
""")
parser.add_argument('--advDiscrInitialSteps', type=int, default=None, help="""
Adverserial training:
Number of iterations for the disciriminator training in the first epoch.
Used in combination with a pretrained generator to let the discriminator catch up.
""")
parser.add_argument('--advDiscrWeightClip', type=float, default=0.01, help="""
For the Wasserstein GAN, this parameter specifies the value of the hyperparameter 'c',
the range in which the discirminator parameters are clipped.
""")
parser.add_argument('--advGenThreshold', type=float, default=0.1, help="""
Adverserial training:
If the cross entropy loss of the generator falls below that threshold, the training for the generator is stopped.
Set this to zero to disable the check and use a fixed number of iterations, see --advGenMaxSteps, instead.
""")
parser.add_argument('--advGenMaxSteps', type=int, default=2, help="""
Adverserial training:
Maximal number of iterations for the generator training.
Set this to -1 to disable the check.
""")
parser.add_argument('--lossBorderPadding', type=int, default=16, help="""
Because flow + warping can't be accurately estimated at the borders of the image,
the border of the input images to the loss (ground truth, low res input, prediction)
are overwritten with zeros. The size of the border is specified by this parameter.
Pass zero to disable this padding. Default=16 as in the TecoGAN paper.
""")
#####



args = parser.parse_args()

if args.partialConv:
    partialConv = args.partialConv
if args.deformConv:
    deformConv = args.deformConv
elif args.netModel.find('Partial') > -1:
    partialConv = True
    # Remove the 'Partial' string from the argument
    args.netModel = args.netModel[:-7]
    args.partialConv = 'True'



if args.iterations:
    iterations = args.iterations
if args.batch_size:
    batch_size = args.batch_size
if args.lrG:
    lrG = args.lrG
if args.lrDiscr:
    lrDiscr = args.lrDiscr
if args.decayLr:
    decayLr = str2bool(args.decayLr)
if args.lrExFak:
    lrExFak = args.lrExFak
if args.channelsInNetwork:
    channelsInNetwork = args.channelsInNetwork

if args.batch_normalization:
    batch_normalization = str2bool(args.batch_normalization)


if args.dropoutFactor:
    dropout = args.dropoutFactor
if args.netRows:
    netRows = args.netRows
if args.netCols:
    netCols = args.netCols
if args.gridSizex:
    gridSizex = args.gridSizex
if args.gridSizey:
    gridSizey = args.gridSizey
if args.centerValidationPatch:
    centerValidationPatch = str2bool(args.centerValidationPatch)

if args.twoConvsPerLev:
    twoConvsPerLev = str2bool(args.twoConvsPerLev)

if args.exportAsPDF:
    exportAsPDF = str2bool(args.exportAsPDF)
if args.exportAsPNG:
    exportAsPNG = str2bool(args.exportAsPNG)
if args.exportFigsToTensorFlowInEachEpoch:
    exportFigsToTensorFlowInEachEpoch = str2bool(args.exportFigsToTensorFlowInEachEpoch)
if args.modeForTraining:
    modeForTraining = args.modeForTraining
if args.dataDir:
    dataDir = args.dataDir
if args.useDataDirAlt:
    useDataDirAlt = args.useDataDirAlt
    if str2bool(useDataDirAlt):
        if args.dataDirAlt:
            dataDir = args.dataDirAlt
if args.dataDirTest:
    dataDirTest = args.dataDirTest
if args.includeSubDirs:
    includeSubDirs = str2bool(args.includeSubDirs)
if args.maxNrFiles:
    maxNrFiles = args.maxNrFiles
if args.startFileID:
    startFileID = args.startFileID
if args.useHDF5Files:
    useHDF5Files = str2bool(args.useHDF5Files)
if args.filterByFileName:
    if args.filterByFileName != "no":
        filterByFileName = args.filterByFileName

if args.renderedDataType:
    renderedDataType = args.renderedDataType
if args.useColor:
    useColor = str2bool(args.useColor)
if args.bSparseSplatsColor:
    bSparseSplatsColor = str2bool(args.bSparseSplatsColor)
if args.useTargetColor:
    useTargetColor = str2bool(args.useTargetColor)
if args.useTarSplatsColor:
    useTarSplatsColor = str2bool(args.useTarSplatsColor)

if args.useRawInput:
    useRawInput = str2bool(args.useRawInput)
if args.useGradientOfInput:
    useGradientOfInput = str2bool(args.useGradientOfInput)
if args.useLinearInterpInput:
    useLinearInterpInput = str2bool(args.useLinearInterpInput)
if args.useWarpedInput:
    useWarpedInput = str2bool(args.useWarpedInput)
if args.useRawFeatureMaps:
    useRawFeatureMaps = str2bool(args.useRawFeatureMaps)
if args.useWarpedFeatureMaps:
    useWarpedFeatureMaps = str2bool(args.useWarpedFeatureMaps)

if args.flipXNow:
    flipXNow = str2bool(args.flipXNow)
if args.flipYNow:
    flipYNow = str2bool(args.flipYNow)

trainOnlyDiscriminator=False
if args.trainOnlyDiscriminator:
    trainOnlyDiscriminator = str2bool(args.trainOnlyDiscriminator)

if args.loadModel:
    loadModel = str2bool(args.loadModel)
    if args.modelName:
        modelName = args.modelName
        if loadModel:
            doLoad = args.modelName
        else:
            doLoad = ""
            # doLoad = "modelG"
if loadModel and args.runNumber:
    runNumber = args.runNumber
if args.resetLearningRate:
    resetLearningRate = str2bool(args.resetLearningRate)

if args.readDataOnTheFly:
    readDataOnTheFly = str2bool(args.readDataOnTheFly)
    print("Read data on the fly: "+ str(readDataOnTheFly))

if args.runModelToTest:
    runModelToTest = str2bool(args.runModelToTest)

cudaDevice = 'cpu'# 'cuda:0'
if args.cudaDevice:
    cudaDevice = args.cudaDevice

modelNameOut = []
if args.modelNameOut:
    modelNameOut = args.modelNameOut
if args.resultsDir:
    resultsDir = args.resultsDir
if args.restoreEpoch:
    restoreEpoch = args.restoreEpoch
if args.checkpointEveryNThEpoch:
    checkpointEveryNThEpoch = args.checkpointEveryNThEpoch

if args.figsDirectory:
    figsDirectory = args.figsDirectory






if args.useNormals:
    useNormals = str2bool(args.useNormals)
if args.useNormalsScreen:
    useNormalsScreen = str2bool(args.useNormalsScreen)

if args.useSparseSplatsNormal:
    useSparseSplatsNormal = str2bool(args.useSparseSplatsNormal)
if args.useTarNormalsScreen:
    useTarNormalsScreen = str2bool(args.useTarNormalsScreen)
if args.useTarSplatsNormalsScreen:
    useTarSplatsNormalsScreen = str2bool(args.useTarSplatsNormalsScreen)
if args.useSparseSplatsNormalScreen:
    useSparseSplatsNormalScreen = str2bool(args.useSparseSplatsNormalScreen)

if args.useDepth:
    useDepth = str2bool(args.useDepth)
if args.useSparseSplatsDepth:
    useSparseSplatsDepth = str2bool(args.useSparseSplatsDepth)
if args.useTargetNormals:
    useTargetNormals = str2bool(args.useTargetNormals)
if args.useTarSplatsNormals:
    useTarSplatsNormals = str2bool(args.useTarSplatsNormals)
if args.useTargetDepth:
    useTargetDepth = str2bool(args.useTargetDepth)
if args.useTarSplatsDepth:
    useTarSplatsDepth = str2bool(args.useTarSplatsDepth)
if args.useLearnDepthMask:
    useLearnDepthMask = str2bool(args.useLearnDepthMask)
if args.useLearnDepthMaskSplats:
    useLearnDepthMaskSplats = str2bool(args.useLearnDepthMaskSplats)


# To save the user from wrong inputs...
if str(args.netModel).__contains__('DUNet'):
    if useLearnDepthMask == False and useLearnDepthMaskSplats == False:
        useLearnDepthMask = True
        args.useLearnDepthMask = 'True'

sigmoidToLastOutput = False
if useLearnDepthMask or useLearnDepthMaskSplats:
    sigmoidToLastOutput = True
if args.useGTMask:
    useGTMask = str2bool(args.useGTMask)




keysToLoad = []
if renderedDataType=='shaded':
    keysToLoad = utils.createNewInputKeysList(bNormals=useNormals, bSparseSplatsNormal=useSparseSplatsNormal,
                                              bNormalsScreen=useNormalsScreen, bSparseSplatsNormalScreen=useSparseSplatsNormalScreen,
                                              bTarNormals=useTargetNormals, bTarNormalsScreen=useTarNormalsScreen,
                                              bColors=useColor,
                                              bSparseSplatsColor=bSparseSplatsColor,
                                              bTarColors=useTargetColor, bDepth=useDepth, bSparseSplatsDepth=useSparseSplatsDepth,bTarDepth=useTargetDepth,
                                              bTarSplatsDepth=useTarSplatsDepth, bTarSplatsNormals=useTarSplatsNormals, bTarSplatsNormalsScreen=useTarSplatsNormalsScreen,
                                              bTarSplatsColor=useTarSplatsColor,
                                              bLearnDepthMask=useLearnDepthMask, bLearnDepthMaskSplats=useLearnDepthMaskSplats,
                                              bUseGTMask=useGTMask)
pcrenderopts = DotMap()

pcrenderopts.useNormals = useNormals
pcrenderopts.useDepth = useDepth
pcrenderopts.useTargetNormals = useTargetNormals
pcrenderopts.useTargetDepth = useTargetDepth
pcrenderopts.useLearnDepthMask = useLearnDepthMask
pcrenderopts.useGTMask = useGTMask
pcrenderopts.keysToLoad = keysToLoad

if showDebugInfo:
    print("PC keys to load")
    print(keysToLoad)

print("model should be loaded: {}, model name: {}, output name {}".format(loadModel, modelName, modelNameOut))

if modeForTraining == 'wind':
    modeForTraining = 'stamped vec'

# ToDo : datadirs are not passed to the dataset object so far. Further, the dataset object also parses arguments. This should be changed.

########### Adjust parameters / variables, for certain occasions
if runModelToTest:
    batch_size = 1
    if not loadModel:
        sys.exit(0)



###########################
# NT_DEBUG , remove
#iterations = 5000


prefix = ""
#if len(sys.argv)>1:
#    prefix = sys.argv[1]
#    print("Output prefix: {}".format(prefix))

autoIter   = False

print("LR: {}".format(lrG))
print("LR decay: {}".format(decayLr))
print("Iterations: {}".format(iterations))
print("Dropout: {}".format(dropout))

##########################

# set GPU device, if specified
print('__CUDNN VERSION:', torch.backends.cudnn.version())
print('__Number CUDA Devices:', torch.cuda.device_count())
print('__Devices')
#call(["nvidia-smi", "--format=csv", "--query-gpu=index,name,driver_version,memory.total,memory.used,memory.free"])
print('Active CUDA Device: GPU', torch.cuda.current_device())

print ('Available devices ', torch.cuda.device_count())
print ('Current cuda device ', torch.cuda.current_device())
#device = torch.cuda.set_device(cudaDevice)
print ('Current cuda device ', torch.cuda.current_device())
#print('Current device name', torch.cuda.get_device_name(torch.cuda.current_device()))

seed = random.randint(0, 2**32 - 1)
print("Random seed: {}".format(seed))
random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)
#torch.backends.cudnn.deterministic=True # warning, slower

# create pytorch data object with dfp dataset
data = []
if modeForTraining == 'frame':
    if not runModelToTest:
        data = dataset.FrameDataset(prop, shuffle=args.shuffleData, dataDir=dataDir, dataDirTest=dataDirTest, includeSubDirsArg = includeSubDirs, \
                                maxFiles = maxNrFiles, startFileID = startFileID, readDataOnTheFly = readDataOnTheFly, readDataFromH5Py = useHDF5Files,
                                useRawInput = useRawInput, useGradientOfInput=useGradientOfInput, useLinearInterpInput=useLinearInterpInput,
                                useWarpedInput = useWarpedInput, useRawFeatureMaps = useRawFeatureMaps,
                                useWarpedFeatureMaps = useWarpedFeatureMaps, gridSizex=gridSizex, gridSizey=gridSizey, filterword = filterByFileName,
                                flipXNow=flipXNow, flipYNow=flipYNow, cropInputBasedOnChannel = args.cropInputBasedOnChannel,
                                dataResolutionX = args.dataResolutionX, dataResolutionY=args.dataResolutionY,
                                maxValFiles = args.maxValFiles)
    else:
        data = dataset.FrameDataset(prop, shuffle=args.shuffleData, dataDir=dataDir, dataDirTest=dataDirTest, includeSubDirsArg = includeSubDirs, \
                                maxFiles = maxNrFiles, startFileID = startFileID, readDataOnTheFly = readDataOnTheFly, readDataFromH5Py = useHDF5Files,
                                useRawInput = useRawInput, useGradientOfInput=useGradientOfInput,useLinearInterpInput=useLinearInterpInput,
                                useWarpedInput = useWarpedInput, useRawFeatureMaps = useRawFeatureMaps,
                                useWarpedFeatureMaps = useWarpedFeatureMaps, gridSizex=gridSizex, gridSizey=gridSizey, splitFactor = 1, filterword = filterByFileName,
                                flipXNow=flipXNow, flipYNow=flipYNow, cropInputBasedOnChannel = args.cropInputBasedOnChannel,
                                dataResolutionX = args.dataResolutionX, dataResolutionY=args.dataResolutionY,
                                maxValFiles=args.maxValFiles)
elif modeForTraining == 'pcrender':

    data = dataset.PCRDataset(prop, shuffle=args.shuffleData, dataDir=dataDir, dataDirTest=dataDirTest, includeSubDirsArg=includeSubDirs, \
                                maxFiles=maxNrFiles, startFileID = startFileID, readDataOnTheFly=readDataOnTheFly, readDataFromH5Py=useHDF5Files,
                                useRawInput=useRawInput, useGradientOfInput=useGradientOfInput, useLinearInterpInput=useLinearInterpInput,
                                useWarpedInput=useWarpedInput, useRawFeatureMaps=useRawFeatureMaps,
                                useWarpedFeatureMaps=useWarpedFeatureMaps, gridSizex=gridSizex, gridSizey=gridSizey, filterword=filterByFileName,
                                flipXNow=flipXNow, flipYNow=flipYNow, cropInputBasedOnChannel = args.cropInputBasedOnChannel,
                                dataResolutionX = args.dataResolutionX, dataResolutionY=args.dataResolutionY, maxValFiles=args.maxValFiles,
                                pcopts=pcrenderopts)

else:
    data = dataset.TurbDataset(prop, shuffle=args.shuffleData, dataDir=dataDir, dataDirTest=dataDirTest, maxFiles = maxNrFiles)


trainLoader = []
if not runModelToTest:
    trainLoader = DataLoader(data, batch_size=batch_size, shuffle=True, drop_last=True)
    print("Training batches: {}".format(len(trainLoader)))

dataValidation = []
valiLoader = []
if not runModelToTest:
    if modeForTraining == 'frame' or modeForTraining == 'pcrender':
        dataValidation = dataset.ValiDatasetFrame(data, centerValidationPatch=centerValidationPatch)
    else:
        dataValidation = dataset.ValiDataset(data)
    valiLoader = DataLoader(dataValidation, batch_size=batch_size, shuffle=False, drop_last=True)
    currDataLoader = len(trainLoader)


dataTest = []
testLoader = []
if runModelToTest:
    dataTest = dataset.ValiDatasetFrame(data, centerValidationPatch=centerValidationPatch)
    testLoader = DataLoader(dataTest, batch_size=batch_size, shuffle=False, drop_last=True)
    currDataLoader = len(testLoader)



print("Validation batches: {}".format(len(valiLoader)))

# setup training
if currDataLoader == []:
    currDataLoader = 1
    print("Something was not implemented correctly. The number of epochs is now set to number of iterations: {}".format(currDataLoader))

epochs = int(iterations/currDataLoader + 0.5)

inpChannelsFrame = -1
outChannels = 1

# Those two variables are used to control which channels will be used for the L1 (or later PSNR etc. ) loss
channelsL1LossRangeStart = 0
channelsL1LossRangeStartInp = 0
#Careful, this is used for input and prediction, so the channels have to be in the right order for this to work...
channelsL1LossRangeEnd = 0
channelsL1LossRangeEndInp = 0

tarDepLoc = 0

if modeForTraining == 'frame':
    inpChannelsFrame = 2*(int(useRawInput) + 2*int(useGradientOfInput) +  int(useWarpedInput) + int(useRawFeatureMaps) + int(useWarpedFeatureMaps)) + int(useLinearInterpInput)
    channelsL1LossRangeEnd = inpChannelsFrame

    channelsL1LossRangeStartInp = channelsL1LossRangeStart
    channelsL1LossRangeEndInp = channelsL1LossRangeEndInp

elif modeForTraining == 'pcrender':
    outChannels = 3*(int(useTargetNormals)) + int(useTargetDepth) + int(useLearnDepthMask) * (not str(args.netModel).__contains__('DUNet'))
    channelsL1LossRangeEnd = 3*(int(useTargetNormals)) + int(useTargetDepth)
    inpChannelsFrame = 3*(int(useNormals)) + int(useDepth) + int(useGTMask)

    # This is used to overwrite the existing variables.
    if keysToLoad != []:
        inpChannelsFrame = 0
        outChannels = 0
        channelsL1LossRangeEnd = 0
        # Count the number of in channels
        for item in keysToLoad:
            inpChannelsFrame += int(item[2] == 'inp') * item[1]

        # Count the number of tar channels (contains'tar')
        for item in keysToLoad:
            outChannels += int(item[2] == 'tar') * item[1]

        for item in keysToLoad:
            if item[2] == 'tar':
                channelsL1LossRangeEnd += int(item[0] == 'PointsNormal' or item[0] == 'PointsColor' or item[0] == 'PointsDepth' or item[0] == 'SplatsDepth') * item[1]

        channelsL1LossRangeStartInp = channelsL1LossRangeStart
        channelsL1LossRangeEndInp = channelsL1LossRangeEnd

        # Hack for only evaluating on depth. So if input is normals, depth, and target is only depth, that the right channels are compared for the
        # untrained loss...
        inpCount = 0
        inpDepLoc = -1
        tarCount = 0
        tarDepLoc = -1
        for item in keysToLoad:
            if item[0].__contains__('Depth') and not item[0].__contains__('Learn'):
                if item[2] == 'inp' and inpDepLoc == -1:
                    inpDepLoc = inpCount
                elif item[2] == 'tar' and tarDepLoc == -1:
                    tarDepLoc = tarCount
            if item[2] == 'inp':
                inpCount += item[1]
            elif item[2] == 'tar':
                tarCount += item[1]
        if inpDepLoc != tarDepLoc and inpDepLoc > 0  and  tarDepLoc > 0 :
            channelsL1LossRangeStartInp = inpDepLoc -1
            channelsL1LossRangeEndInp = inpDepLoc
            channelsL1LossRangeStart = tarDepLoc -1
            channelsL1LossRangeEnd = tarDepLoc

        if keysToLoad != []:
            inpChannelsFrame = inpCount
            outChannels = tarCount
            print("CAUTION: evaluation of loss compared to untrained is only performed on the depth!")


#print('target depth key', keysToLoad[tarDepLoc])


print("Number of input channels used for loss function ")
print(inpChannelsFrame)
print("Number of channels used for L1 loss")
print(channelsL1LossRangeEnd)

#######################
# Build Loss Function #
#######################
device = torch.cuda.current_device()
print('===> Building losses')
multipleCriterions = False
argsForCriterion = []
lossCriterionForChannels = []
klri = utils.keylistToRepetStrList(utils.keylistInpTarKeys(keysToLoad)[0])
klrt = utils.keylistToRepetStrList(utils.keylistInpTarKeys(keysToLoad)[1]) # target keys
currStartCh = 0
if "&" in args.losses: # This means, more than one criterion has to be build
    multipleCriterions = True
    # Split them, then remove the channels specifier !x! and store it somewhere
    lossArgs = [s for s in args.losses.split('&')]
    for ilossArg in lossArgs:
        currChannel, currLossStr = int(ilossArg[1:2]), ilossArg[3:]
        lossCriterionForChannels.append(currChannel)
        currAdaptArgs = copy.deepcopy(args)
        # Just interchange the losses argument. The others can stay the same
        currAdaptArgs.losses = currLossStr
        argsForCriterion.append(currAdaptArgs)


        print('Loss', currLossStr,' applied to ',  klrt[currStartCh:currStartCh+currChannel])
        currStartCh += currChannel

else:
    argsForCriterion.append(copy.deepcopy(args))
    lossCriterionForChannels.append(outChannels) # This was inChannels before. Was there a reason?


bPrintOnce1 = True
bPrintOnce2 = True
bPrintOnce2 = True
lcrFC = lossCriterionForChannels
criterions = []
for currArgs in argsForCriterion:
    criterion = losses.LossNet(
        device,
        data.inputShape[0], #no previous image, only rgb, mask, optional depth + normal
        data.outputShape[0],
        gridSizex, #high resolution size
        currArgs.lossBorderPadding,
        currArgs)
    criterion.to(device)
    criterions.append(criterion)
    print('Losses:', criterion)
    res = gridSizex
    criterion.print_summary(
        (data.outputShape[0], res, res),
        (data.outputShape[0], res, res),
        (data.inputShape[0], res, res),
        (data.outputShape[0], res, res),
        currArgs.batch_size)
numberOfDiscriminators = 0
alreadyImplementForMultipleDiscr = False
for criterion in criterions:
    if (criterion.discriminator != None):
        numberOfDiscriminators += 1
if numberOfDiscriminators > 1:
    assert (alreadyImplementForMultipleDiscr)


#######################
# Build Model         #
#######################

print(args.netModel)
netG = []
if args.pretrained == "no":
    if modeForTraining == 'frame' or modeForTraining == 'pcrender':
        if args.netModel == 'GridNet':
            netG = gnf(inpChannels = inpChannelsFrame, outChannels=outChannels, channels = channelsInNetwork, netRows = netRows, netCols = netCols, kernelSizeSpec=(args.kernelsizew, args.kernelsizeh), paddingSpec=args.padding,
                   dilationSpec=args.dilation, totalDropout=dropout, cOpt=0, enableTotalDropout = str2bool(args.enableTotalDropoutGridNet), batchNorm=batch_normalization, keysToLoad=keysToLoad)
        elif args.netModel == 'GridNetConcat':
            netG = gnf(inpChannels=inpChannelsFrame, outChannels=outChannels, channels=channelsInNetwork, netRows=netRows, netCols=netCols, kernelSizeSpec=(args.kernelsizew, args.kernelsizeh), paddingSpec=args.padding,
                       dilationSpec=args.dilation, totalDropout=dropout, cOpt=1, enableTotalDropout = str2bool(args.enableTotalDropoutGridNet), batchNorm=batch_normalization, keysToLoad=keysToLoad)
        elif args.netModel == 'UNet':
            netG = ModifiedUNetFrame(channelExponent=channelsInNetwork, inputChannels=inpChannelsFrame, outputChannels=outChannels, dropout=dropout, sizeOfKernels=np.fromstring(args.kernelsizeUNet, dtype=int, sep=','),
                                 resX=gridSizex, resY=gridSizey, usePartialConv=partialConv, uNetDepth=netRows, sigmoidToLastOutput=sigmoidToLastOutput, twoConvsPerLev = twoConvsPerLev, batchNorm=batch_normalization, keysToLoad=keysToLoad)
        elif args.netModel == 'UNetMax':
            netG = UNetFrame(channelExponent=channelsInNetwork, inputChannels=inpChannelsFrame, outputChannels=outChannels, dropout=dropout, sizeOfKernels=np.fromstring(args.kernelsizeUNet, dtype=int, sep=','),
                             resX=gridSizex, resY=gridSizey, maxPool=True, usePartialConv=partialConv, uNetDepth=netRows, sigmoidToLastOutput=sigmoidToLastOutput, twoConvsPerLev = twoConvsPerLev, batchNorm=batch_normalization, keysToLoad=keysToLoad)
        elif args.netModel == 'ResUNet':
            netG = UNetFrame(channelExponent=channelsInNetwork, inputChannels=inpChannelsFrame, outputChannels=outChannels, dropout=dropout, sizeOfKernels=np.fromstring(args.kernelsizeUNet, dtype=int, sep=','),
                                 resX=gridSizex, resY=gridSizey, addNotCat = True, usePartialConv=partialConv, useDeformConv=deformConv, uNetDepth=netRows, sigmoidToLastOutput=sigmoidToLastOutput, twoConvsPerLev = twoConvsPerLev, batchNorm=batch_normalization, keysToLoad=keysToLoad)
        elif args.netModel == 'UNetPP':
            netG = PreProcessNestedUNet(channelExponent=channelsInNetwork, inputChannels=inpChannelsFrame, outputChannels=outChannels, dropout=dropout, sizeOfKernels=np.fromstring(args.kernelsizeUNet, dtype=int, sep=','),
                                 resX=gridSizex, resY=gridSizey, usePartialConv=partialConv, uNetDepth=netRows, sigmoidToLastOutput=sigmoidToLastOutput, twoConvsPerLev = twoConvsPerLev, batchNorm=batch_normalization, keysToLoad=keysToLoad)



        elif args.netModel == 'Enhancenet':
            currOpt = DotMap()
            currOpt.upsample="nearest"
            currOpt.reconType="residual"
            currOpt.useBn=str2bool(args.batch_normalization)
            netG = EnhanceNet(1, inpChannelsFrame, outChannels, currOpt, inner_channels=channelsInNetwork, keysToLoad=keysToLoad)
            # Set the resiudal improvement only for the linear interpolated channel
            if modeForTraining == 'frame' and currOpt.reconType == "residual":
                netG.setInputChannelForResidual(data.posOfLinearInterp)
                print("Just set the input channel which should be used for adding the residual to to {}".format(data.posOfLinearInterp))

        print(netG)  # print full net
        model_parameters = filter(lambda p: p.requires_grad, netG.parameters())
        params = sum([np.prod(p.size()) for p in model_parameters])
        print("Initialized TurbNet with {} trainable params ".format(params))

    #else:
     #   netG = TurbNetGVec(channelExponent=expo, dropout=dropout)




#############################
# PRETRAINED
#############################
if args.pretrained != "no":
    checkpoint = torch.load(args.pretrained)
    netG = checkpoint['model']
    netG.load_state_dict(checkpoint['model'].state_dict())
    print(netG)
    model_parameters = filter(lambda p: p.requires_grad, netG.parameters())
    params = sum([np.prod(p.size()) for p in model_parameters])
    print("Initialized TurbNet with {} trainable params ".format(params))
    #only load the state dict, not the whole model
    #this asserts that the model structure is the same
    print('Using pretrained model for the generator')
if args.pretrainedDiscr != "no":
    for criterion in criterions:
        if (criterion.discriminator != None):
            assert criterion.discriminator is not None
            checkpoint = torch.load(args.pretrainedDiscr)
            criterion.discriminator = checkpoint['modelDiscriminator']
            criterion.discriminator.load_state_dict(checkpoint['discriminator'])
            print('Using pretrained model for the discriminator')
        if numberOfDiscriminators > 1:
            assert (alreadyImplementForMultipleDiscr)




if not os.path.exists(resultsDir):
    os.makedirs(resultsDir)




print('===> Create Optimizer')
optimizer = []
scheduler = []
gen_optimizer = []
gen_scheduler = []
discr_optimizer = []
discr_scheduler = []

adversarial_training = []
if numberOfDiscriminators == 0:
    adversarial_training = False
    #if len(doLoad) == 0:
    optimizer = optim.Adam(netG.parameters(), lr=lrG)
    gen_optimizer = optimizer
    if lrExFak > 0:
        scheduler = optim.lr_scheduler.ExponentialLR(optimizer, lrExFak)
        gen_scheduler = scheduler
    else: # Take the stepwise decay as alternative
        scheduler = optim.lr_scheduler.StepLR(optimizer, args.lrStep, args.lrGamma)
        gen_scheduler = scheduler
else:
    adversarial_training = True
    gen_optimizer = optim.Adam(netG.parameters(), lr=lrG, betas=(0.5, 0.999), weight_decay=0.0)
    for criterion in criterions:
        if (criterion.discriminator != None):
            discr_optimizer = optim.Adam(criterion.discriminator.parameters(), lr=lrDiscr)
    if numberOfDiscriminators > 1:
        assert (alreadyImplementForMultipleDiscr)
    if lrExFak > 0:
        gen_scheduler = optim.lr_scheduler.ExponentialLR(gen_optimizer, lrExFak)
        discr_scheduler = optim.lr_scheduler.ExponentialLR(discr_optimizer, lrExFak)
    else: # Take the stepwise decay as alternative
        gen_scheduler = optim.lr_scheduler.StepLR(gen_optimizer, args.lrStep, args.lrGamma)
        discr_scheduler = optim.lr_scheduler.StepLR(discr_optimizer, args.lrStep, args.lrGamma)
    scheduler = gen_scheduler
    optimizer = gen_optimizer




nextRunNumber = -1
optimizerG = []
if iterativeTraining:
    modelFn = "./" + prefix + "modelG"
    if not os.path.isfile(modelFn):
        print("Could not find network model")
        sys.exit(-1)
    checkpoint = torch.load(modelFn)

    netG.load_state_dict(checkpoint['state_dict'])
    optimizerG = optim.Adam(netG.parameters(), lr=lrG, betas=(0.5, 0.999), weight_decay=0.0)
    optimizerG.load_state_dict(checkpoint['optimizer'])
    print("Loaded model " + modelFn)
else:
    #if args.pretrained == "no":
    #    netG.apply(weights_init)
    if len(doLoad) > 0:
        nextRunNumber = int(runNumber)
        runName = 'run%05d' % nextRunNumber
        modeldir = os.path.join(resultsDir, runName)
        if restoreEpoch == -1:
            # find highest available epoch number.
            restoreEpoch = 0
            while True:
                modelInName = os.path.join(modeldir, "{}_epoch_{}.pth".format(modelName, restoreEpoch + 1))
                if not os.path.exists(modelInName):
                    break;
                restoreEpoch += 1
        else:
            restoreEpoch = restoreEpoch

        print("Restore training from run ", runNumber, " and epoch ", restoreEpoch)
        modelInName = os.path.join(modeldir, "{}_epoch_{}.pth".format(modelName, restoreEpoch))

        checkpoint = torch.load(modelInName)
        if args.pretrained != "no":
            print('Attention: A specified pretrained model was already loaded. So the model is not loaded from the restore point!')
        else:
            netG = checkpoint['model']


        if adversarial_training:
            # If error occurs, try adding .state_dict() after the dictionary access.
            if args.pretrainedDiscr != "no":
                print(
                    'Attention: A specified pretrained criterion was already loaded. So the discriminator is not loaded from the restore point!')
            else:
                for criterion in criterions:
                    if (criterion.discriminator != None):
                        criterion.discriminator.load_state_dict(checkpoint['discriminator'])
                if numberOfDiscriminators > 1:
                    assert (alreadyImplementForMultipleDiscr)

            discr_optimizer.load_state_dict(checkpoint['discr_optimizer'])
            gen_optimizer.load_state_dict(checkpoint['gen_optimizer'])
            optimizerG = gen_optimizer
            discr_scheduler.load_state_dict(checkpoint['discr_scheduler'])
            gen_scheduler.load_state_dict(checkpoint['gen_scheduler'])
        else:

            optimizerG = gen_optimizer
            # The next if is not optimal, since the scheduler looses all previously gathered information.
            if not resetLearningRate:
                for item in checkpoint:
                    if item == 'gen_scheduler':
                        print(item)
                        scheduler.load_state_dict(checkpoint['gen_scheduler'].state_dict())
                    elif item == 'scheduler':
                        print(item)
                        scheduler.load_state_dict(checkpoint['scheduler'].state_dict())

        print("Loaded optimizer " + doLoad)

        if resetLearningRate:
            for param_group in optimizerG.param_groups:
                param_group['initial_lr'] = lrG
                param_group['lr'] = lrG
                print("reset param_group to {}".format(param_group['lr']))
            scheduler = optim.lr_scheduler.ExponentialLR(optimizerG, lrExFak)

            print("Reset learning rate to {}. Scheduler deactivated...".format(lrG))

        print("Loaded model " + doLoad)
    else:
        # Generate new folder for the current run in the specified directory
        nextRunNumber = max(utils.findNextRunNumber(resultsDir), utils.findNextRunNumber(resultsDir)) + 1 # The second in max was originally the model dir. So far, they are the same in this code

        optimizerG = optim.Adam(netG.parameters(), lr=lrG, betas=(0.5, 0.999), weight_decay=0.0)


if torch.cuda.device_count() >1:
    netG = nn.DataParallel(netG)

netG.cuda()

criterionL1 = nn.L1Loss()
criterionL1.cuda()
#criterionL1 = criterion
#criterionL1.cuda()



#paths
print('Current run: %05d'%nextRunNumber)
runName = 'run%05d'%nextRunNumber
logdir = os.path.join(resultsDir, runName)
modeldir = os.path.join(resultsDir, runName)

#os.makedirs(modeldir)

optStr = str(args)
print(optStr)


if not os.path.exists(modeldir):
    os.mkdir(modeldir)
if not os.path.exists(logdir):
    os.mkdir(logdir)

with open(os.path.join(modeldir, 'info.txt'), "w") as text_file:
    text_file.write(optStr)

# If there is a keylist used for PCRenderer, save it, so the GUI can load it later.
if keysToLoad != []:
    with open(os.path.join(modeldir, 'PCRenderKeysToLoad.pkl'), 'wb') as f:
        pickle.dump(keysToLoad, f)
    with open(os.path.join(modeldir, 'PCRenderKeysToLoad.txt'), 'w') as f:
        print(keysToLoad, file=f)


#writer = SummaryWriter(logdir)
writer = SummaryWriter(logdir, purge_step=1)

# Loop through the arguments, and convert them to a string which can be used immideatly again to run the model again
argsStr = ""
argsStr2 = ""
for i in args.__dict__:
    argsStr += "--{} {} ".format(i, args.__dict__[i])
    argsStr2 += "export {}={}  \n".format(i, args.__dict__[i])

writer.add_text('args to copy', argsStr, 0)
writer.add_text('args for export', argsStr2, 0)
writer.add_text('info', optStr, 0)





####### Part for Training ############

zerosOrOnes = []
#blendedInp = []

#blendedInp = Variable(torch.FloatTensor(batch_size, 1, 1, 1))
#blendedInp.cuda()

if data.bPolynom:
    targets = Variable(torch.FloatTensor(batch_size, 1, 128, 128))
    inputs = Variable(torch.FloatTensor(batch_size, 1, 128, 128))
elif modeForTraining == 'frame' or modeForTraining == 'pcrender':
    targets = Variable(torch.FloatTensor(batch_size, 1, gridSizex, gridSizey))
    inputs = Variable(torch.FloatTensor(batch_size, inpChannelsFrame, gridSizex, gridSizey))
    zerosOrOnes = Variable(torch.FloatTensor(batch_size, 1, 1, 1))
    zerosOrOnes = zerosOrOnes.cuda()
else:
    targets = Variable(torch.FloatTensor(batch_size, 2, 128, 128))
    inputs  = Variable(torch.FloatTensor(batch_size, 3, 128, 128))
    zerosOrOnes = Variable(torch.FloatTensor(batch_size, 1, 1, 1))
    zerosOrOnes = zerosOrOnes.cuda()

targets = targets.cuda()
inputs  = inputs.cuda()

#prediction = Variable(torch.FloatTensor(1, 1, 1, 1))
#prediction.cuda()

##########################


def checkpoint(epoch):
    model_out_path = os.path.join(modeldir, "{}_epoch_{}.pth".format(modelNameOut, epoch))
    state = []
    if not adversarial_training:
        state = {'epoch': epoch + 1, 'model': netG}
        state.update({'optimizer':optimizer, 'scheduler':scheduler,
                      'gen_optimizer': gen_optimizer,
                      'gen_scheduler': gen_scheduler})
    else:
        state = {'epoch': epoch + 1, 'model': netG}
        i = 0

        for criterion in criterions:
            if (criterion.discriminator != None):
                if len(criterions) == 1 or numberOfDiscriminators == 1:
                    state.update({'modelDiscriminator': criterion.discriminator})
                    state.update({'discr_optimizer':discr_optimizer,
                                  'gen_optimizer':gen_optimizer,
                                  'discr_scheduler':discr_scheduler,
                                  'gen_scheduler':gen_scheduler,
                                  'discriminator': criterion.discriminator.state_dict()})
                else:
                    alreadyImplement = False
                    assert(alreadyImplement)
                    state.update({'modelDiscriminator%d'%i: criterion.discriminator})
                    state.update({'discr_optimizer%d'%i:discr_optimizer,
                                  'discr_scheduler%d'%i:discr_scheduler,
                                  'discriminator%d'%i: criterion.discriminator.state_dict()})
            i += 1
    torch.save(state, model_out_path)
    print("Checkpoint (adv = {}) saved to {}".format(adversarial_training, model_out_path))


def feedThroughNet(input):
    if args.netModel == "Enhancenet":
        prediction,_ = netG(input)
    else:
        prediction = netG(input)
    return prediction


def train(epoch, epochs):
    print("Starting epoch {} / {}".format((epoch),epochs))
    if not resetLearningRate and decayLr:
        scheduler.step()

    # This can be done by schedulers...
    #if decayLr:
    #    for g in optimizerG.param_groups:
    #        g['lr'] = utils.exp_decay(lrG, lrExFak, epoch)

    zerosOrOnes = Variable(torch.FloatTensor(batch_size, 1, 1, 1))
    zerosOrOnes = zerosOrOnes.cuda()

    netG.train()


    L1_accum = 0.0
    L1stamp_accum = 0.0
    lossOrgAccum = 0.0
    #samples_accum = 0
    epochProgressPerc = 0

    for p in netG.parameters():
        p.requires_grad = True

    #pbar = tqdm(total=len(trainLoader))

    for i, traindata in enumerate(tqdm(trainLoader), 0):
        inputs_cpu, targets_cpu, _,_,  idxes, damaged = traindata
        if damaged:
            continue
        # current_batch_size = targets_cpu.size(0)
        #print(inputs_cpu[0,0,0])

        if i == 0:
            ##########################
            # create a range object for the loss function. The loss should not be computed along the boundary,
            # since 1. the optical flow is very inaccurate there and 2. convolutional layers destroy the boundaries
            xShape = inputs_cpu.shape[2]
            xs = xShape
            yShape = inputs_cpu.shape[3]
            ys = yShape
            lossRangeXY = np.ix_(range(lossBorder,xShape-lossBorder), range(lossBorder, yShape- lossBorder))
            #:,:,lb : xs - lb, lb : ys - lb
            ##########################

        targets, inputs = targets_cpu.float().cuda(), inputs_cpu.float().cuda()


        # TODO: Blending has be adjusted to the stepSize used for this specific dataset. So far, the middle frame t = 0.5 is assumed
        # Further, it assumes, that inp1 is in channel 0, inp2 in channel 1.
        trilinarIntInp = []

        if modeForTraining == 'frame':
            if False:
                blendedInp = blendedInp.float().cuda()
                blendedInp.data.resize_as_(targets_cpu).copy_(
                    (inputs_cpu.data[:, 0:1, :, :] + inputs_cpu.data[:, 1:2, :, :]) / 2.)

                meanInp = np.mean( inputs_cpu.cpu().numpy()[0,0,:,:])
                #zerosOrOnes_np = np.ones(inputs_cpu.shape) * np.mean( inputs_cpu.cpu().numpy()[0,0,:,:])
                zerosOrOnes.data.resize_as_(inputs_cpu).copy_(inputs_cpu)
                zerosOrOnes *= 0
                zerosOrOnes += meanInp
            # Take the mean using pytorch, so everything happens on the GPU
            blendedInp = torch.mean(inputs[:, 0:2], 1, True)

            # Trilinear interpolation is used here. However, the result is the same as blending since we stay on the same grid for the intermediate level.
            trilinarIntInp = nn.functional.interpolate(inputs.unsqueeze(1)[:,0:1,0:2],size = [3, inputs.shape[2], inputs.shape[3]],
                                                    mode = 'trilinear').squeeze(1)

            #zerosOrOnes = torch.mean(inputs_cpu[:,0:2], 1, True)

        #netG.zero_grad()
        optimizerG.zero_grad()


        gen_out = feedThroughNet(inputs)




        lossL1 = criterionL1(gen_out[:,:,lb : xs - lb, lb : ys - lb],
                             targets[:,:,lb : xs - lb, lb : ys - lb])
        lossL1.backward()

        inputsStamp = []
        lossOrgData = -1
        lossBlendData = []
        blendedAvailable = False
        if modeForTraining == 'stamped vec':
            inputsStamp = inputs[:, [0, 1], :, :]
            #### BUG HERE: LOSS ORG NEVER COMPUTED.... ######
            L1stamp_accum += lossOrgData.item()
            #lossOrgData = criterionL1(inputsStamp[:,:,lb : xs - lb, lb : ys - lb], targets[:,:,lb : xs - lb, lb : ys - lb])
        elif modeForTraining == 'noise poly' or modeForTraining == 'pcrender':
            lossOrgData = criterionL1(inputs[:,:,lb : xs - lb, lb : ys - lb], targets[:,:,lb : xs - lb, lb : ys - lb])
            lossOrgAccum += lossOrgData.item()
        elif modeForTraining == 'frame':
            #lossOrgData = criterionL1(zerosOrOnes[:,:,lb : xs - lb, lb : ys - lb], targets[:,:,lb : xs - lb, lb : ys - lb])
            lossBlendData = criterionL1(blendedInp[:,:,lb : xs - lb, lb : ys - lb], targets[:,:,lb : xs - lb, lb : ys - lb])
            lossTrilinear = criterionL1(trilinarIntInp[:,1,lb : xs - lb, lb : ys - lb], targets[:,:,lb : xs - lb, lb : ys - lb])
            blendedAvailable = True

            L1stamp_accum += lossBlendData.item()
            writer.add_scalar("frameTrain/lossBlended", lossBlendData.item(), i + ((epoch-1)*len(trainLoader)))

        optimizerG.step()

        lossL1viz = lossL1.item()
        L1_accum += lossL1viz

        writer.add_scalar('frameTrain/loss', lossL1.item(), i + len(trainLoader)*(epoch-1) )



        if blendedAvailable:
            writer.add_scalar("frameTrain/percOverBlended", (100 - round((lossL1.item() / (lossBlendData.item() + 10e-9)*1000)) / 10) ,  i + len(trainLoader)*(epoch-1))
            writer.add_scalar("frameTrain/percOverTriLinear",
                          (100 - round((lossL1.item() / (lossTrilinear.item() + 10e-9) * 1000)) / 10),
                          i + len(trainLoader) * (epoch - 1))
        if lossOrgData > 0:
            writer.add_scalar("frameTrain/percOverOriginal", (100 - round((lossL1.item() / (lossOrgData.item() + 10e-9) * 1000)) / 10), i + len(trainLoader) * (epoch - 1))


        #samples_accum += current_batch_size

        # Status print to see progress of the epoch...
        #if epochProgressPerc + 2 < 100.0 / len(trainLoader) * i:
        #    epochProgressPerc += 2
        #    sys.stdout.write("#")
        #    sys.stdout.flush()

        #pbar.write(i)

        #pbar.update(1)
    #pbar.close()
    logline = []
    if modeForTraining == 'frame':
        #logline = "Epoch: {}, batch-idx: {}, L1: {}, L1Blended: {}, L1Trilinear: {}\n".format(epoch, i, lossL1viz, lossBlendData.item(), lossTrilinear.item())
        logline = "Epoch: {}, batch-idx: {}, L1: {}, L1Blended: {}, L1Trilinear: {}\n".format(epoch, i, L1_accum / len(trainLoader), L1stamp_accum / len(trainLoader), lossTrilinear.item())
    else:
        logline = "Epoch: {}, batch-idx: {}, L1: {}, L1Unlearned: {}, \n".format(epoch, i,lossL1viz,lossOrgData.item() )
    print(logline)

    writer.add_scalar("train/loss", L1_accum / len(trainLoader), epoch)
    writer.add_scalar("train/lr", scheduler.get_lr()[0], epoch)


######### Code To Fill In!!

def trainGenerator(input, target, epoch_loss, discr_loss_gen, mean_gt_logits_gen, mean_pred_logits_gen,
                   lossGenAll, lossBlendedAll,
                   iteration, epoch, lentrainLoader):
    global bPrintOnce1

    if adversarial_training:
        for criterion in criterions:
            if (criterion.discriminator != None):
                criterion.discriminator.eval()
                for p in criterion.discriminator.parameters():
                    p.requires_grad = False
        if numberOfDiscriminators > 1:
            assert (alreadyImplementForMultipleDiscr)

    netG.train()
    gen_optimizer.zero_grad()
    for p in netG.parameters():
        p.requires_grad = True


    # evaluate generator

    prediction = feedThroughNet(input)

    if showDebugInfo:
        print("shape of input")
        print(input.shape)
        print("shape of prediction")
        print(prediction.shape)
        print("shape of target")
        print(target.shape)
        print("prediction variables")
        print(prediction[0,0,:,:])
        print(prediction[0, 1, :, :])
        print(prediction[0, 2, :, :])
    # Clamp the prediction to 0,1
    # prediction = torch.clamp(prediction, 0, 1)

    # evaluate loss
    loss_gen = []
    map = {}
    iCrit = 0
    critChannel = 0
    for criterion in criterions:
        loss_gen_curr = []
        map_curr = []
        if 'adv' in criterion.weight_dict:
            loss_gen_curr, map_curr = criterion(target,
                                                prediction,
                                                input,
                                                None)
        else:
            if showDebugInfo:
                print("loss criterion channel")
                print(critChannel)
                print("number of channels for this criterion")
                print(lcrFC[iCrit])
            if target[:,critChannel:critChannel+lcrFC[iCrit]].shape[1] == 0:
                break
            loss_gen_curr, map_curr = criterion(target[:,critChannel:critChannel+lcrFC[iCrit]],
                                                prediction[:,critChannel:critChannel+lcrFC[iCrit]],
                                                input[:,critChannel:critChannel+lcrFC[iCrit]],
                                                None)

        if iCrit == 0:
            loss_gen = loss_gen_curr
        else:
            loss_gen += loss_gen_curr
        if (criterion.discriminator != None):
            map = map_curr
        critChannel += lcrFC[iCrit]
        iCrit += 1

    epoch_loss += loss_gen
    discr_loss_gen = -1
    iCrit = 0
    if adversarial_training:
        if iCrit == 0 :
            discr_loss_gen = criterion.adv_g_loss
        else:
            discr_loss_gen += criterion.adv_g_loss
        iCrit += 1

    # test
    if adversarial_training:
        mean_gt_logits_gen += map['adv_mean_gt_logits']
        mean_pred_logits_gen += map['adv_mean_pred_logits']

    if showDebugInfo:
        print("current loss")
        print(loss_gen)
        #pdb.set_trace()
    loss_gen.backward()
    gen_optimizer.step()

    if modeForTraining == 'frame':
        blendedInp = torch.mean(losses.LossNet.pad(input[:, 0:2], args.lossBorderPadding).cuda(), 1, True)
        loss0_blended = 0
        iCrit = 0
        critChannel = 0
        for criterion in criterions:
            loss0_blended_curr, _ = criterion(target[:,critChannel:critChannel+lcrFC[iCrit]],
                                              blendedInp[:,critChannel:critChannel+lcrFC[iCrit]],
                                              input[:,critChannel:critChannel+lcrFC[iCrit]],
                                              None)
            if iCrit == 0:
                loss0_blended = loss0_blended_curr

            else:
                loss0_blended += loss0_blended_curr
            critChannel += lcrFC[iCrit]
            iCrit += 1

        lossGenAll[iteration] = loss_gen
        lossBlendedAll[iteration] = loss0_blended


        #writer.add_scalar("train/percOverBlended",
        #                  (100 - round((loss_gen.item() / (loss0_blended.item() + 10e-9) * 1000)) / 10),
        #                  iteration + lentrainLoader * (epoch - 1))
    elif modeForTraining == 'pcrender':
        critChannel = 0
        iCrit = 0
        for criterion in criterions:
            loss0_org_curr = []
            if 'adv' in criterion.weight_dict:
                if target.shape != input.shape:
                    loss0_org_curr = float("NaN")
                else:
                    loss0_org_curr, _ = criterion(target,
                                              input,
                                              input,
                                              None)
            else:
                if critChannel+lcrFC[iCrit] > input.shape[1] or critChannel+lcrFC[iCrit] > target.shape[1]:
                    if not critChannel+lcrFC[iCrit] > target.shape[1]: # There are less channels in the input than in the target, so the l1 for original loss can only be evaluated on the number of input channels
                        if not critChannel > input.shape[1] and iCrit == 0:
                            loss0_org_curr, _ = criterion(target[:, critChannel:input.shape[1]],
                                                          input[:, critChannel:input.shape[1]],
                                                          input[:, critChannel:input.shape[1]],
                                                          None)
                            if bPrintOnce1:
                                print('Criterion on org (input)', criterion.loss_dict, ' applied to ', klri[critChannel:input.shape[1]])
                                print('Criterion on org (target)', criterion.loss_dict, ' applied to ', klrt[critChannel:input.shape[1]])
                        else:
                            break
                    else:
                        break

                loss0_org_curr, _ = criterion(target[:,critChannel:critChannel+lcrFC[iCrit]],
                                                  input[:,critChannel:critChannel+lcrFC[iCrit]],
                                                  input[:,critChannel:critChannel+lcrFC[iCrit]],
                                                  None)

                if bPrintOnce1:
                    print('Criterion on org (input)', criterion.loss_dict, ' applied to ', klri[critChannel:critChannel+lcrFC[iCrit]])
                    print('Criterion on org (target)', criterion.loss_dict, ' applied to ', klrt[critChannel:critChannel+lcrFC[iCrit]])
            if iCrit == 0:
                loss0_org = loss0_org_curr

            else:
                loss0_org += loss0_org_curr
            critChannel += lcrFC[iCrit]
            iCrit += 1

        bPrintOnce1 = False
        lossGenAll[iteration] = loss_gen
        lossBlendedAll[iteration] = loss0_org

    else:#if modeForTraining == 'pcrender':
        lossGenAll[iteration] = loss_gen
        #writer.add_scalar("train/lossIteration",
        #                  loss_gen.item(),
        #                  iteration + lentrainLoader * (epoch - 1))


    return prediction, epoch_loss, discr_loss_gen, mean_gt_logits_gen, mean_pred_logits_gen


def trainDiscriminator(input, target, prediction, predAvailable, discr_loss, mean_gt_logits, mean_pred_logits,
                       iteration, epoch, lentrainLoader):
    for criterion in criterions:
        if (criterion.discriminator != None):
            criterion.discriminator.train()
            for p in criterion.discriminator.parameters():
                p.requires_grad = True
    if numberOfDiscriminators > 1:
        assert (alreadyImplementForMultipleDiscr)

    for p in netG.parameters():
        p.requires_grad = False
    netG.eval()
    # for the Wasserstein GAN, clip the weights
    for criterion in criterions:
        if (criterion.discriminator != None):
            if criterion.discriminator_clip_weights:
                for p in criterion.discriminator.parameters():
                    p.data.clamp_(-args.advDiscrWeightClip, + args.advDiscrWeightClip)

    discr_optimizer.zero_grad()
    loss = 0
    gt_logits = 0
    pred_logits = 0

    prediction = feedThroughNet(input)

    input_images = torch.cat([
        torch.cat([target, input], dim=1),
        torch.cat([prediction.detach(), input], dim=1)], dim=0)

    input_images = losses.LossNet.pad(input_images, args.lossBorderPadding)

    loss0 = 0
    gt_logits0 = 0
    pred_logits0 = 0
    iCrit = 0
    for criterion in criterions:
        if (criterion.discriminator != None):
            _, loss0_curr, gt_logits0_curr, pred_logits0_curr = criterion.adv_loss.train_discr(
                input_images, criterion.discriminator)
            if iCrit == 0:
                loss0 = loss0_curr
                gt_logits0 = gt_logits0_curr
                pred_logits0 = pred_logits0_curr
                iCrit += 1
            else:
                loss0 += loss0_curr
                gt_logits0 += gt_logits0_curr
                pred_logits0 += pred_logits0_curr

    loss += loss0
    discr_loss += loss0.item()
    gt_logits += gt_logits0
    pred_logits += pred_logits0

    mean_gt_logits += gt_logits
    mean_pred_logits += pred_logits

    loss.backward()
    discr_optimizer.step()
    writer.add_scalar("train/updateDiscr", 1, iteration + len(trainLoader) * (epoch - 1))

    return discr_loss, mean_gt_logits, mean_pred_logits


def decideWhetherGenOrDiscr(mean_gt_logits, it):
    gen = True
    discr = True

    if mean_gt_logits / float(it + 1) < args.doNotTrainDiscrIfAboveThreshold:
        discr = True
    elif mean_gt_logits / float(it + 1) > 0.9:
        discr = False

    if mean_gt_logits / float(it + 1) < 0.6:
        gen = False
    else:
        gen = True

    # If none should be trained, train both...
    if not gen and not discr:
        gen = True
        discr = True

    return gen, discr



def trainAdvGenAndDiscr(epoch, epochs):
    print("Starting epoch {} / {}".format((epoch), epochs))

    # Code for discriminator

    if adversarial_training:
        if decayLr:
            discr_scheduler.step()
        writer.add_scalar('train/lr_discr', discr_scheduler.get_lr()[0], epoch)
    num_steps = 0
    mean_gt_logits = 0
    mean_pred_logits = 0
    num_minibatch = len(trainLoader)

    # Code for generator
    if decayLr:
        print(gen_scheduler.get_lr()[0])
        gen_scheduler.step()
    writer.add_scalar('train/lr_gen', gen_scheduler.get_lr()[0], epoch)
    gen_threshold = args.advGenThreshold
    mean_gt_logits_gen = 0
    mean_pred_logits_gen = 0

    num_steps += 1
    epoch_loss = 0
    discr_loss = 0
    discr_loss_gen = 0

    visualizeNet = False
    lentrainLoader = len(trainLoader)

    # Variables used during trainGenerator
    lossGenAll = torch.cuda.FloatTensor(lentrainLoader).fill_(0)
    lossGenAll.cuda()
    lossBlendedAll = torch.cuda.FloatTensor(lentrainLoader).fill_(0)
    lossBlendedAll.cuda()

    generatorCounter = 0
    discrCounter = 0
    for iteration, batch in enumerate(tqdm(trainLoader,0)):
        input, target, idxes, damaged, maskPC = batch[0].float().to(device), batch[1].float().to(device), batch[4], batch[5], batch[6].float().to(device)
        if str2bool(args.checkDataForErrors):
            if max(damaged.data.numpy()) == 1: # This slows down since it has to be transfered to the cpu...
                continue
        # decide, whether to train generator or discriminator or both. Default is Generator training:
        trainGenNow = True
        trainDiscrNow = False

        if adversarial_training:
            trainGenNow, trainDiscrNow = decideWhetherGenOrDiscr(mean_gt_logits, discrCounter)




        prediction = []
        predAvailable = False
        if trainGenNow:
            prediction, epoch_loss, discr_loss_gen, mean_gt_logits_gen, mean_pred_logits_gen = \
                trainGenerator(input, target, epoch_loss, discr_loss_gen, mean_gt_logits_gen, mean_pred_logits_gen,
                               lossGenAll, lossBlendedAll,
                                iteration, epoch, lentrainLoader)
            predAvailable = True
            generatorCounter += 1
        if not trainDiscrNow and adversarial_training:
            mean_gt_logits += mean_gt_logits_gen/(iteration+1)
        if trainDiscrNow:
            discr_loss, mean_gt_logits, mean_pred_logits = \
                trainDiscriminator(input, target, prediction, predAvailable, discr_loss, mean_gt_logits, mean_pred_logits,
                       iteration, epoch, lentrainLoader)
            discrCounter += 1




        # Network can be exported as .pdf to debug
        if visualizeNet == True:
            dot = torchviz.make_dot(netG(input), params=dict(netG.named_parameters()))
            dot.render()
            visualizeNet = False

    if generatorCounter == 0:
        generatorCounter = - 0.001
    epoch_loss /= generatorCounter
    discr_loss_gen /= generatorCounter
    mean_gt_logits_gen /= generatorCounter
    mean_pred_logits_gen /= generatorCounter
    print("---> Train Generator {}: loss {:.7f}, num steps {}".format(num_steps, discr_loss_gen, generatorCounter))
    print("     mean_gt_score_gen={:.5f}, mean_pred_score_gen={:.5f}".format(mean_gt_logits_gen, mean_pred_logits_gen))

    if discrCounter == 0:
        discrCounter = - 0.001

    discr_loss /= discrCounter
    mean_gt_logits /= discrCounter
    mean_pred_logits /= discrCounter
    print("---> Train Discriminator {}: loss {:.7f}, num steps {}".format(num_steps, discr_loss, discrCounter))
    print("     mean_gt_score={:.5f}, mean_pred_score={:.5f}".format(mean_gt_logits, mean_pred_logits))


    # Add all the losses of the single iterations:
    lossGenAll_cpu = lossGenAll.detach().cpu().numpy()
    lossBlendedAll_cpu = lossBlendedAll.detach().cpu().numpy()


    for i in range(0, lentrainLoader):
        writer.add_scalar("train/percOverBlended",
                          (100 - round((lossGenAll_cpu[i] / (lossBlendedAll_cpu[i] + 10e-9) * 1000)) / 10),
                          i + lentrainLoader * (epoch - 1))
        writer.add_scalar("train/lossIteration",
                          lossGenAll_cpu[i],
                          i + lentrainLoader * (epoch - 1))


    writer.add_scalar("train/gen_loss", discr_loss_gen, epoch)
    writer.add_scalar('train/gen_steps', num_steps, epoch)

    writer.add_scalar("train/discr_loss", discr_loss, epoch)
    writer.add_scalar("train/discr_steps", num_steps, epoch)
    writer.add_scalar("train/mean_gt_logits", mean_gt_logits, epoch)
    writer.add_scalar("train/mean_pred_logits", mean_pred_logits, epoch)
    print("===> Epoch {} Complete".format(epoch))





def netOnValidation(epoch, epochs, figuresExport = True):
    global bPrintOnce2

    netG.eval()
    L1val_accum = 0.0
    L1valstamp_accum = 0.0
    lossL1Depth_accum = 0.0
    lentrainLoader = len(valiLoader)
    for i, validata in enumerate(valiLoader, 0):
        inputs_cpu, targets_cpu, usedXRange, usedYRange, idxes, damaged, maskPCVal_cpu = validata
        input, target, maskPCVal = validata[0].float().to(device), validata[1].float().to(device), validata[6].float().to(device)

        if max(damaged.cpu().data.numpy()) == 1: # Slowdown due to cpu transfer
           continue

        #target, input = targets_cpu.float().cuda(), inputs_cpu.float().cuda()

        if modeForTraining == 'frame':
            blendedInp = torch.mean(input[:, 0:2], 1, True)

        #torch.cuda.empty_cache()
        prediction = feedThroughNet(input)

        outputs_cpu = prediction.cpu().data.numpy()
        lossL1 = []
        if  modeForTraining == 'pcrender':
            lossL1 = criterionL1(prediction[:,channelsL1LossRangeStart:channelsL1LossRangeEnd], target[:,channelsL1LossRangeStart:channelsL1LossRangeEnd])
            if bPrintOnce2:
                print('L1 (pred/GT) applied to ', klrt[channelsL1LossRangeStart:channelsL1LossRangeEnd])

            # Compute L1 loss for depth only

            lossL1Depth = criterionL1(prediction[:, tarDepLoc:tarDepLoc+1], target[:, tarDepLoc:tarDepLoc+1])
            if bPrintOnce2:
                print('keys to load ', keysToLoad)
                print('tarDepLoc ', tarDepLoc)
                print('L1 Depth (pred/GT) applied to ', klrt[tarDepLoc:tarDepLoc+1])

        else:
            lossL1 = criterionL1(prediction, target)

        bPrintOnce2 = False
        L1val_accum += lossL1.item()
        lossL1Depth_accum += lossL1Depth.item()

        loss_gen = 0
        map = {}
        iCrit = 0
        critChannel = 0
        for criterion in criterions:
            loss_gen_curr = []
            map_curr = []
            if 'adv' in criterion.weight_dict:
                loss_gen_curr, map_curr = criterion(target,
                                                    prediction,
                                                    input,
                                                    None)
            else:
                loss_gen_curr, map_curr = criterion(target[:,critChannel:critChannel+lcrFC[iCrit]],
                                                    prediction[:,critChannel:critChannel+lcrFC[iCrit]],
                                                    input[:,critChannel:critChannel+lcrFC[iCrit]],
                                                    None)
            if iCrit == 0:
                loss_gen = loss_gen_curr
            else:
                loss_gen += loss_gen_curr
            if (criterion.discriminator != None):
                map = map_curr
            critChannel += lcrFC[iCrit]
            iCrit += 1

        writer.add_scalar('frameVali/lossCriterion', loss_gen, i + lentrainLoader * (epoch - 1))

        lossOrgData = []
        lossBlendData = []
        blendedAvailable = False

        # input velocities
        if modeForTraining == 'stamped vec':
            inputsStamp = input[:, [0, 1], :, :]
            lossOrgData = criterionL1(inputsStamp, target)
            L1valstamp_accum += lossOrgData.item()
        elif modeForTraining == 'noise poly' or modeForTraining == 'pcrender':
            # If the program crashes here, it means that the L1 should not only be on depth, but the order of channels is different in input and target
            lossOrgData = criterionL1(input[:,channelsL1LossRangeStartInp:channelsL1LossRangeEndInp], target[:,channelsL1LossRangeStart:channelsL1LossRangeEnd])
            L1valstamp_accum += lossOrgData.item()
        elif modeForTraining == 'frame':
            lossBlendData = criterionL1(blendedInp, target)
            blendedAvailable = True
            L1valstamp_accum += lossBlendData.item()


        if i == len(valiLoader)-1:
            input_ndarray = inputs_cpu.numpy()[0:1]

            outputs_denormalized = []
            targets_denormalized = []
            if modeForTraining.__contains__('vec'):
                v_norm = (np.max(np.abs(input_ndarray[0, 0, :, :])) ** 2 + np.max(
                    np.abs(input_ndarray[0, 1, :, :])) ** 2) ** 0.5
                outputs_denormalized = data.denormalize(outputs_cpu[0:1], v_norm, idxes)
                targets_denormalized = data.denormalize(targets_cpu.numpy()[0:1], v_norm, idxes)
                inputs_denormalized = data.denormalize(input_ndarray, 1, idxes)
            elif modeForTraining.__contains__('frame') or modeForTraining == 'pcrender':
                outputs_denormalized = data.denormalizeFrame(outputs_cpu, idxes.cpu().data.numpy(), 'tar')
                targets_denormalized = data.denormalizeFrame(targets_cpu.cpu().data.numpy(), idxes.cpu().data.numpy(), 'tar')

            else:
                v_norm = np.max(np.abs(input_ndarray[0, :, :]))
                outputs_denormalized = data.denormalizePoly(outputs_cpu, v_norm)
                targets_denormalized = data.denormalizePoly(targets_cpu.numpy(), v_norm)

            # create a plot to see how the denoised / reconstructed field looks like
            # print("epoch{} i {} validLoaderLength {}".format(epoch, len(valiLoader), i))
            # if epoch == epochs-1 and i == 0:

            if modeForTraining == 'noise poly':
                plt.subplot(3, 1, 1)
                plt.pcolormesh(inputs_cpu[0, 0, :, :])
                plt.subplot(3, 1, 2)
                plt.pcolormesh(outputs_cpu[0, 0, :, :])
                plt.subplot(3, 1, 3)
                plt.pcolormesh(targets_cpu[0, 0, :, :])
                plt.show()

            elif modeForTraining == 'frame':
                inputs_denormalized_n = dataValidation.getRawInputValidation(idxes.data.numpy(),
                                                                             torch.stack(usedXRange),
                                                                             torch.stack(usedYRange))

                inputs_warped_denormalized = data.denormalizeFrame(inputs_cpu.numpy(), idxes.data.numpy(), 'tar')

                if figuresExport:
                    currMin = np.min(inputs_denormalized_n)
                    currMax = np.max(inputs_denormalized_n)
                    imgInp1Vals =  np.moveaxis(utils.colorize(inputs_denormalized_n[0, 0:1, :, :], vmin = currMin, vmax = currMax),2,0)
                    imgInp2Vals = np.moveaxis(utils.colorize(inputs_denormalized_n[0, 1:2, :, :], vmin=currMin, vmax=currMax), 2, 0)
                    imgOutVals = np.moveaxis(utils.colorize(outputs_denormalized[0, 0:1, :, :], vmin = currMin, vmax = currMax),2,0)
                    imgTarVals = np.moveaxis(utils.colorize(targets_denormalized[0, 0:1, :, :], vmin = currMin, vmax = currMax),2,0)


                    writer.add_image('frameVali_Input/input0%03d'%(i), utils.insertBoundaryInImg(imgInp1Vals, currMin, lossBorder, drawLossBorder), epoch)
                    writer.add_image('frameVali_Input/input1%03d' % (i), utils.insertBoundaryInImg(imgInp2Vals, currMin, lossBorder, drawLossBorder), epoch)
                    writer.add_image('frameVali_Output/output0%03d' % (i), utils.insertBoundaryInImg(imgOutVals, currMin, lossBorder, drawLossBorder), epoch)
                    writer.add_image('frameVali_Target/target0%03d' % (i), utils.insertBoundaryInImg(imgTarVals, currMin, lossBorder, drawLossBorder), epoch)


                writer.add_scalar('frameVali/lossL1', L1val_accum / len(valiLoader), epoch)
                writer.add_scalar('frameVali/lossBlended', L1valstamp_accum / len(valiLoader), epoch)
                writer.add_scalar('frameVali/percOverLinear', round(100 - (((L1val_accum / len(valiLoader)) / (L1valstamp_accum / len(valiLoader) + 10e-9)) * 1000) / 10) ,epoch)

                if figuresExport:
                    if exportAsPDF or exportAsPNG:
                        utils.saveFrameTestImages(modelNameOut, figsDirectory, exportAsPDF, exportAsPNG,
                                          inputs_denormalized_n[0:1, 0:2, :, :],
                                          outputs_denormalized[0:1, 0:1, :, :],
                                          targets_denormalized[0:1, 0:1, :, :],
                                          inputs_warped_denormalized[0:1, 0:2, :, :])
            elif modeForTraining == 'pcrender':

                inputs_denormalized_n = data.denormalizeFrame(inputs_cpu.cpu().data.numpy(), idxes.cpu().data.numpy(), 'inp')
                if figuresExport:
                    # Correct everything here. Only the magnitude of the normal should be plotted in the beginning... make if for depth, normals etc.
                    currChannel = 0
                    if data.useNormals:
                        #inputNormalsMagnitude = np.sqrt(np.sum(pow(inputs_denormalized_n[:, currChannel:currChannel+1,:,:],2), axis=1))
                        #currMin = 0# np.min(inputNormalsMagnitude[0])
                        #currMax = 1# np.max(inputNormalsMagnitude[0])
                        #imgInpNormm = np.moveaxis(utils.colorize(inputNormalsMagnitude[0], vmin=currMin, vmax=currMax), 2, 0)
                        # writer.add_image('frameVali/input_mnormal0%03d' % (i), utils.insertBoundaryInImg(imgInpNormm, currMin, lossBorder, drawLossBorder), epoch)
                        writer.add_image('frameVali_Input/input_mnormal0%03d' % (i), utils.normalsToRGB(inputs_denormalized_n[:, currChannel:currChannel+3,:,:], lossBorder, drawLossBorder), epoch)
                        currChannel += 3
                    if data.useDepth:
                        currMin = np.min(inputs_denormalized_n[0,currChannel])
                        currMax = np.max(inputs_denormalized_n[0,currChannel])
                        imgInpDepths = np.moveaxis(utils.colorize(inputs_denormalized_n[0, currChannel], vmin=currMin, vmax=currMax), 2, 0)
                        writer.add_image('frameVali_Input/input_depth0%03d' % (i), utils.insertBoundaryInImg(imgInpDepths, currMin, lossBorder, drawLossBorder), epoch)
                        currChannel += 1

                    currChannel = 0
                    if data.useTargetNormals:
                        #targetNormalsMagnitude = np.sqrt(np.sum(pow(targets_denormalized[:, currChannel:currChannel+1,:,:],2), axis=1))
                        #currMin = 0# np.min(targetNormalsMagnitude[0])
                        #currMax = 1# np.max(targetNormalsMagnitude[0])
                        #imgTarNormm =  np.moveaxis(utils.colorize(targetNormalsMagnitude[0], vmin=currMin, vmax=currMax), 2, 0)
                        #writer.add_image('frameVali/target_mnormal0%03d' % (i), utils.insertBoundaryInImg(imgTarNormm, currMin, lossBorder, drawLossBorder), epoch)
                        writer.add_image('frameVali_Target/target_mnormal0%03d' % (i), utils.normalsToRGB(targets_denormalized[:, currChannel:currChannel + 3, :, :], lossBorder, drawLossBorder), epoch)
                        currChannel += 3
                    if data.useTargetDepth:
                        currMin = np.min(targets_denormalized[0,currChannel])
                        currMax = np.max(targets_denormalized[0,currChannel])
                        imgTarDepths = np.moveaxis(utils.colorize(targets_denormalized[0, currChannel], vmin=currMin, vmax=currMax), 2, 0)
                        writer.add_image('frameVali_Target/target_depth0%03d' % (i), utils.insertBoundaryInImg(imgTarDepths, currMin, lossBorder, drawLossBorder), epoch)
                        currChannel += 1
                    currChannel = 0
                    if data.useTargetNormals:
                        #outputNormalsMagnitude = np.sqrt(np.sum(pow(outputs_denormalized[:, currChannel:currChannel+1,:,:],2), axis=1))
                        #currMin = 0# np.min(outputNormalsMagnitude[0])
                        #currMax = 1# np.max(outputNormalsMagnitude[0])
                        #imgOutNormm = np.moveaxis(utils.colorize(outputNormalsMagnitude[0], vmin=currMin, vmax=currMax), 2, 0)
                        #writer.add_image('frameVali/output_mnormal0%03d' % (i), utils.insertBoundaryInImg(imgOutNormm, currMin, lossBorder, drawLossBorder), epoch)
                        writer.add_image('frameVali_Output/output_mnormal0%03d' % (i), utils.normalsToRGB(outputs_denormalized[:, currChannel:currChannel + 3, :, :], lossBorder, drawLossBorder), epoch)
                        currChannel += 3
                    if data.useTargetDepth:
                        currMin = np.min(outputs_denormalized[0,currChannel])
                        currMax = np.max(outputs_denormalized[0,currChannel])
                        imgOutDepths =  np.moveaxis(utils.colorize(outputs_denormalized[0, currChannel], vmin=currMin, vmax=currMax), 2, 0)
                        writer.add_image('frameVali_Output/output_depth0%03d' % (i), utils.insertBoundaryInImg(imgOutDepths, currMin, lossBorder, drawLossBorder), epoch)
                        currChannel += 1

                writer.add_scalar('frameVali/lossL1', L1val_accum / len(valiLoader), epoch)
                writer.add_scalar('frameVali/lossL1_Depth', lossL1Depth_accum / len(valiLoader), epoch)



            elif modeForTraining == 'stamped vec':
                # inputs_denormalized[0,0,0,0:10]
                utils.saveVectorfieldAsImage(inputs_denormalized[0], outputs_denormalized[0], targets_denormalized[0],
                                             figsDirectory,
                                             gridSizex, gridSizey, exportAsPDF, exportAsPNG)

        del prediction
        # The following code is only used for terminal printouts.
        logline2 = []
        if i == len(valiLoader) - 1:
            # Multiplying by 1000 and rounding preserves 1 digit.
            lossPercent = []
            if blendedAvailable:
                lossPercent = (100 - round(lossL1.item() / (lossBlendData.item() + 10e-9) * 1000) / 10)
            else:
                lossPercent = (100 - round(lossL1.item() / (lossOrgData.item() + 10e-9) * 1000) / 10)

            lossPercentAverage = (100 - round(L1val_accum / len(valiLoader) / (L1valstamp_accum / len(valiLoader) + 10e-9)*1000) / 10)
            logline = []
            if modeForTraining == 'frame':
                logline = "Epoch: {}, L1Vali: {}, L1Blended: {}, improved by: {}, In percent: {} \n".format(
                    epoch, lossL1.item(), lossBlendData.item(), lossBlendData.item() - lossL1.item(),
                    lossPercent)
                logline2 = "Epoch: {}, L1ValiAverage: {}, L1ValiLinearBlendAverage: {}, improved by: {}, In percent: {} \n".format(
                    epoch, L1val_accum / len(valiLoader), L1valstamp_accum / len(valiLoader),
                           L1valstamp_accum / len(valiLoader) - L1val_accum / len(valiLoader),
                    lossPercentAverage)

            else:
                logline = "Epoch: {}, L1Vali: {}, L1ValiUnlearned: {}, improved by: {}, In percent: {} \n".format(
                    epoch, lossL1.item(), lossOrgData.item(), lossOrgData.item() - lossL1.item(),
                    lossPercent)
            print(logline)
            if logline2 != []:
                print(logline2)
        # print('Check whether model is output with jit')
        exportModelAsJit = ((epoch % checkpointEveryNThEpoch) == 0) or (epoch ==  epochs + restoreEpoch)
        # if exportModelAsJit:
        #     # print('Model is output with jit')
        #     scripted_module = torch.jit.trace(netG, input)
        #     model_jit_out_path = os.path.join(modeldir, "{}_epoch_{}_jit.pt".format(modelNameOut, epoch))
        #     torch.jit.save(scripted_module, model_jit_out_path)



def testModel(epoch, epochs, figuresExport = True):
    netG.eval()
    L1val_accum = 0.0
    L1valstamp_accum = 0.0
    for i, testdata in enumerate(testLoader, 0):
        inputs_cpu, targets_cpu, usedXRange, usedYRange, idxes, damaged = testdata
        if max(damaged.data.numpy()) == 1:
            continue
        current_batch_size = targets_cpu.size(0)

        targets, inputs = targets_cpu.float().cuda(), inputs_cpu.float().cuda()

        blendedInp = []
        if modeForTraining == 'frame':
            blendedInp = torch.mean(inputs[:, 0:2], 1, True)

        outputs = feedThroughNet(inputs)
        outputs_cpu = outputs.data.cpu().numpy()

        lossL1 = criterionL1(outputs, targets)
        L1val_accum += lossL1.item()
        lossBlendData = []

        # input velocities
        lossOrgData = []
        if modeForTraining == 'stamped vec':
            inputsStamp = inputs[:, [0, 1], :, :]
            lossOrgData = criterionL1(inputsStamp, targets)
            L1valstamp_accum += lossOrgData.item()
        elif modeForTraining == 'noise poly':
            lossOrgData = criterionL1(inputs, targets)
            L1valstamp_accum += lossOrgData.item()
        elif modeForTraining == 'frame':
            #lossOrgData = criterionL1(zerosOrOnes, targets)
            lossBlendData = criterionL1(blendedInp, targets)
            L1valstamp_accum += lossBlendData.item()


        input_ndarray = inputs_cpu.numpy()[0:1]

        outputs_denormalized = []
        targets_denormalized = []
        if modeForTraining.__contains__('vec'):
            v_norm = (np.max(np.abs(input_ndarray[0, 0, :, :])) ** 2 + np.max(
                np.abs(input_ndarray[0, 1, :, :])) ** 2) ** 0.5
            outputs_denormalized = data.denormalize(outputs_cpu[0:1], v_norm, idxes)
            targets_denormalized = data.denormalize(targets_cpu.numpy()[0:1], v_norm, idxes)
            inputs_denormalized = data.denormalize(input_ndarray, 1, idxes)
        elif modeForTraining.__contains__('frame'):
            outputs_denormalized = data.denormalizeFrame(outputs_cpu, idxes.data.numpy(), 'tar')
            targets_denormalized = data.denormalizeFrame(targets_cpu.numpy(), idxes.data.numpy(), 'tar')

        else:
            v_norm = np.max(np.abs(input_ndarray[0, :, :]))
            outputs_denormalized = data.denormalizePoly(outputs_cpu, v_norm)
            targets_denormalized = data.denormalizePoly(targets_cpu.numpy(), v_norm)

        # create a plot to see how the denoised / reconstructed field looks like
        if modeForTraining == 'noise poly':
            plt.subplot(3, 1, 1)
            plt.pcolormesh(inputs_cpu[0, 0, :, :])
            plt.subplot(3, 1, 2)
            plt.pcolormesh(outputs_cpu[0, 0, :, :])
            plt.subplot(3, 1, 3)
            plt.pcolormesh(targets_cpu[0, 0, :, :])
            plt.show()

        elif modeForTraining == 'frame':
            inputs_denormalized_n = dataTest.getRawInputValidation(idxes.data.numpy(),
                                                                         torch.stack(usedXRange),
                                                                         torch.stack(usedYRange))

            inputs_warped_denormalized = data.denormalizeFrame(inputs_cpu.numpy(), idxes.data.numpy(), 'tar')

            if figuresExport:
                currMin = np.min(inputs_denormalized_n)
                currMax = np.max(inputs_denormalized_n)
                writer.add_image('frameTest/input0', np.moveaxis(utils.colorize(inputs_denormalized_n[0, 0:1, :, :], vmin = currMin, vmax = currMax),2,0), i)
                writer.add_image('frameTest/input1', np.moveaxis(utils.colorize(inputs_denormalized_n[0, 1:2, :, :], vmin = currMin, vmax = currMax),2,0), i)
                writer.add_image('frameTest/output0', np.moveaxis(utils.colorize(outputs_denormalized[0, 0:1, :, :], vmin = currMin, vmax = currMax),2,0), i)
                writer.add_image('frameTest/target0', np.moveaxis(utils.colorize(targets_denormalized[0, 0:1, :, :], vmin = currMin, vmax = currMax),2,0), i)


            writer.add_scalar('frameTest/loss', lossL1.item() , i)
            writer.add_scalar('frameTest/lossBlended', lossBlendData.item(), i)
            writer.add_scalar('frameTest/percOverLinear', round(100 - (((lossL1.item() ) / (lossBlendData.item() + 10e-9)) * 1000) / 10) ,i)

            if figuresExport:
                if exportAsPDF or exportAsPNG:
                    utils.saveFrameTestImages(modelNameOut, figsDirectory, exportAsPDF, exportAsPNG,
                                      inputs_denormalized_n[0:1, 0:2, :, :],
                                      outputs_denormalized[0:1, 0:1, :, :],
                                      targets_denormalized[0:1, 0:1, :, :],
                                      inputs_warped_denormalized[0:1, 0:2, :, :])

        elif modeForTraining == 'stamped vec':
            # inputs_denormalized[0,0,0,0:10]
            utils.saveVectorfieldAsImage(inputs_denormalized[0], outputs_denormalized[0], targets_denormalized[0],
                                         figsDirectory,
                                         gridSizex, gridSizey, exportAsPDF, exportAsPNG)


        # The following code is only used for terminal printouts.
        logline2 = []
        if i == len(testLoader) - 1:
            # Multiplying by 1000 and rounding preserves 1 digit.
            lossPercent = (100 - round(lossL1.item() / (lossBlendData.item() + 10e-9) * 1000) / 10)
            lossPercentAverage = (100 - round(L1val_accum / len(testLoader) / (L1valstamp_accum / len(testLoader) + 10e-9)*1000) / 10)
            logline = []
            if modeForTraining == 'frame':
                logline = "Epoch: {}, L1Vali: {}, L1Blended: {}, improved by: {}, In percent: {} \n".format(
                    epoch, lossL1.item(), lossBlendData.item(), lossBlendData.item() - lossL1.item(),
                    lossPercent)
                logline2 = "Epoch: {}, L1ValiAverage: {}, L1ValiLinearBlendAverage: {}, improved by: {}, In percent: {} \n".format(
                    epoch, L1val_accum / len(testLoader), L1valstamp_accum / len(testLoader),
                           L1valstamp_accum / len(testLoader) - L1val_accum / len(testLoader),
                    lossPercentAverage)

            else:
                logline = "Epoch: {}, L1Vali: {}, L1ValiUnlearned: {}, improved by: {}, In percent: {} \n".format(
                    epoch, lossL1.item(), lossOrgData.item(), lossOrgData.item() - lossL1.item(),
                    lossPercent)
            print(logline)
            if logline2 != []:
                print(logline2)



# This function alters the parameters in a way that training can be continued right away.
def writeStrToContinueTrainingOrTest(epoch, wmode = 'train'):
    strCont = ""
    strContExpo = ""
    for i in args.__dict__:
        currItem = i
        currVal = args.__dict__[i]

        if currItem == 'loadModel':
            currVal = True
        elif currItem == 'runNumber':
            currVal = nextRunNumber
        elif currItem == 'restoreEpoch':
            currVal = epoch

        if wmode == 'test':
            if currItem == runModelToTest:
                currVal = True

        strCont += "--{} {} ".format(currItem, currVal)
        strContExpo += "export {}={}  \n".format(currItem, currVal)

    writer.add_text('continue training', strCont, epoch)
    writer.add_text('continue training export', strCont, epoch)

def prepareVariables(epoch):
    trainOnlyDiscriminator = False
    if str2bool(args.trainOnlyDiscriminator):
        # Only train discriminator then
        trainOnlyDiscriminator = str2bool(args.trainOnlyDiscriminator)
        return trainOnlyDiscriminator
    elif args.pretrainDiscriminatorForNEpochs > 0:
        if args.pretrainDiscriminatorForNEpochs > epoch:
            trainOnlyDiscriminator=True
        else:
            trainOnlyDiscriminator=False
    return trainOnlyDiscriminator



###########################
### Start Training ########
print('===> Start Training')
startEpochNr = max(1, restoreEpoch +1)
if not adversarial_training:
    for epoch in range(startEpochNr, epochs + 1 + restoreEpoch):
        if not runModelToTest:
            #train(epoch, epochs)
            ###### Start of training of GANs ########
            trainAdvGenAndDiscr(epoch, epochs)

            # Create a checkpoint every n-th epoch.
            exportCheckpoint = ((epoch % checkpointEveryNThEpoch) == 0) or (epoch ==  epochs + restoreEpoch)
            if exportCheckpoint:
                checkpoint(epoch)
                writeStrToContinueTrainingOrTest(epoch, 'train')
            if args.maxValFiles > 0:
                with torch.no_grad():
                    netOnValidation(epoch,epochs, exportFigsToTensorFlowInEachEpoch)

        else:
            testModel(epoch, epochs)
            break
    if not runModelToTest:
        writeStrToContinueTrainingOrTest(epoch, 'test')
else:
    for epoch in range(startEpochNr, epochs + 1 + restoreEpoch):
        if not runModelToTest:

            trainOnlyDiscriminator = prepareVariables(epoch - startEpochNr)

            trainAdvGenAndDiscr(epoch, epochs)

            exportCheckpoint = ((epoch % checkpointEveryNThEpoch) == 0) or (epoch == epochs + restoreEpoch)
            if exportCheckpoint:
                checkpoint(epoch)
                writeStrToContinueTrainingOrTest(epoch, 'train')
            if args.maxValFiles > 0:
                with torch.no_grad():
                    netOnValidation(epoch, epochs, exportFigsToTensorFlowInEachEpoch)
        else:
            print('The Testing of a model still has to be implemented...')



if False: # This is the old code to save a model
    state = {'state_dict': netG.state_dict(), 'optimizer': optimizerG.state_dict()}
    if modelNameOut != []:
        torch.save(state, modelNameOut)
        print("Saved the model as: " + str(modelNameOut))
    else:
        torch.save(state, prefix + "modelG" )
        print("Saved the model as: " + str(prefix + "modelG"))

writer.close()




