####
# Alexander Kumpf
# Generates a 2D scalar field based on polynomial functions of given degree
# Files can be exported
#
#




import numpy as np
import string
import random

from matplotlib import pyplot as plt


class PolySim:
    def __init__(self,data = [], dataNoise = [], nx = 128, ny = 128, xmin = 0, xmax = 1, ymin = 0, ymax = 1):
        self.data = data
        self.bNoise = True
        self.dataNoise = dataNoise
        self.noiseFrequency = 10

        self.noiseLev = 0.2

        self.nx = nx
        self.ny = ny
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax

        self.dim = -1

        self.letters = random.choice(string.ascii_letters) + random.choice(string.ascii_letters)
        self.id = 0

        self.export = False
        self.exportPath = ''

    def setXYMinMax(self,xmin,xmax, ymin,ymax):
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
    def setExport(self, export = False, exportPath = ''):
        self.export = export
        self.exportPath = exportPath

    def setActivateNoise(self, activateNoise = False):
        self.bNoise = activateNoise

    def setNoiseLevel(self, noiselev = 0.2):
        self.noiseLev = noiselev

    def setNoiseFrequency(self, frequency = 10):
        self.noiseFrequency = frequency


    def writeToFile(self, data, dataNoise = []):
        file = "polyDeg" + str(self.dim) + "_" + self.letters + "%d" % self.id
        self.id = self.id + 1
        filename = self.exportPath + file
        if (len(dataNoise) == 0):
            np.savez_compressed(filename, data)
        else:
            np.savez_compressed(filename, inp = dataNoise, out = data)


    def addNoise1(self, data):
        i = 0
        j = 0
        idx = 0
        self.dataNoise = np.copy(self.data)
        xshape = data.shape[0]
        yshape = data.shape[1]
        while (i < data.shape[0] and j < data.shape[1]):
            # Random a number l. Every lth number is variated. ## (not yet) l stays constant for repeat times.
            idx += round(random.uniform(1,self.noiseFrequency))
            #repeat = round(random.uniform(1,10))
            i = idx % xshape
            j = idx // xshape
            if (j < yshape):
                self.dataNoise[i,j] += random.gauss(0, self.noiseLev)





    # generates a grid, calls for random coefficients for a polynomial and samples this at given grid points.
    # If requested, fields are written to files.
    def generate2DScalarFieldFromPolynomial(self, dim, nSamples = 1, normalize = True, random = 0):
        self.dim = dim
        coefficients = self.generateCoefficients(dim)
        print("%d\n" , coefficients[0])

        x = np.linspace(self.xmin, self.xmax, self.nx)
        y = np.linspace(self.ymin, self.ymax, self.ny)
        xv, yv = np.meshgrid(x, y)

        for i in range (0, nSamples):
            coefficients = self.generateCoefficients(dim)
            self.data = np.polynomial.polynomial.polyval2d(xv,yv,coefficients)
            #print("%d\n", self.data[0][0])
            if normalize:
                max = np.max(self.data)
                min = np.min(self.data)
                self.data = (self.data - min) / (max - min)
            if self.bNoise:
                self.addNoise1(self.data)
            if self.export:
                self.writeToFile(self.data, self.dataNoise)

            print("%d\n", i)



    def generateCoefficients(self, dim):
        return np.random.rand(dim + 1, 2)*2 -1





######### Code to copy in the end ##############
if True:
    poly1 = PolySim()
    dimension = 4
    poly1.setExport(True, "data/Polynoms/noNoise/")
    rand1 = abs(random.normalvariate(0,0.2) )
    rand1=0
    print(str(rand1))
    poly1.setNoiseLevel(rand1)
    rand2 = abs(round(random.normalvariate(0,1000) ))
    print(str(rand2))
    poly1.setNoiseFrequency(rand2)
    poly1.generate2DScalarFieldFromPolynomial(dimension, 1000, True)

#################################################


if False:
    poly1 = PolySim()

    dimension = 8

    #poly1.setExport(True, "data/Polynoms/")
    colorMin = -10
    colorMax = 10

    poly1.setXYMinMax(-2,2,-2,2)
    poly1.generate2DScalarFieldFromPolynomial(dimension, 1, True)
    plt.subplot(2,2,1)
    plt.pcolormesh(poly1.data, vmin=colorMin, vmax=colorMax)

    poly1.setXYMinMax(-15, 15, -15, 15)
    poly1.generate2DScalarFieldFromPolynomial(dimension, 1, True)
    plt.subplot(2,2,2)
    plt.pcolormesh(poly1.data,  vmin=colorMin, vmax=colorMax)

    poly1.setXYMinMax(-.025, .025, -.025, .025)
    poly1.generate2DScalarFieldFromPolynomial(dimension, 1, True)
    plt.subplot(2,2,3)
    plt.pcolormesh(poly1.data,  vmin=colorMin, vmax=colorMax)

    poly1.setXYMinMax(-0.5, 0.50, -.50, .50)
    poly1.generate2DScalarFieldFromPolynomial(dimension, 1, True)
    plt.subplot(2,2,4)
    plt.pcolormesh(poly1.data,  vmin=colorMin, vmax=colorMax)
    plt.show()