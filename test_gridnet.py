
# Code to create and run a model to check whether the construction of it works.


import os, sys, random
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torch.optim as optim
import matplotlib.pyplot as plt

from DfpNet import TurbNetG, weights_init
from VecLearnNet import TurbNetGVec
from VecLearnNet import TurbNetGVecPoly
import dataset
import utils
#from tensorboardX import SummaryWriter

data = dataset.FrameDataset()

from GridNetUtils import GridNetFrame as gnf

from importlib import reload
import sys

#reload(sys.modules[gnf.__module__])

netG = gnf()

netG.cuda()

dataInput =np.zeros((1,2,128,128))
#dataInput =
dataInput2 =np.zeros((1,2,128,128))
#dataInput =np.zeros((128,128))

#targets = Variable(torch.FloatTensor(1, 1, 128, 128))
#inputs = Variable(torch.FloatTensor(1, 1, 128, 128))
inputs = torch.tensor(dataInput)
inputs_prepared = Variable(inputs.float().cuda())

inputs2 = torch.tensor(dataInput2)
inputs2_prepared = Variable(inputs2.float().cuda())
netG.eval()


output = netG(inputs_prepared)