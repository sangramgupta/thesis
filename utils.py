################
#
# Deep Flow Prediction - N. Thuerey, K. Weissenov, H. Mehrotra, N. Mainali, L. Prantl, X. Hu (TUM)
#
# Helper functions for image output
#
################

import math, re, os
import numpy as np
from PIL import Image
from matplotlib import cm
import matplotlib
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
from torch.autograd import Variable


# add line to logfiles
def log(file, line, doPrint=True):
    f = open(file, "a+")
    f.write(line + "\n")
    f.close()
    if doPrint: print(line)


# reset log file
def resetLog(file):
    f = open(file, "w")
    f.close()


# compute learning rate with decay in second half
def computeLR(i, epochs, minLR, maxLR):
    if i < epochs * 0.5:
        return maxLR
    e = (i / float(epochs) - 0.5) * 2.
    # rescale second half to min/max range
    fmin = 0.
    fmax = 6.
    e = fmin + e * (fmax - fmin)
    f = math.pow(0.5, e)
    return minLR + (maxLR - minLR) * f

# exponential decay learning rate
def exp_decay(lrG, lrExFak, epoch):
   return lrG * math.exp(-lrExFak*epoch)


def mapValueToColor(minVal, maxVal, val):
    mappedVal = (val - minVal) / (maxVal - minVal)

    return np.maximum(0, np.minimum(1, mappedVal))


# image output
def imageOut(filename, _outputs, _targets, saveTargets=False, normalize=False, saveMontage=True):
    outputs = np.copy(_outputs)
    targets = np.copy(_targets)

    s = outputs.shape[1]  # should be 128
    if saveMontage:
        new_im = Image.new('RGB', ((s + 10) * 3, s * 2), color=(255, 255, 255))
        BW_im = Image.new('RGB', ((s + 10) * 3, s * 3), color=(255, 255, 255))

    for i in range(3):
        outputs[i] = np.flipud(outputs[i].transpose())
        targets[i] = np.flipud(targets[i].transpose())
        min_value = min(np.min(outputs[i]), np.min(targets[i]))
        max_value = max(np.max(outputs[i]), np.max(targets[i]))
        if normalize:
            outputs[i] -= min_value
            targets[i] -= min_value
            max_value -= min_value
            outputs[i] /= max_value
            targets[i] /= max_value
        else:  # from -1,1 to 0,1
            outputs[i] -= -1.
            targets[i] -= -1.
            outputs[i] /= 2.
            targets[i] /= 2.

        if not saveMontage:
            suffix = ""
            if i == 0:
                suffix = "_pressure"
            elif i == 1:
                suffix = "_velX"
            else:
                suffix = "_velY"

            im = Image.fromarray(cm.magma(outputs[i], bytes=True))
            im = im.resize((512, 512))
            im.save(filename + suffix + "_pred.png")

            im = Image.fromarray(cm.magma(targets[i], bytes=True))
            if saveTargets:
                im = im.resize((512, 512))
                im.save(filename + suffix + "_target.png")

        if saveMontage:
            im = Image.fromarray(cm.magma(targets[i], bytes=True))
            new_im.paste(im, ((s + 10) * i, s * 0))
            im = Image.fromarray(cm.magma(outputs[i], bytes=True))
            new_im.paste(im, ((s + 10) * i, s * 1))

            im = Image.fromarray(targets[i] * 256.)
            BW_im.paste(im, ((s + 10) * i, s * 0))
            im = Image.fromarray(outputs[i] * 256.)
            BW_im.paste(im, ((s + 10) * i, s * 1))
            imE = Image.fromarray(np.abs(targets[i] - outputs[i]) * 10. * 256.)
            BW_im.paste(imE, ((s + 10) * i, s * 2))

    if saveMontage:
        new_im.save(filename + ".png")
        BW_im.save(filename + "_bw.png")



def saveVectorfieldAsImage(inputs_denormalized,
                           outputs_denormalized, targets_denormalized, figsDirectory = "", gridSizex = 128, gridSizey = 128,
                           output_as_pdf = True, output_as_png = False, filenamePrefix = ""):
    filename_inp = figsDirectory + filenamePrefix + "Input.png"
    filename_out = figsDirectory + filenamePrefix + "Output.png"
    filename_target = figsDirectory + filenamePrefix + "Target.png"
    filename_diff = figsDirectory + filenamePrefix + "Difference.png"
    filename_diffUFN = figsDirectory + filenamePrefix + "Difference_u.png"
    filename_diffVFN = figsDirectory + filenamePrefix + "Difference_v.png"

    filename_inp_pdf = figsDirectory + filenamePrefix + "Input.pdf"
    filename_out_pdf = figsDirectory + filenamePrefix + "Output.pdf"
    filename_target_pdf = figsDirectory + filenamePrefix + "Target.pdf"
    filename_diff_pdf = figsDirectory + filenamePrefix + "Difference.pdf"
    filename_diffUFN_pdf = figsDirectory + filenamePrefix + "Difference_u.pdf"
    filename_diffVFN_pdf = figsDirectory + filenamePrefix + "Difference_v.pdf"

    if not os.path.exists(os.path.dirname(figsDirectory)) and figsDirectory != "":
        try:
            os.makedirs(os.path.dirname(figsDirectory))
        except OSError as exc:  # Guard against race condition
            if exc.errno != exc.errno.EEXIST:
                raise

    M = inputs_denormalized[2, :, :]
    maskIndices = np.where(M > 0.4)
    minX = np.min(maskIndices[0])
    maxX = np.max(maskIndices[0])
    minY = np.min(maskIndices[1])
    maxY = np.max(maskIndices[1])
    maskIdY = [minX, minX, maxX, maxX]
    maskIdX = [minY, maxY, maxY, minY]

    UTemp = targets_denormalized[0, :, :]
    VTemp = targets_denormalized[1, :, :]

    magnTemp = np.sqrt(UTemp ** 2 + VTemp ** 2)
    minMagn = np.min(magnTemp)
    maxMagn = np.max(magnTemp)


    X, Y = np.meshgrid(np.arange(gridSizex), np.arange(gridSizey))
    UD = np.abs(outputs_denormalized[0, :, :] - targets_denormalized[0, :, :])
    VD = np.abs(outputs_denormalized[1, :, :] - targets_denormalized[1, :, :])

    minUD = np.min(UD)
    maxUD = np.max(UD)
    minVD = np.min(VD)
    maxVD = np.max(VD)
    C = np.array([mapValueToColor(minUD, maxUD, e) for e in UD])

    reds_cm = matplotlib.cm.get_cmap('Reds')

    plt.figure()
    xmin = 0
    xmax = gridSizex
    ymin = 0
    ymax = gridSizey

    plt.gca().set_xlim(xmin, xmax)
    plt.gca().set_ylim(ymin, ymax)

    plt.pcolormesh(X, Y, UD, cmap=reds_cm, vmin=0, vmax=0.5)
    if output_as_pdf:
        plt.savefig(filename_diffUFN_pdf)
    if output_as_png:
        plt.savefig(filename_diffUFN)

    plt.pcolormesh(X, Y, VD, cmap=reds_cm, vmin=0, vmax=0.5)
    if output_as_pdf:
        plt.savefig(filename_diffVFN_pdf)
    if output_as_png:
        plt.savefig(filename_diffVFN)
    plt.close()

    X, Y = np.meshgrid(np.arange(gridSizex), np.arange(gridSizey))
    U_inp = inputs_denormalized[0, :, :]
    V_inp = inputs_denormalized[1, :, :]

    L_inp = np.sqrt(U_inp ** 2 + V_inp ** 2)
    C_inp = np.array([mapValueToColor(minMagn, maxMagn, i) for i in L_inp])
    Q = plt.quiver(X, Y, U_inp, V_inp, C_inp, pivot='mid', units='width')

    #plt.fill(maskIdX, maskIdY, 'r', alpha=1.0)
    plt.scatter(maskIndices[1], maskIndices[0], color='red', s=4)

    #plt.fill(maskIndices, 'r', alpha=1.0)
    plt.gca().set_xlim(xmin, xmax)
    plt.gca().set_ylim(ymin, ymax)
    plt.clim(0, 1)
    plt.colorbar(Q)

    if output_as_pdf:
        plt.savefig(filename_inp_pdf, format='pdf')
    if output_as_png:
        plt.savefig(filename_inp, format='png')
    plt.close()

    plt.figure()
    #X, Y = np.meshgrid(np.arange(gridSizex), np.arange(gridSizey))
    U_out = outputs_denormalized[0, :, :]
    V_out = outputs_denormalized[1, :, :]
    L_out = np.sqrt(U_out ** 2 + V_out ** 2)
    C_out = np.array([mapValueToColor(minMagn, maxMagn, i) for i in L_out])
    Q = plt.quiver(X, Y, U_out, V_out, C_out, pivot='mid', units='width')  # , headwidth=3, headlength=5)
    # Q = plt.quiver(X, Y, U, V, C, edgecolor='k', facecolor='None', linewidth=0.1, headwidth=4, headlength=6, pivot='mid', units='inches')
    # plt.show()
    plt.gca().set_xlim(xmin, xmax)
    plt.gca().set_ylim(ymin, ymax)
    plt.clim(0, 1)
    plt.colorbar(Q)

    if output_as_pdf:
        plt.savefig(filename_out_pdf, format='pdf')
    if output_as_png:
        plt.savefig(filename_out, format='png')
    plt.close()

    plt.figure()
    #X, Y = np.meshgrid(np.arange(gridSizex), np.arange(gridSizey))
    U_tar = targets_denormalized[0, :, :]
    V_tar = targets_denormalized[1, :, :]
    L_tar = np.sqrt(U_tar ** 2 + V_tar ** 2)
    C_tar = np.array([mapValueToColor(minMagn, maxMagn, i) for i in L_tar])
    # W = np.array([0.005 * l / 80.0 for l in L])
    Q = plt.quiver(X, Y, U_tar, V_tar, C_tar, pivot='mid', units='width')  # headwidth=3, headlength=5)
    plt.gca().set_xlim(xmin, xmax)
    plt.gca().set_ylim(ymin, ymax)
    plt.clim(0, 1)
    plt.colorbar(Q)

    if output_as_pdf:
        plt.savefig(filename_target_pdf, format='pdf')
    if output_as_png:
        plt.savefig(filename_target, format='png')

    plt.close()







def saveFrameTestImages(modelNameOut = "", figsDirectory = "", exportAsPDF = False, exportAsPNG = True,
                        inputs_denormalized = [], outputs_denormalized = [], targets_denormalized = [] , inputs_warped_denormalized = []):
    plt.close()
    figPrefix = modelNameOut
    if figPrefix == []:
        figPrefix = ""

    # crate output directory if it's not already existing
    if not os.path.exists(os.path.dirname(figsDirectory)):
        try:
            os.makedirs(os.path.dirname(figsDirectory))
        except OSError as exc:  # Guard against race condition
            if exc.errno != exc.errno.EEXIST:
                raise

    # inputs_denormalized = data.denormalizeFrame(inputs_cpu.cpu().numpy(), idxes.data.numpy(), 'inp')
    #inputs_denormalized = data.getRawInput(idxes.data.numpy(), torch.stack(usedXRange), torch.stack(usedYRange))

    #outputs_denormalized = data.denormalizeFrame(outputs_cpu, idxes.data.numpy(), 'tar')
    #targets_denormalized = data.denormalizeFrame(targets_cpu.cpu().numpy(), idxes.data.numpy(), 'tar')



    # Give quantitative results
    criterionL1 = nn.L1Loss()
    zerosOrOnes = np.zeros(targets_denormalized.shape)
    blendedInp = (inputs_warped_denormalized[0:1,0:1,:,:] + inputs_warped_denormalized[0:1,1:2,:,:]) / 2.0





    # Transform numpy arrays to torch objects
    blendedInp_Torch = Variable(torch.FloatTensor(blendedInp.shape))
    blendedInp_Torch.cuda()
    inputs_denormalized_torch = Variable(torch.FloatTensor(inputs_denormalized.shape))
    inputs_denormalized_torch.cuda()
    outputs_denormalized_torch = Variable(torch.FloatTensor(outputs_denormalized.shape))
    outputs_denormalized_torch.cuda()
    targets_denormalized_torch = Variable(torch.FloatTensor(targets_denormalized.shape))
    targets_denormalized_torch.cuda()
    inputs_warped_denormalized_torch = Variable(torch.FloatTensor(inputs_warped_denormalized.shape))
    inputs_warped_denormalized_torch.cuda()
    zerosOrOnes_torch = Variable(torch.FloatTensor(zerosOrOnes.shape))
    zerosOrOnes_torch.cuda()





    lossL1 = criterionL1(outputs_denormalized_torch, targets_denormalized_torch)
    lossOrgData = criterionL1(zerosOrOnes_torch, targets_denormalized_torch)
    lossBlendData = criterionL1(blendedInp_Torch, targets_denormalized_torch)



    logline = "L1: {}, L1Unlearned: {}, L1Blended: {}\n".format(lossL1.item(), lossOrgData.item(), lossBlendData.item())


    cmin = np.min(inputs_denormalized[0, :, :, :])
    cmax = np.max(inputs_denormalized[0, :, :, :])
    # plt.subplot(4, 1, 1)
    plt.pcolormesh(inputs_denormalized[0, 0, :, :])
    plt.clim(cmin, cmax)
    plt.colorbar()

    if exportAsPNG:
        plt.savefig(figsDirectory + figPrefix + '1FrameIn1.png', quality=100)
    if exportAsPDF:
        plt.savefig(figsDirectory + figPrefix + '1FrameIn1.pdf')

    plt.cla()

    # plt.subplot(4, 1, 2)
    plt.pcolormesh(inputs_denormalized[0, 1, :, :])
    plt.clim(cmin, cmax)
    # plt.colorbar()
    if exportAsPNG:
        plt.savefig(figsDirectory + figPrefix + '4FrameIn2.png', quality=100)
    if exportAsPDF:
        plt.savefig(figsDirectory + figPrefix + '4FrameIn2.pdf')
    plt.cla()

    # plt.subplot(4, 1, 3)
    plt.pcolormesh(outputs_denormalized[0, 0, :, :])
    plt.clim(cmin, cmax)
    # plt.colorbar()
    if exportAsPNG:
        plt.savefig(figsDirectory + figPrefix + '3FrameOut.png', quality=100)
    if exportAsPDF:
        plt.savefig(figsDirectory + figPrefix + '3FrameOut.pdf')
    plt.cla()

    # plt.subplot(4, 1, 4)
    plt.pcolormesh(targets_denormalized[0, 0, :, :])
    plt.clim(cmin, cmax)
    # plt.colorbar()
    if exportAsPNG:
        plt.savefig(figsDirectory + figPrefix + '2FrameTarget.png', quality=100)
    if exportAsPDF:
        plt.savefig(figsDirectory + figPrefix + '2FrameTarget.pdf')
    plt.cla()


# save single image
def saveAsImage(filename, field_param):
    field = np.copy(field_param)
    field = np.flipud(field.transpose())

    min_value = np.min(field)
    max_value = np.max(field)
    field -= min_value
    max_value -= min_value
    field /= max_value

    im = Image.fromarray(cm.magma(field, bytes=True))
    im = im.resize((512, 512))
    im.save(filename)


# read data split from command line
def readProportions():
    flag = True
    while flag:
        input_proportions = input(
            "Enter total numer for training files and proportions for training (normal, superimposed, sheared respectively) seperated by a comma such that they add up to 1: ")
        input_p = input_proportions.split(",")
        prop = [float(x) for x in input_p]
        if prop[1] + prop[2] + prop[3] == 1:
            flag = False
        else:
            print("Error: poportions don't sum to 1")
            print("##################################")
    return (prop)


# helper from data/utils
def makeDirs(directoryList):
    for directory in directoryList:
        if not os.path.exists(directory):
            os.makedirs(directory)

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1', 'True'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0', 'False'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


# von Sebastian # ->
def findNextRunNumber(folder):
    files = os.listdir(folder)
    files = sorted([f for f in files if f.startswith('run')])
    if len(files)==0:
        return 0
    return int(files[-1][3:])

##
def colorize(value, vmin=None, vmax=None, cmap=None):
    """
    A utility function for Torch/Numpy that maps a grayscale image to a matplotlib
    colormap for use with TensorBoard image summaries.
    By default it will normalize the input value to the range 0..1 before mapping
    to a grayscale colormap.
    Arguments:
      - value: 2D Tensor of shape [height, width] or 3D Tensor of shape
        [height, width, 1].
      - vmin: the minimum value of the range used for normalization.
        (Default: value minimum)
      - vmax: the maximum value of the range used for normalization.
        (Default: value maximum)
      - cmap: a valid cmap named for use with matplotlib's `get_cmap`.
        (Default: Matplotlib default colormap)

    Returns a 4D uint8 tensor of shape [height, width, 4].
    """

    # normalize
    vmin = value.min() if vmin is None else vmin
    vmax = value.max() if vmax is None else vmax
    if vmin != vmax:
        value = (value - vmin) / (vmax - vmin)  # vmin..vmax
    else:
        # Avoid 0-division
        value = value * 0.
    # squeeze last dim if it exists
    value = value.squeeze()

    cmapper = cm.get_cmap(cmap)
    value = cmapper(value, bytes=True)  # (nxmx4)
    return value


def insertBoundaryInImg(inp, minVal, border, applyIt = True):
    if not applyIt:
        return inp

    inp[:,border:-border,border] = minVal
    inp[:, border:-border, -border] = minVal
    inp[:, border, border:-border] = minVal
    inp[:, -border, border:-border] = minVal
    return inp

def normalsToRGB(inp, lossBorder, drawLossBorder, customMax = 1):
    # inp has range[Batch, direction, x, y]
    # min is assumed to be 0, max 1
    out = np.around(inp[0,0:3,:,:]/customMax*255)
    out = out.astype(int)
    out = insertBoundaryInImg(out, 0, lossBorder, drawLossBorder)
    #out = np.moveaxis(out, 0, 2)
    return out


def createNewInputKeysList(bNormals = False, bNormalsScreen=False, bTarNormals=False, bTarNormalsScreen=False, bColors=False, bTarColors=False,
                           bDepth = False, bTarDepth=False, bLearnDepthMask=False, bLearnDepthMaskSplats = False, bUseGTMask=False,
                           bTarSplatsDepth = False, bTarSplatsNormals = False, bTarSplatsNormalsScreen=False, bTarSplatsColor = False,
                           bSparseSplatsColor = False, bSparseSplatsDepth = False, bSparseSplatsNormal = False, bSparseSplatsNormalScreen=False):
    newKeys = []
    currCh = 0
    inpChStartCount = 0
    if bNormals:
        currCh = 3
        newKeys.append(['PointsSparseNormal', currCh, 'inp', 'normalize', inpChStartCount])
        inpChStartCount += currCh
    if bNormalsScreen:
        currCh = 3
        newKeys.append(['PointsSparseNormalScreen', currCh, 'inp', 'normalize', inpChStartCount])
        inpChStartCount += currCh
    if bColors:
        currCh = 3
        newKeys.append(['PointsSparseColor', currCh, 'inp', 'normalize', inpChStartCount])
        inpChStartCount += currCh
    if bDepth:
        currCh = 1
        newKeys.append(['PointsSparseDepth', currCh, 'inp', 'normalize', inpChStartCount])
        inpChStartCount += currCh
    if bUseGTMask:
        currCh = 1
        newKeys.append(['useGTMask', currCh, 'inp', '', inpChStartCount])
        inpChStartCount += currCh

    tarChStartCount = 0
    # Always make sure whether you really want to use the following variables as input. They are the best rendering which can be achieved with splats.
    # This is what the neural net should beat. Maybe give as input? Maybe not?
    if bSparseSplatsColor:
        currCh = 3
        newKeys.append(['SplatsSparseColor', currCh, 'inp', 'normalize', inpChStartCount])
        inpChStartCount += currCh
    if bSparseSplatsDepth:
        currCh = 1
        newKeys.append(['SplatsSparseDepth', currCh, 'inp', 'normalize', inpChStartCount])
        inpChStartCount += currCh
    if bSparseSplatsNormal:
        currCh = 1
        newKeys.append(['SplatsSparseNormal', currCh, 'inp', 'normalize', inpChStartCount])
        inpChStartCount += currCh
    if bSparseSplatsNormalScreen:
        currCh = 1
        newKeys.append(['SplatsSparseNormalScreen', currCh, 'inp', 'normalize', inpChStartCount])
        inpChStartCount += currCh

    if bTarSplatsNormals:
        currCh = 3
        newKeys.append(['SplatsNormal', currCh, 'tar', 'normalize', tarChStartCount])
        tarChStartCount += currCh
    if bTarSplatsNormalsScreen:
        currCh = 3
        newKeys.append(['SplatsNormalScreen', currCh, 'tar', 'normalize', tarChStartCount])
        tarChStartCount += currCh
    if bTarSplatsColor:
        currCh = 3
        newKeys.append(['SplatsColor', currCh, 'tar', 'normalize', tarChStartCount])
        tarChStartCount += currCh
    if bTarSplatsDepth:
        currCh = 1
        newKeys.append(['SplatsDepth', currCh, 'tar', 'normalize', tarChStartCount])
        tarChStartCount += currCh



    if bTarNormals:
        currCh = 3
        newKeys.append(['PointsNormal', currCh, 'tar', 'normalize', tarChStartCount])
        tarChStartCount += currCh
    if bTarNormalsScreen:
        currCh = 3
        newKeys.append(['PointsNormalScreen', currCh, 'tar', 'normalize', tarChStartCount])
        tarChStartCount += currCh
    if bTarColors:
        currCh = 3
        newKeys.append(['PointsColor', currCh, 'tar', 'normalize', tarChStartCount])
        tarChStartCount += currCh
    if bTarDepth:
        currCh = 1
        newKeys.append(['PointsDepth', currCh, 'tar', 'normalize', tarChStartCount])
        tarChStartCount += currCh


    # This has to be the last item, always!
    if bLearnDepthMask:
        currCh = 1
        newKeys.append(['useLearnDepthMask', currCh, 'tar', '', tarChStartCount])
        tarChStartCount += currCh
    if bLearnDepthMaskSplats:
        currCh = 1
        newKeys.append(['useLearnDepthMaskSplats', currCh, 'tar', '', tarChStartCount])
        tarChStartCount += currCh


    return newKeys


def keylistInpTarKeys(kl):
    # This function splits a keylist into input and output. Is used for the GUI to select the right channels to render...
    inpList = []
    tarList = []

    inpKeyList = []
    tarKeyList = []


    for item in kl:
        if item[2] == 'inp':
            inpList.append(item)
        elif item[2] == 'tar':
            tarList.append(item)

    for item in inpList:
        inpKeyList.append(item[0])

    for item in tarList:
        tarKeyList.append(item[0])

    return inpList, tarList, inpKeyList, tarKeyList


def keylistToRepetStrList(kl):
    outl = []
    for item in kl:
        for i in range(0, item[1]):
            outl.append(item[0])
    return outl