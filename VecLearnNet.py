
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from models.enhancenet import EnhanceNet
from kernels.partialconv2d import PartialConv2d as PartialConv2d
from DfpNet import blockUNet, weights_init
from DfpNet import weights_init, conv2d, blockUNet
from sparseConv import SparseConv



class UNetFrame(nn.Module):
    def __init__(self, inputChannels = 4, outputChannels = 1, channelExponent=6, dropout=0., sizeOfKernels = 4, resX = 256, resY=256,
                 addNotCat = False, maxPool = False, usePartialConv = True, useDeformConv=False, uNetDepth = 6, sigmoidToLastOutput = False, softmaxToLast=False, twoConvsPerLev=True,
                 batchNorm=True, keysToLoad = []):
        super(UNetFrame, self).__init__()
        #channels = int(2 ** channelExponent + 0.5)
        # Keys to load is not needed for the model, but nevertheless, it can be loaded so it will be exported for the C++ files.
        self.keysToLoad = keysToLoad
        self.sizeOfKernels = sizeOfKernels
        self.sizeIndex = 0
        self.usedSizes = []

        self.uNetDepth = uNetDepth
        self.resX = resX
        self.resY = resY
        self.addNotCat = addNotCat

        self.sigmoidToLastOutput = sigmoidToLastOutput
        self.softmaxToLast = softmaxToLast

        self.partialConv = usePartialConv

        if not self.partialConv:
            from sparseConv import firstBlockUNet, blockUNetAdaptAndAutoPad
        else:
            from partialConv_BlockClasses import firstBlockUNet, blockUNetAdaptAndAutoPad

        channels = channelExponent

        self.layer1 = firstBlockUNet(inputChannels, channels-inputChannels, name='layer1_resConv', size=self.getSizeOfNextChannel( convDeconv = 'conv'),
                                     stride=1, bias=True, relu=False, bn=batchNorm, useDeformConv=useDeformConv)

        if uNetDepth > 0:
            self.layer2 = blockUNetAdaptAndAutoPad(channels, channels * 2, 'layer2', transposed=False, bn=batchNorm, relu=False,
                                dropout=dropout, size=self.getSizeOfNextChannel( convDeconv = 'conv'), resX = self.resX, maxPool=maxPool, useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)
        if uNetDepth > 1:
            self.layer2b = blockUNetAdaptAndAutoPad(channels * 2, channels * 4, 'layer2b', transposed=False, bn=batchNorm, relu=False,
                                 dropout=dropout, size=self.getSizeOfNextChannel( convDeconv = 'conv'), resX = self.resX, maxPool=maxPool, useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)
        if uNetDepth > 2:
            self.layer3 = blockUNetAdaptAndAutoPad(channels * 4, channels * 8, 'layer3', transposed=False, bn=batchNorm, relu=False,
                                dropout=dropout, size=self.getSizeOfNextChannel( convDeconv = 'conv'), resX = self.resX, maxPool=maxPool, useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)
        if uNetDepth > 3:
            self.layer4 = blockUNetAdaptAndAutoPad(channels * 8, channels * 16, 'layer4', transposed=False, bn=batchNorm, relu=False,
                                dropout=dropout, size=self.getSizeOfNextChannel( convDeconv = 'conv'), maxPool=maxPool, useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)
        if uNetDepth > 4:
            self.layer5 = blockUNetAdaptAndAutoPad(channels * 16, channels * 32, 'layer5', transposed=False, bn=batchNorm, relu=False,
                                dropout=dropout, size=self.getSizeOfNextChannel( convDeconv = 'conv'), maxPool=maxPool, useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)
        if uNetDepth > 5:
            self.layer6 = blockUNetAdaptAndAutoPad(channels * 32, channels * 64, 'layer6', transposed=False, bn=batchNorm, relu=False,
                                dropout=dropout,size=self.getSizeOfNextChannel( convDeconv = 'conv'), maxPool=maxPool, useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)
            self.dlayer6 = blockUNetAdaptAndAutoPad(channels * 64, channels * 32, 'dlayer6', transposed=True, bn=batchNorm, relu=False,
                                 dropout=dropout, size=self.getSizeOfNextChannel( convDeconv = 'deconv'), useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)
        if uNetDepth > 4:
            self.dlayer5 = blockUNetAdaptAndAutoPad(channels * 32 * (1 + int(uNetDepth!=5)), channels * 16, 'dlayer5', transposed=True, bn=batchNorm, relu=False,
                                 dropout=dropout, size=self.getSizeOfNextChannel( convDeconv = 'deconv'), useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)
        if uNetDepth > 3:
            self.dlayer4 = blockUNetAdaptAndAutoPad(channels * 16 * (1 + int(uNetDepth!=4)), channels * 8, 'dlayer4', transposed=True, bn=batchNorm, relu=False,
                                 dropout=dropout, size=self.getSizeOfNextChannel( convDeconv = 'deconv'), useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)
        if uNetDepth > 2:
            self.dlayer3 = blockUNetAdaptAndAutoPad(channels * 8 * (1 + int(uNetDepth!=3)), channels * 4, 'dlayer3', transposed=True, bn=batchNorm, relu=False,
                                 dropout=dropout, size=self.getSizeOfNextChannel( convDeconv = 'deconv'), resX = self.resX, useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)
        if uNetDepth > 1:
            self.dlayer2b = blockUNetAdaptAndAutoPad(channels * 4 * (1 + int(uNetDepth!=2)), channels * 2, 'dlayer2b', transposed=True, bn=batchNorm, relu=False,
                                  dropout=dropout, size=self.getSizeOfNextChannel( convDeconv = 'deconv'), resX = self.resX, useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)
        if uNetDepth > 0:
            self.dlayer2 = blockUNetAdaptAndAutoPad(channels * 2 * (1 + int(uNetDepth!=1)), channels, 'dlayer2', transposed=True, bn=batchNorm, relu=False,
                                 dropout=dropout, size=self.getSizeOfNextChannel( convDeconv = 'deconv'), resX = self.resX, useDeformConv=useDeformConv, twoConvsPerLev=twoConvsPerLev)


        currKernelSize = self.getSizeOfNextChannel( convDeconv = 'deconv')
        currpad = int((currKernelSize-1) / 2 )
        in_c = channels * 2
        out_c = outputChannels

        if not self.partialConv:
            self.dlayer1 = SparseConv(in_c, out_c, kernel_size=currKernelSize, stride=1, padding=currpad, bias=True,)

            if not self.addNotCat:
                self.dlayer0 = SparseConv(inputChannels + outputChannels, outputChannels,
                                                    kernel_size=currKernelSize, stride=1, padding=currpad, bias=True)
            else:
                self.dlayer0 = SparseConv(inputChannels, outputChannels, kernel_size=currKernelSize, stride=1,
                                                    padding=currpad, bias=True)

        else:
            self.dlayer1 = PartialConv2d(in_c, out_c, kernel_size=currKernelSize, stride=1, padding=currpad, bias=True,
                                             return_mask=True)

            if not self.addNotCat:
                self.dlayer0 = PartialConv2d(inputChannels + outputChannels, outputChannels,
                                                    kernel_size=currKernelSize, stride=1, padding=currpad, bias=True,
                                                    return_mask=True)
            else:
                self.dlayer0 = PartialConv2d(inputChannels, outputChannels, kernel_size=currKernelSize, stride=1,
                                                    padding=currpad, bias=True, return_mask=True)

        if self.sigmoidToLastOutput:
            self.lsigmoid = nn.Sigmoid()
        elif self.softmaxToLast:
            self.lsigmoid = nn.Softmax2d()


    def getSizeOfNextChannel(self, convDeconv = 'conv'):
        if convDeconv == 'conv':
            currSize = []
            sizeIsArray = False
            if type(self.sizeOfKernels)==int:
                currSize = self.sizeOfKernels
                self.sizeIndex += 1
            else:
                if self.sizeOfKernels.shape[0] > self.sizeIndex:
                    sizeIsArray = True
                    currSize = self.sizeOfKernels[self.sizeIndex]
                    self.sizeIndex += 1
                else:
                    currSize = self.sizeOfKernels[-1]
                    self.sizeIndex += 1
            self.usedSizes.append(currSize)
            return currSize
        else:
            currSize = self.usedSizes[self.sizeIndex - 1]
            self.sizeIndex -= 1
            return currSize


    def forward(self, x, maskPC):

        if not self.partialConv:

            res1,mask1 = self.layer1(x, maskPC)
            out1 = torch.cat([res1, x], dim=1)
            if self.uNetDepth > 0:
                out2, mask2 = self.layer2(out1,mask1)
                if self.uNetDepth > 1:
                    out2b, mask2b = self.layer2b(out2, mask2)
                    if self.uNetDepth > 2:
                        out3, mask3 = self.layer3(out2b, mask2b)
                        if self.uNetDepth > 3:
                            out4, mask4 = self.layer4(out3, mask3)
                            if self.uNetDepth > 4:
                                out5, mask5 = self.layer5(out4, mask4)
                                if self.uNetDepth > 5:
                                    out6, mask6 = self.layer6(out5, mask5)
                                    dout6, dmask6 = self.dlayer6(out6, mask6)

                                    dout6_out5 = torch.cat([dout6, out5], 1)
                                    dmask6_concat = (dmask6.type(torch.IntTensor) | mask5.type(torch.IntTensor))
                                    dout5, dmask5 = self.dlayer5(dout6_out5,dmask6_concat.type(torch.FloatTensor).cuda())
                                else:
                                    dout5, dmask5 = self.dlayer5(out5,mask5)
                                dout5_out4 = torch.cat([dout5, out4], 1)
                                dmask5_concat = (dmask5.type(torch.IntTensor) | mask4.type(torch.IntTensor))
                                dout4, dmask4 = self.dlayer4(dout5_out4,dmask5_concat.type(torch.FloatTensor).cuda())
                            else:
                                dout4, dmask4 = self.dlayer4(out4, mask4)
                            dout4_out3 = torch.cat([dout4, out3], 1)
                            dmask4_concat = (dmask4.type(torch.IntTensor) | mask3.type(torch.IntTensor))
                            dout3, dmask3 = self.dlayer3(dout4_out3, dmask4_concat.type(torch.FloatTensor).cuda())
                        else:
                            dout3, dmask3 = self.dlayer3(out3, mask3)
                        dout3_out2b = torch.cat([dout3, out2b], 1)
                        dmask3_concat = (dmask3.type(torch.IntTensor) | mask2b.type(torch.IntTensor))
                        dout2b, dmask2b = self.dlayer2b(dout3_out2b, dmask3_concat.type(torch.FloatTensor).cuda())
                    else:
                        dout2b, dmask2b = self.dlayer2b(out2b, mask2b)
                    dout2b_out2 = torch.cat([dout2b, out2], 1)
                    dmask2b_concat = (dmask2b.type(torch.IntTensor) | mask2.type(torch.IntTensor))
                    dout2, dmask2 = self.dlayer2(dout2b_out2,dmask2b_concat.type(torch.FloatTensor).cuda())
                else:
                    dout2, dmask2 = self.dlayer2(out2, mask2)
                dout2_out1 = torch.cat([dout2, out1], 1)
                dmask2_concat = (dmask2.type(torch.IntTensor) | mask1.type(torch.IntTensor))
                dout1, dmask1 = self.dlayer1(dout2_out1, dmask2_concat.type(torch.FloatTensor).cuda())
            else:
                dout1, dmask1 = self.dlayer1(out1,mask1)

            # And a last skip connection from x to final output
            dout0 = []
            if not self.addNotCat:
                dout1_x = torch.cat([dout1, x], 1)
                dmask1_concat = (dmask1.type(torch.IntTensor) | mask1.type(torch.IntTensor))
                dout0, dmask0 = self.dlayer0(dout1_x, dmask1_concat.type(torch.FloatTensor).cuda())
            else:
                # In case there is one input field which is not piped through (depth mask when combining two UNets, this case has to be handled.)
                if dout1.shape[1] == x.shape[1]:
                    dout1_x = torch.add(dout1, x)
                else:
                    dout1_x = torch.add(dout1,x[:,0:dout1.shape[1]])
                    dout1_x = torch.cat([dout1_x,  x[:,dout1.shape[1]:]], dim=1)

                dmask1_concat = dmask1
                dout0, dmask0 = self.dlayer0(dout1_x, dmask1_concat)

            if self.sigmoidToLastOutput or self.softmaxToLast:
                dout0_split = torch.split(dout0, 1, dim=1)

                dout0_splitLast = self.lsigmoid(dout0_split[-1])
                if len(dout0_split) > 1:
                    dout0 = torch.cat( [  torch.cat( dout0_split[0:-1], dim=1 )  , dout0_splitLast ], 1)
                else:
                    dout0 = dout0_splitLast
        else:
            res1,mask1 = self.layer1(x, maskPC)
            out1 = torch.cat([res1, x], dim=1)
            if self.uNetDepth > 0:
                out2, mask2 = self.layer2(out1,mask1)
                if self.uNetDepth > 1:
                    out2b, mask2b = self.layer2b(out2, mask2)
                    if self.uNetDepth > 2:
                        out3, mask3 = self.layer3(out2b, mask2b)
                        if self.uNetDepth > 3:
                            out4, mask4 = self.layer4(out3, mask3)
                            if self.uNetDepth > 4:
                                out5, mask5 = self.layer5(out4, mask4)
                                if self.uNetDepth > 5:
                                    out6, mask6 = self.layer6(out5, mask5)
                                    dout6, dmask6 = self.dlayer6(out6, mask6)

                                    dout6_out5 = torch.cat([dout6, out5], 1)
                                    dmask6_concat = (dmask6.type(torch.IntTensor) | mask5.type(torch.IntTensor))
                                    dout5, dmask5 = self.dlayer5(dout6_out5,dmask6_concat.type(torch.FloatTensor).cuda())
                                else:
                                    dout5, dmask5 = self.dlayer5(out5,mask5)
                                dout5_out4 = torch.cat([dout5, out4], 1)
                                dmask5_concat = (dmask5.type(torch.IntTensor) | mask4.type(torch.IntTensor))
                                dout4, dmask4 = self.dlayer4(dout5_out4,dmask5_concat.type(torch.FloatTensor).cuda())
                            else:
                                dout4, dmask4 = self.dlayer4(out4, mask4)
                            dout4_out3 = torch.cat([dout4, out3], 1)
                            dmask4_concat = (dmask4.type(torch.IntTensor) | mask3.type(torch.IntTensor))
                            dout3, dmask3 = self.dlayer3(dout4_out3, dmask4_concat.type(torch.FloatTensor).cuda())
                        else:
                            dout3, dmask3 = self.dlayer3(out3, mask3)
                        dout3_out2b = torch.cat([dout3, out2b], 1)
                        dmask3_concat = (dmask3.type(torch.IntTensor) | mask2b.type(torch.IntTensor))
                        dout2b, dmask2b = self.dlayer2b(dout3_out2b, dmask3_concat.type(torch.FloatTensor).cuda())
                    else:
                        dout2b, dmask2b = self.dlayer2b(out2b, mask2b)
                    dout2b_out2 = torch.cat([dout2b, out2], 1)
                    dmask2b_concat = (dmask2b.type(torch.IntTensor) | mask2.type(torch.IntTensor))
                    dout2, dmask2 = self.dlayer2(dout2b_out2,dmask2b_concat.type(torch.FloatTensor).cuda())
                else:
                    dout2, dmask2 = self.dlayer2(out2, mask2)
                dout2_out1 = torch.cat([dout2, out1], 1)
                dmask2_concat = (dmask2.type(torch.IntTensor) | mask1.type(torch.IntTensor))
                dout1, dmask1 = self.dlayer1(dout2_out1, dmask2_concat.type(torch.FloatTensor).cuda())
            else:
                dout1, dmask1 = self.dlayer1(out1,mask1)

            # And a last skip connection from x to final output
            dout0 = []
            if not self.addNotCat:
                dout1_x = torch.cat([dout1, x], 1)
                dmask1_concat = (dmask1.type(torch.IntTensor) | mask1.type(torch.IntTensor))
                dout0, dmask0 = self.dlayer0(dout1_x, dmask1_concat.type(torch.FloatTensor).cuda())
            else:
                # In case there is one input field which is not piped through (depth mask when combining two UNets, this case has to be handled.)
                if dout1.shape[1] == x.shape[1]:
                    dout1_x = torch.add(dout1, x)
                else:
                    dout1_x = torch.add(dout1,x[:,0:dout1.shape[1]])
                    dout1_x = torch.cat([dout1_x,  x[:,dout1.shape[1]:]], dim=1)

                dmask1_concat = dmask1
                dout0, dmask0 = self.dlayer0(dout1_x, dmask1_concat)

            if self.sigmoidToLastOutput or self.softmaxToLast:
                dout0_split = torch.split(dout0, 1, dim=1)

                dout0_splitLast = self.lsigmoid(dout0_split[-1])
                if len(dout0_split) > 1:
                    dout0 = torch.cat( [  torch.cat( dout0_split[0:-1], dim=1 )  , dout0_splitLast ], 1)
                else:
                    dout0 = dout0_splitLast

        return dout0


class ModifiedUNetFrame(nn.Module):
    def __init__(self, inputChannels=4, outputChannels=1, channelExponent=6, dropout=0., sizeOfKernels=4, resX=256, resY=256,
                 addNotCat=False, maxPool=False, usePartialConv='Second', useDeformConv='First', uNetDepth=6, sigmoidToLastOutput=False, twoConvsPerLev=True, batchNorm=True,
                 keysToLoad = []):
        super(ModifiedUNetFrame, self).__init__()
        self.keysToLoad = keysToLoad
        self.depthChannel=3

        if usePartialConv == "First":
            useFirstPartialConv = True
            useSecondPartialConv = False
        if usePartialConv == "Second":
            useFirstPartialConv = False
            useSecondPartialConv = True
        if usePartialConv == "Both":
            useFirstPartialConv = True
            useSecondPartialConv = True
        if usePartialConv == "None":
            useFirstPartialConv = False
            useSecondPartialConv = False

        if useDeformConv == "First":
            useFirstDeformConv = True
            useSecondDeformConv = False
        if useDeformConv == "Second":
            useFirstDeformConv = False
            useSecondDeformConv = True
        if useDeformConv == "Both":
            useFirstDeformConv = True
            useSecondDeformConv = True
        if useDeformConv == "None":
            useFirstDeformConv = False
            useSecondDeformConv = False

        self.net = UNetFrame(channelExponent=channelExponent, inputChannels=inputChannels, outputChannels=outputChannels, dropout=dropout, sizeOfKernels=sizeOfKernels,
                         resX=resX, resY=resY, addNotCat=addNotCat, usePartialConv=useFirstPartialConv, useDeformConv=useFirstDeformConv, uNetDepth=uNetDepth, sigmoidToLastOutput=sigmoidToLastOutput, twoConvsPerLev=twoConvsPerLev, batchNorm=batchNorm)

    def forward(self, x, maskPC):

        out = self.net(x,maskPC)

        return out
