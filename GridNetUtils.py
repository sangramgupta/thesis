################
# Utility functions to create a GridNet (Residual Conv-Deconv Grid Network for Semantic Segmentation - )
#
#
#
#
################

import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import numpy as np

def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

#def FirstBlock(in_c, out_c, name, transposed=False, bn=True, relu=True, size=3, pad=1, dropout=0.):
#    block = nn.Sequential()
#
#    block.add_module('%s_prelu' % name, nn.PReLU())
#    block.add_module('%s_conv' % name, nn.Conv2d(in_c, out_c, kernel_size=size, stride=1, padding=pad, bias=True))
#
#    block.add_module('%s_prelu' % name, nn.PReLU())
#    block.add_module('%s_conv' % name, nn.Conv2d(in_c, out_c, kernel_size=size, stride=1, padding=pad, bias=True))

# Corrects the number of channels for residual layers changing them
def BlockAddConv(in_c, out_c, name, transposed=False, bn=True, relu=True, size=1, pad=0, dropout=0., addTanh = False):
    block = nn.Sequential()

    block.add_module('%s_conv' % name, nn.Conv2d(in_c, out_c, kernel_size=size, stride=1, padding=pad, bias=True))
    if addTanh:
        block.add_module('%s_tanh' % name, nn.Tanh())
    return block


def ResConvBlocksTest(in_c, out_c, name, transposed=False, bn=True, relu=True, size=3, pad=1, dropout=0., dilation = 1):
    block = nn.Sequential()

    block.add_module('%s_prelu' % name, nn.ReLU(inplace=True))
    block.add_module('%s_conv' % name, nn.Conv2d(in_c, out_c, kernel_size=size, stride=1, padding=pad, bias=True, dilation=dilation))

    block.add_module('%s_prelu' % name, nn.ReLU(inplace=True))
    block.add_module('%s_conv' % name, nn.Conv2d(in_c, out_c, kernel_size=size, stride=1, padding=pad, bias=True, dilation=dilation))
    # block.add_module('%s_dropout' % name, nn.TotalDropout(dropout))

    return block

def ResConvBlocks(in_c, out_c, name, transposed=False, bn=True, relu=True, size=3, pad=1, dropout=0., dilation = 1):
    block = nn.Sequential()

    block.add_module('%s_prelu' % name, nn.PReLU())
    block.add_module('%s_conv' % name, nn.Conv2d(in_c, out_c, kernel_size=size, stride=1, padding=pad, bias=True, dilation=dilation))

    block.add_module('%s_prelu' % name, nn.PReLU())
    block.add_module('%s_conv' % name, nn.Conv2d(in_c, out_c, kernel_size=size, stride=1, padding=pad, bias=True, dilation=dilation))
    # block.add_module('%s_dropout' % name, nn.TotalDropout(dropout))

    return block

    #input = input - nn

def DropoutLayer(in_c, out_c, name, transposed=False, bn=True, relu=True, size=3, pad=1, dropout=0.):
    block = nn.Sequential()
    block.add_module('%s_dropout2d' % name, nn.Dropout2d(p=dropout, inplace=False))
    return block

def DownSampleBlock(in_c, out_c, name, transposed=False, bn=True, relu=True, size=3, pad=1, dropout=0., dilation = 1):
    block = nn.Sequential()

    block.add_module('%s_prelu' % name, nn.PReLU())
    block.add_module('%s_conv' % name, nn.Conv2d(in_c, out_c, kernel_size=size, stride=2, padding=pad, bias=True, dilation=dilation))

    block.add_module('%s_prelu' % name, nn.PReLU())
    block.add_module('%s_conv' % name, nn.Conv2d(in_c, out_c, kernel_size=size, stride=1, padding=pad, bias=True, dilation=dilation))
    return block


def UpSampleBlock(in_c, out_c, name, transposed=False, bn=True, relu=True, size=3, pad=1, dropout=0.):
    block = nn.Sequential()
    # Upsampling has to be done in the forward function...
    #block.add_module('%s_bilinearUp' % name, F.upsample_bilinear())


    block.add_module('%s_prelu' % name, nn.PReLU())
    block.add_module('%s_conv' % name, nn.Conv2d(in_c, out_c, kernel_size=size, stride=1, padding=pad, bias=True))

    block.add_module('%s_prelu' % name, nn.PReLU())
    block.add_module('%s_conv' % name, nn.Conv2d(in_c, out_c, kernel_size=size, stride=1, padding=pad, bias=True))
    return block



# generator model
class GridNetFrame(nn.Module):
    def __init__(self, channelExponent=6, dropout=0., inpChannels = 4, outChannels =1, channels = 2, netRows = 3, netCols = 6, kernelSizeSpec = (3,3), paddingSpec = 1,
                 strideSpec = 1, dilationSpec = 1, totalDropout = 0.1, cOpt = 0, enableTotalDropout = True, batchNorm = False, keysToLoad = []):
        super(GridNetFrame, self).__init__()
        #channels = 2 # 16
        channelsCol2 = channels
        channelsCol3 = channelsCol2
        multLev1 = 2
        multLev2 = 3
        multLev3 = 4
        multLev4 = 5
        multLev5 = 6
        multLev6 = 7
        multLev7 = 8
        multLev8 = 9
        multLev9 = 10
        self.totalDropout = totalDropout
        self.outChannels = outChannels
        self.enableTotalDropout = enableTotalDropout

        self.batchNorm = batchNorm

        self.cOpt = cOpt # 0 for adding values, 1 for concatinating channels (when connecting e.g. downsample and residual layers)


        self.kernelSize = kernelSizeSpec
        self.padding = paddingSpec
        self.stride = strideSpec
        self.dilation = dilationSpec


        self.createClassical = False

        self.resConvLayers = nn.ModuleList()
        self.firstResLayer = nn.ModuleList()
        self.dropoutLayer = nn.ModuleList()
        self.downSampleCols = nn.ModuleList()
        self.upSampleCols = nn.ModuleList()

        self.bnLayers = nn.ModuleList()

        self.resConvRedNumChannel = nn.ModuleList()

        self.netCols = netCols
        self.netRows = netRows

        if self.createClassical:
            self.layer0 = ResConvBlocks(inpChannels, channels, 'layer0_inp')
            self.layerFirstAddConv = BlockAddConv(inpChannels, channels, 'layer0_blockaddConv')

            self.layer01 = ResConvBlocks(channels, channels, 'layer01_inp')
            self.layer02 = ResConvBlocks(channels, channels, 'layer02_inp')
            self.layer03 = ResConvBlocks(channels, channels, 'layer03_inp')
            self.layer04 = ResConvBlocks(  channels,   channels, 'layer04_inp')
            self.layer05 = ResConvBlocks(  channels,   channels, 'layer05_inp')
            self.layer06 = ResConvBlocks(  channels,   self.outChannels, 'layer06_inp')
            self.layerLastAddConv = BlockAddConv(channels, self.outChannels, 'layer06_blockaddConv')

            self.dropout01 = DropoutLayer(1, 1, 'dropout01', dropout=self.totalDropout)
            self.dropout02 = DropoutLayer(1, 1, 'dropout02', dropout=self.totalDropout)
            self.dropout03 = DropoutLayer(1, 1, 'dropout03', dropout=self.totalDropout)
            self.dropout04 = DropoutLayer(1, 1, 'dropout04', dropout=self.totalDropout)
            self.dropout05 = DropoutLayer(1, 1, 'dropout05', dropout=self.totalDropout)


            # Check number of channels, whether they stay the same or not
            self.layer0_to_1 = DownSampleBlock(channels, multLev1 * channels, 'layer0_to_1')
            self.layer01_to_1 = DownSampleBlock(channels, multLev1 * channels, 'layer01_to_1')
            self.layer02_to_1 = DownSampleBlock(channels, multLev1 * channels, 'layer02_to_1')

            self.layer13_to_0 = UpSampleBlock(multLev1 * channels, channels, 'layer13_to_0')
            self.layer14_to_0 = UpSampleBlock(multLev1 * channels, channels, 'layer14_to_0')
            self.layer15_to_0 = UpSampleBlock(multLev1 * channels, channels, 'layer15_to_0')

            self.layer11 = ResConvBlocks(multLev1 * channels, multLev1 * channels, 'layer11')
            self.layer12 = ResConvBlocks(multLev1 *  channels, multLev1 *  channels, 'layer12')
            self.layer13 = ResConvBlocks(multLev1 *  channels, multLev1 *  channels, 'layer13')
            self.layer14 = ResConvBlocks(multLev1 * channels, multLev1 * channels, 'layer14')
            self.layer15 = ResConvBlocks(multLev1 * channels, multLev1 * channels, 'layer15')

            self.dropout11 = DropoutLayer(1, 1, 'dropout11', dropout=self.totalDropout)
            self.dropout12 = DropoutLayer(1, 1, 'dropout12', dropout=self.totalDropout)
            self.dropout13 = DropoutLayer(1, 1, 'dropout13', dropout=self.totalDropout)
            self.dropout14 = DropoutLayer(1, 1, 'dropout14', dropout=self.totalDropout)
            self.dropout15 = DropoutLayer(1, 1, 'dropout15', dropout=self.totalDropout)


            self.layer1_to_2 = DownSampleBlock(multLev1 *  channels, multLev2 * channels, 'layer0_to_1')
            self.layer11_to_2 = DownSampleBlock(multLev1 *  channels, multLev2 * channels, 'layer01_to_1')
            self.layer12_to_2 = DownSampleBlock(multLev1 *  channels, multLev2 * channels, 'layer02_to_1')

            self.layer23_to_1 = UpSampleBlock(multLev2 * channels, multLev1 *  channels, 'layer23_to_1')
            self.layer24_to_1 = UpSampleBlock(multLev2 * channels, multLev1 *  channels, 'layer24_to_1')
            self.layer25_to_1 = UpSampleBlock(multLev2 * channels, multLev1 *  channels, 'layer25_to_1')

            self.layer21 = ResConvBlocks(multLev2 * channels, multLev2 * channels, 'layer21')
            self.layer22 = ResConvBlocks(multLev2 * channels, multLev2 * channels, 'layer22')
            self.layer23 = ResConvBlocks(multLev2 * channels, multLev2 * channels, 'layer23')
            self.layer24 = ResConvBlocks(multLev2 * channels, multLev2 * channels, 'layer24')
            self.layer25 = ResConvBlocks(multLev2 * channels, multLev2 * channels, 'layer25')

            self.dropout21 = DropoutLayer(1, 1, 'dropout11', dropout=self.totalDropout)
            self.dropout22 = DropoutLayer(1, 1, 'dropout11', dropout=self.totalDropout)
            self.dropout23 = DropoutLayer(1, 1, 'dropout11', dropout=self.totalDropout)
            self.dropout24 = DropoutLayer(1, 1, 'dropout11', dropout=self.totalDropout)
            self.dropout25 = DropoutLayer(1, 1, 'dropout11', dropout=self.totalDropout)
        else:

            # The following code is to create the net automatically for a given number of rows and columns.


            self.nrDownSampleCols = math.floor(self.netCols / 2.0)
            multLevAcc = [1, multLev1, multLev2, multLev3, multLev4, multLev5, multLev6, multLev7, multLev8, multLev9]

            # Multiply the number of input channels for residual blocks if upsample and residual are concatenated, not added.
            # There is a decision to make: double the number of channels for every column, or use a 1-conv layer to reduce
            # the number of channels in the skip connection.
            # So far, the number of channels is doubled...
            cMultChan = 1
            if self.cOpt == 1:
                cMultChan = 2

            for row in range(0,self.netRows):
                self.resConvLayers.append(nn.ModuleList())
                self.dropoutLayer.append(nn.ModuleList())
                self.downSampleCols.append(nn.ModuleList())
                self.upSampleCols.append(nn.ModuleList())
                # only needed for self.cOpt == 1
                self.resConvRedNumChannel.append(nn.ModuleList())

                if self.batchNorm:
                    self.bnLayers.append(nn.ModuleList())

            if self.cOpt == 0:
                # Append the input layer
                self.firstResLayer = ResConvBlocks(inpChannels, channels, 'layer0_inp', size=self.kernelSize, pad = self.padding, dilation = self.dilation)
                self.layerFirstAddConv = BlockAddConv(inpChannels, channels, 'layer0_blockaddConv')

                for row in range(0, self.netRows):
                    for col in range(0, self.netCols):

                        nameRes = "layerRes{}{}".format(row,col)
                        if col != self.netCols-1:
                            self.resConvLayers[row].append(ResConvBlocks(multLevAcc[row] * channels, multLevAcc[row] * channels, nameRes, size=self.kernelSize, pad = self.padding, dilation = self.dilation))
                        nameDropout = "layerDO{}{}".format(row, col)

                        # The bottom one must not be dropped out. Otherwise, there wouldn't be always a connection through the net
                        if row != self.netRows - 1 and col != self.netCols - 1 and self.enableTotalDropout:
                            self.dropoutLayer[row].append(DropoutLayer(1,1,nameDropout, dropout=self.totalDropout))

                        # Construct the downsample and upsample layers
                        if row != self.netRows -1 and col < self.nrDownSampleCols:
                            nameDownSample = "layerDown{}{}_to_{}".format(row,col,row+1)
                            self.downSampleCols[row].append( DownSampleBlock(multLevAcc[row]*channels, multLevAcc[row+1] * channels, nameDownSample))

                        elif row != 0 and col >= self.nrDownSampleCols:
                            nameUpSample = "layerUp{}{}_to_{}".format(row,col, row - 1)
                            self.upSampleCols[row].append(UpSampleBlock(multLevAcc[row] * channels, multLevAcc[row-1] * channels, nameUpSample))

                        if self.batchNorm:
                            self.bnLayers[row].append(nn.BatchNorm2d(multLevAcc[row] * channels))

                # Add output layer
                self.resConvLayers[0].append(ResConvBlocks(channels, self.outChannels, 'layerLast_res', size=self.kernelSize, pad = self.padding, dilation = self.dilation))
                self.lasConvBlock = BlockAddConv(channels, self.outChannels, 'layerOut_blockaddConv', addTanh = False)


            elif self.cOpt == 1:
                assert (channels % 2 == 0)
                # Here, channels are not added but concatenated. Hence, do not increase the number of channels by row, since they automatically increase

                # Append the input layer
                self.firstResLayer = ResConvBlocks(inpChannels, channels, 'layer0_inp', size=self.kernelSize,
                                                   pad=self.padding, dilation=self.dilation)
                self.layerFirstAddConv = BlockAddConv(inpChannels, channels, 'layer0_blockaddConv')

                cH = (np.zeros((self.netRows, self.netCols), dtype=int) +1) *-1


                for i in range(0, len(multLevAcc)):
                    if i == cH.shape[0]:
                        break
                    cH[i][0] = channels * multLevAcc[i]
                for j in range(0, self.netCols):
                    cH[0][j] = cH[0][0]


                # Fill the matrix with channel sizes
                # First, the downscale part
                for row in range(1, self.netRows):
                    for col in range(1, self.nrDownSampleCols):
                        cH[row][col] = cH[row][col - 1]*2

                # upscale part
                for col in range(self.nrDownSampleCols, self.netCols):
                    cH[self.netRows - 1][col] = cH[self.netRows - 1][col-1]

                for row in range(self.netRows-2 ,-1, -1 ):
                    for col in range(self.nrDownSampleCols, self.netCols):
                        cH[row][col] = cH[row + 1][col] + cH[row][col - 1]


                # mirror the array, for upscaling
                for row in range(0, self.netRows):
                    for col in range(self.nrDownSampleCols, self.netCols):
                        cH[row][col] = cH[row][self.nrDownSampleCols - abs((col+1) - self.nrDownSampleCols)]


                for row in range(0, self.netRows):
                    for col in range(0, self.netCols):

                        nameRes = "layerRes{}{}".format(row, col)
                        if col != self.netCols - 1:
                            self.resConvLayers[row].append(
                                ResConvBlocks( cH[row][col] , cH[row][col] , nameRes,
                                              size=self.kernelSize, pad=self.padding, dilation=self.dilation))

                            if col >= self.nrDownSampleCols -1 and row != self.netRows -1:
                                self.resConvRedNumChannel[row].append(BlockAddConv(cH[row][col], int(round(cH[row][col+1] / 2)), 'layerOut_blockaddConv'))
                            elif col >= self.nrDownSampleCols -1:
                                self.resConvRedNumChannel[row].append(
                                    BlockAddConv(cH[row][col], int(round(cH[row][col + 1])),
                                                 'layerOut_blockaddConv'))

                        nameDropout = "layerDO{}{}".format(row, col)

                        # The bottom one must not be dropped out. Otherwise, there wouldn't be always a connection through the net
                        if row != self.netRows - 1 and col != self.netCols - 1:
                            self.dropoutLayer[row].append(DropoutLayer(1, 1, nameDropout, dropout=self.totalDropout))

                        # Construct the downsample and upsample layers
                        if row != self.netRows - 1 and col < self.nrDownSampleCols:
                            nameDownSample = "layerDown{}{}_to_{}".format(row, col, row + 1)
                            colForOutCh = col
                            if col != 0:
                                colForOutCh = col-1
                            self.downSampleCols[row].append(
                                DownSampleBlock(cH[row][col], cH[row+1][colForOutCh],
                                                nameDownSample))

                        elif row != 0 and col >= self.nrDownSampleCols:
                            nameUpSample = "layerUp{}{}_to_{}".format(row, col, row - 1)
                            self.upSampleCols[row].append(
                                UpSampleBlock(  cH[row][col] , int(round(cH[row -1][col]/2))  ,
                                              nameUpSample))

                # Batch normalization channels:
                if self.batchNorm:
                    for row in range(0,self.netRows):
                        for col in range(0, self.netCols):
                            self.bnLayers[row].append(nn.BatchNorm2d(cH[row][col]))

                self.resConvLayers[0].append(
                    ResConvBlocks(cH[0][self.netCols-1] , self.outChannels, 'layerLast_res', size=self.kernelSize, pad=self.padding,
                                  dilation=self.dilation))
                self.lasConvBlock = BlockAddConv(cH[0][self.netCols-1] , self.outChannels, 'layerOut_blockaddConv', addTanh=False)
















                        # # cM is the multiplier needed if we concatenate instead of adding of channels
                        # cM = 1
                        # # Downsamplepart
                        # if col > 0 and col < self.nrDownSampleCols:
                        #     if row > 0:
                        #         cM = cMultChan
                        # elif col >= self.nrDownSampleCols and row != self.netRows - 1:
                        #     cM = cMultChan
                        # cMIn = pow(cM, col)
                        # cMOut = pow(cM, col)






    def AddTotalDropout(self, inLayer, dropLayer):
        # Get size of original Tensor
        currSize = inLayer.size()
        # Reshape the original tensor to only one channel. The first dimension is the batch size. Since dropout2d drops the j-th channel
        # of i th sample in the batch, we can keep the batch dimension
        out = inLayer.view(currSize[0], 1, currSize[1] * currSize[2], currSize[3])

        out = dropLayer(out)
        return out.view(currSize[0], currSize[1], currSize[2], currSize[3])

    def combineChannels(self, in1, in2, cOpt = 0):
        # cOpt is the combination option. 0 for simply adding the values, 1 for concatinating the channels
        if cOpt == 0:
            return torch.add(in1, in2)
        elif cOpt == 1:
            return torch.cat([in1, in2], dim=1)

    #def ResLayerWithDropout(self, in, out):


    def forward(self, x):

        if self.createClassical:
            # First, all horizontal layers in row 0 are connected
            res1 = self.layer0(x)
            res1 # torch.add(x,res1)
            x_conved = self.layerFirstAddConv(x)

            out1 = torch.add(x_conved, res1)

            res2 = self.layer01(out1)
            out2 = torch.add(out1, res2)
            out2 = self.AddTotalDropout(out2, self.dropout01)

            #currSize = out2.size() # 1,16,128,128
            #out2.view(currSize[0]*currSize[1] * currSize[2],currSize[3])
            # Now apply the channel dropout function

            # Reshape it back to the original shape
            #out2.view(currSize[0], currSize[1], currSize[2], currSize[3])

            res3 = self.layer02(out2)
            out3 = torch.add(out2, res3)
            out3 = self.AddTotalDropout(out3, self.dropout02)

            res4 = self.layer03(out3)
            out4 = torch.add(out3, res4)
            out4 = self.AddTotalDropout(out4, self.dropout03)



            # Now, all downsample connections from level 0 to level 1
            down0_1 = self.layer0_to_1(out1)
            down01_1 = self.layer01_to_1(out1)
            down02_1 = self.layer02_to_1(out1)

            # Second row of residual layers is build

            res11 = self.layer11(down0_1)
            out11 = torch.add(down0_1, res11)
            out11 = self.AddTotalDropout(out11, self.dropout11)

             #out11_down0_1 = torch.cat([out11, down01_1], 1)
            out11_down0_1 = torch.add(out11, down01_1)
            res12 = self.layer12(out11_down0_1)
            out12 = torch.add(out11_down0_1, res12)
            out12 = self.AddTotalDropout(out12, self.dropout12)

     #        out12_down0_2 = torch.cat([out12, down02_1], 1)
            out12_down0_2 = torch.add(out12, down02_1)
            res13 = self.layer13(out12_down0_2)
            out13 = torch.add(out12_down0_2, res13)
            out13 = self.AddTotalDropout(out13, self.dropout13)


            # Downsample from Row 1 to 2

            down1_2 = self.layer1_to_2(down0_1)
            down11_2 = self.layer11_to_2(out11_down0_1)
            down12_2 = self.layer11_to_2(out12_down0_2)

            # vertical connections in 2nd Row

            out21 = self.layer21(down1_2)

            out21_down11_2 = torch.add(out21, down11_2)
            out22 = self.layer21(out21_down11_2)
            out22 = self.AddTotalDropout(out22, self.dropout22)

            out22_down11_2 = torch.add(out22, down12_2)
            out23 = self.layer21(out22_down11_2)
            out23 = self.AddTotalDropout(out23, self.dropout23)

            out24 = self.layer21(out23)
            out24 = self.AddTotalDropout(out24, self.dropout24)

            out25 = self.layer21(out24)
            out25 = self.AddTotalDropout(out25, self.dropout25)

            # upsample from row 2 to row 1
            outup23_1 = self.layer23_to_1(out23)
            outup24_1 = self.layer24_to_1(out24)
            outup25_1 = self.layer25_to_1(out25)


            # just vertical connections
            out13_outup23_1 = torch.add(out13, outup23_1)
            res14 = self.layer13(out13_outup23_1)
            out14 = torch.add(out13_outup23_1, res14)
            out14 = self.AddTotalDropout(out14, self.dropout14)

            out14_outup24_1 = torch.add(out14, outup24_1)
            res15 = self.layer13(out14_outup24_1)
            out15 = torch.add(out14_outup24_1, res15)
            out15 = self.AddTotalDropout(out15, self.dropout15)


            # Upward connections from Layer 1 to 0
            outUp13_0 = self.layer13_to_0(out13_outup23_1)
            outUp14_0 = self.layer14_to_0(out14_outup24_1)

            out15_outup25_1 = torch.add(out15, outup25_1)
            outUp15_0 = self.layer15_to_0(out15_outup25_1)



            # finish level 0
            out4_outUp13_0 = torch.add(out4, outUp13_0)
            res5 = self.layer04(out4_outUp13_0)
            out5 = torch.add(out4_outUp13_0, res5)
            out5 = self.AddTotalDropout(out5, self.dropout04)

            out5_outUp14_0 = torch.add(out5, outUp14_0)
            res6 = self.layer05(out5_outUp14_0)
            out6 = torch.add(out5_outUp14_0, res6)
            out6 = self.AddTotalDropout(out6, self.dropout05)

            out6_outUp15_0 = torch.add(out6, outUp15_0)
            res7 = self.layer06(out6_outUp15_0)

            # Reduce number of channels in skip connection, since it is reduced for the output...
            out7_Addconv = self.layerLastAddConv(out6_outUp15_0)
            out7 = torch.add(out7_Addconv ,res7)

            #out7 = res7 # torch.add(out6_outUp15_0, res7)

            return out7

        else:
            if self.cOpt == 0:
                # Create the first block
                res1 = self.firstResLayer(x)
                x_conved = self.layerFirstAddConv(x)
                inX = torch.add(x_conved, res1)

                aDown = [{} for i in range(self.netRows)]
                bUp = [{} for i in range(self.netRows)]

                # For the downsampling, first the rows have to be connected. Then the same input can be overwritten for the next column.
                # For the residual blocks, the column index is reduced by one, since for the first column in the Grid we dont need any of them.


                for col in range(0, self.netCols):
                    for row in range(0, self.netRows):
                        # Connect all layers with downsampling
                        if col < self.nrDownSampleCols:
                            if row == 0 and col == 0:
                                aDown[0] = inX
                            elif row == 0:
                                res = self.resConvLayers[row][col-1]( aDown[row] )
                                aDown[row] = torch.add(res, aDown[row])

                                if self.batchNorm:
                                    aDown[row] = self.bnLayers[row][col](aDown[row])

                                #aDown[row] = self.resConvLayers[row][col-1]( aDown[row] )


                                if self.netRows > 1 and self.enableTotalDropout:
                                    aDown[row] = self.AddTotalDropout(aDown[row], self.dropoutLayer[row][col-1])

                            # Connect the result downstream. If col != 0, also add the value from the left
                            if row != 0 and col == 0:
                                aDown[row] = self.downSampleCols[row-1][col](  aDown[row-1] )
                                #aDown[row] = self.dropoutLayer[row][col](aDown[row])
                            elif row != 0:
                                # Here, add also from the left
                                res = self.resConvLayers[row][col-1](  aDown[row] )
                                left = torch.add(aDown[row], res)
                                if row != self.netRows - 1 and self.enableTotalDropout:
                                    left = self.AddTotalDropout(left, self.dropoutLayer[row][col-1])

                                down = self.downSampleCols[row - 1][col](aDown[row - 1])
                                aDown[row] = self.combineChannels(left, down, self.cOpt)
                                if self.batchNorm:
                                    aDown[row] = self.bnLayers[row][col](aDown[row])
                        else:
                            break

                # bUp[0] = aDwon[0]. Where does this happen??
                #bUp[0] = aDown[0]


                # Now, apply upsampling. This is done starting from
                for col in range(self.nrDownSampleCols, self.netCols):
                    for row in range(self.netRows - 1, -1, -1):
                        if row == self.netRows -1 and col == self.nrDownSampleCols:
                            res = self.resConvLayers[row][col-1](  aDown[row] )
                            left = torch.add(aDown[row], res)
                            bUp[row] = left
                            if self.batchNorm:
                                bUp[row] = self.bnLayers[row][col](bUp[row])

                            if row != self.netRows - 1 and self.enableTotalDropout:
                                bUp[row] = self.AddTotalDropout(bUp[row], self.dropoutLayer[row][col-1])




                        elif col == self.nrDownSampleCols:
                            res = self.resConvLayers[row][col-1](  aDown[row] )
                            left = torch.add(aDown[row], res)

                            if row != self.netRows-1 and self.enableTotalDropout:
                                left = self.AddTotalDropout(left, self.dropoutLayer[row][col-1])

                            up = self.upSampleCols[row+1][col-self.nrDownSampleCols](bUp[row+1])

                            bUp[row] = self.combineChannels(left, up, self.cOpt)
                            if self.batchNorm:
                                bUp[row] = self.bnLayers[row][col](bUp[row])


                        elif row == self.netRows - 1: # bottom row has no incoming upsample
                            res = self.resConvLayers[row][col-1](  bUp[row] )
                            left = torch.add(bUp[row], res)
                            bUp[row] = left
                            if self.batchNorm:
                                bUp[row] = self.bnLayers[row][col](bUp[row])

                            # Dropout not used in last row, so there will be always this path avaialable...
                            #bUp[row] = self.dropoutLayer[row][col](bUp[row])
                        else:
                            res = self.resConvLayers[row][col-1](  bUp[row] )
                            left = torch.add(bUp[row], res)

                            if row != 0 or col != 0: # The very first and last block must not be dropped. Otherwise, there is no connection left...
                                #if col != self.netCols-1: #Maybe the last blocks can be dropped as well. There will always be the outer path
                                if self.enableTotalDropout:
                                    left = self.AddTotalDropout(left, self.dropoutLayer[row][col-1])

                            up = self.upSampleCols[row+1][col-self.nrDownSampleCols](bUp[row+1])

                            bUp[row] = self.combineChannels(left, up, self.cOpt)
                            if self.batchNorm:
                                bUp[row] = self.bnLayers[row][col](bUp[row])


                res = self.resConvLayers[0][self.netCols-1](bUp[0])
                outSkip1Channel = self.lasConvBlock(bUp[0])

                lastOut = torch.add(res, outSkip1Channel)

                return lastOut



            elif self.cOpt == 1:
                # Create the first block
                res1 = self.firstResLayer(x)
                x_conved = self.layerFirstAddConv(x)
                inX = torch.add(x_conved, res1)

                aDown = [{} for i in range(self.netRows)]
                bUp = [{} for i in range(self.netRows)]

                # For the downsampling, first the rows have to be connected. Then the same input can be overwritten for the next column.
                # For the residual blocks, the column index is reduced by one, since for the first column in the Grid we dont need any of them.

                for col in range(0, self.netCols):
                    for row in range(0, self.netRows):
                        # Connect all layers with downsampling
                        if col < self.nrDownSampleCols:
                            if row == 0 and col == 0:
                                aDown[0] = inX
                            elif row == 0:
                                res = self.resConvLayers[row][col - 1](aDown[row])
                                aDown[row] = torch.add(res, aDown[row])

                                if self.batchNorm:
                                    aDown[row] = self.bnLayers[row][col](aDown[row])

                                # aDown[row] = self.resConvLayers[row][col-1]( aDown[row] )

                                if self.netRows > 1 and self.enableTotalDropout:
                                    aDown[row] = self.AddTotalDropout(aDown[row], self.dropoutLayer[row][col - 1])

                            # Connect the result downstream. If col != 0, also add the value from the left
                            if row != 0 and col == 0:
                                aDown[row] = self.downSampleCols[row - 1][col](aDown[row - 1])
                                # aDown[row] = self.dropoutLayer[row][col](aDown[row])
                            elif row != 0:
                                # Here, add also from the left
                                res = self.resConvLayers[row][col - 1](aDown[row])
                                left = torch.add(aDown[row], res)
                                if row != self.netRows - 1 and self.enableTotalDropout:
                                    left = self.AddTotalDropout(left, self.dropoutLayer[row][col - 1])

                                down = self.downSampleCols[row - 1][col](aDown[row - 1])
                                aDown[row] = self.combineChannels(left, down, self.cOpt)
                                if self.batchNorm:
                                    aDown[row] = self.bnLayers[row][col](aDown[row])
                        else:
                            break

                # bUp[0] = aDwon[0]. Where does this happen??
                # bUp[0] = aDown[0]

                # Now, apply upsampling. This is done starting from
                for col in range(self.nrDownSampleCols, self.netCols):
                    for row in range(self.netRows - 1, -1, -1):
                        if row == self.netRows - 1 and col == self.nrDownSampleCols:
                            res = self.resConvLayers[row][col - 1](aDown[row])
                            left = torch.add(aDown[row], res)

                            bUp[row] = left
                            if self.batchNorm:
                                bUp[row] = self.bnLayers[row][col](bUp[row])

                            if row != self.netRows - 1 and self.enableTotalDropout:
                                bUp[row] = self.AddTotalDropout(bUp[row], self.dropoutLayer[row][col - 1])




                        elif col == self.nrDownSampleCols:
                            res = self.resConvLayers[row][col - 1](aDown[row])
                            left = torch.add(aDown[row], res)

                            if row != self.netRows - 1 and self.enableTotalDropout:
                                left = self.AddTotalDropout(left, self.dropoutLayer[row][col - 1])


                            # Reduce the number of channels to the right amount
                            left = self.resConvRedNumChannel[row][col-self.nrDownSampleCols](left)

                            up = self.upSampleCols[row + 1][col - self.nrDownSampleCols](bUp[row + 1])

                            bUp[row] = self.combineChannels(left, up, self.cOpt)
                            if self.batchNorm:
                                bUp[row] = self.bnLayers[row][col](bUp[row])


                        elif row == self.netRows - 1:  # bottom row has no incoming upsample
                            res = self.resConvLayers[row][col - 1](bUp[row])
                            left = torch.add(bUp[row], res)
                            # Reduce the number of channels to the right amount
                            left = self.resConvRedNumChannel[row][col-self.nrDownSampleCols](left)

                            bUp[row] = left

                            if self.batchNorm:
                                bUp[row] = self.bnLayers[row][col](bUp[row])

                            # Dropout not used in last row, so there will be always this path avaialable...
                            # bUp[row] = self.dropoutLayer[row][col](bUp[row])
                        else:
                            res = self.resConvLayers[row][col - 1](bUp[row])
                            left = torch.add(bUp[row], res)

                            if row != 0 or col != 0:  # The very first and last block must not be dropped. Otherwise, there is no connection left...
                                # if col != self.netCols-1: #Maybe the last blocks can be dropped as well. There will always be the outer path
                                if self.enableTotalDropout:
                                    left = self.AddTotalDropout(left, self.dropoutLayer[row][col - 1])
                            # Reduce the number of channels to the right amount
                            left = self.resConvRedNumChannel[row][col-self.nrDownSampleCols](left)
                            up = self.upSampleCols[row + 1][col - self.nrDownSampleCols](bUp[row + 1])

                            bUp[row] = self.combineChannels(left, up, self.cOpt)
                            if self.batchNorm:
                                bUp[row] = self.bnLayers[row][col](bUp[row])

                res = self.resConvLayers[0][self.netCols - 1](bUp[0])
                outSkip1Channel = self.lasConvBlock(bUp[0])

                lastOut = torch.add(res, outSkip1Channel)

                return lastOut












class GridNetFrame2(nn.Module):
    def __init__(self, channelExponent=6, dropout=0.):
        super(GridNetFrame2, self).__init__()
        channels = 8
        channelsCol2 = channels
        channelsCol3 = channelsCol2
        multLev1 = 2
        multLev2 = 3
        self.totalDropout = 0.0

        self.layer0 = ResConvBlocks(2, channels, 'layer0_inp')
        self.layer06 = ResConvBlocks(channels,   1, 'layer06_inp')

    def forward(self, x):
        # First, all horizontal layers in row 0 are connected
        res1 = self.layer0(x)
        out1 = res1 # torch.add(x,res1)

        res7 = self.layer06(out1)
        out7 = res7 # torch.add(out6_outUp15_0, res7)

        return out7