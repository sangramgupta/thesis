
# This scipt can be used to apply masks from testing_mask_dataset onto vector files. 
# The first two channels should contain the u-, v-component of the wind
# The third input channel additionally receives the mask during application.

import os, sys, random
import numpy as np
import argparse
import dataset
import utils

dataDir = "/home/kumpf/projects/Neural_networks/physics_unet/data/10mWind/"
dataDirTest = ""

outputFolderStamps = "/home/kumpf/projects/Neural_networks/physics_unet/data/Masks_on_10m_wind/"
masksFolder =  '/home/kumpf/projects/Neural_networks/image_inpainting/mask/testing_mask_dataset/'
includeSubDirs=True

maxNrFiles = 20000
prop=None 
data = dataset.TurbDataset(prop, shuffle=1, dataDir=dataDir, dataDirTest=dataDirTest,  \
                                maxFiles = maxNrFiles, outputFolderStamps = outputFolderStamps, use_prestamped_data = False, masksFolder = masksFolder, usePrecomputedMasks = True)
