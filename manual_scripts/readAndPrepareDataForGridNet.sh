


# 1.) First, extract the data from netCDF and save into a folder
# !!! Change the folders in the scipt accordingly
# or: first argument input data_dir ("/abc/def/") and second export_dir
# third argument: number of files to extract (can be more, is just a break somewhere)

python3 createTestSetFromNetCDF.py "/home/kumpf/net/KumpfBackup/AusgelagerteDaten/2018_05_Yvonne/netcdf/" "/home/kumpf/projects/Neural_networks/physics_unet/data/FrameDataYvePressure/" 20000

# 2.) Then, compute the optical flow of the data
# !!! also change folder, where filesToWarp.txt is
# or: first argument can point to the file
# Note: The network for the optical flow uses Torch 0.2 and Python 2.7!
cd /home/kumpf/projects/Neural_networks/physics_unet/
/home/kumpf/bin/anaconda3/envs/pwcnet_test/bin/python2.7 addFlowToFiles.py "/home/kumpf/projects/Neural_networks/physics_unet/data/Temperature/filesToWarp.txt"

# 3.) Warp the images and extract feature maps using the conv layer1 of ResNet-18
python3 utils2.py "/home/kumpf/projects/Neural_networks/physics_unet/data/Temperature/filesToWarp.txt" "yes" "yes" "yes"


## Now, the data should be prepared ##



## convert the finished dataset to hdf5


export dataDir='/home/kumpf/net/ramDiskDynamic/FrameData/FrameDataYvePressure/'
export dataDirOut='/mnt/nfs/netdiskcg/datasets/FrameData/'
export fileNameOut='YveFramePressure.hdf5'
export printMessages=0
~/anaconda3/envs/frame1/bin/python3.7 convert_npz_to_h5py.py --dataDir $dataDir --dataDirOut $dataDirOut --fileNameOut $fileNameOut --printMessages $printMessages





