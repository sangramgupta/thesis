# script to read .npz files from one folder and put them all into one hpy5 file.


import h5py
import numpy as np
from os import listdir
import argparse



inputFolder = '/home/kumpf/projects/Neural_networks/physics_unet/data/tmp/in/'
outputFolder = '/home/kumpf/projects/Neural_networks/physics_unet/data/tmp/'
fileNameOut = 'mytestfile.hdf5'
printMessages = 0

#### Parse arguments #################
parser = argparse.ArgumentParser(description='Variables which can be specified.')
parser.add_argument('--dataDir', type=str, help='Directory for input. Default: ...')
parser.add_argument('--dataDirOut', type=str, help='Directory for output. Default: ...')
parser.add_argument('--fileNameOut', type=str, help='Filename for output hdf5 file: Default: mytestfile.hdf5')
parser.add_argument('--printMessages', type=int, help='Should all used filenames etc. be printed on screen? Default: 0')

args = parser.parse_args()
if args.dataDir:
    inputFolder = args.dataDir
if args.dataDirOut:
    outputFolder = args.dataDirOut
if args.fileNameOut:
    fileNameOut = args.fileNameOut
if args.printMessages:
    printMessages = args.printMessages


f = h5py.File(outputFolder + fileNameOut, "w")


files = listdir(inputFolder)
for fin in files:
    if fin.endswith(".npz"):
        npFile = np.load(inputFolder + fin)
        if printMessages:
            print(fin[:-4])
        grp = f.create_group(fin[:-4])

        lst = npFile.files
        for item in lst:
            if printMessages:
                print(item)

            if npFile[item].shape == (): # Most likely an integer. Try to store it as integer...
                dset = grp.create_dataset(item, (1,), compression="lzf",  dtype='float')
                dset[0] = data=npFile[item]
                aaa = 6
            else:
                dset = grp.create_dataset(item, data=npFile[item], compression="lzf")

f.close()

        #dset = grp.create_dataset('a', data=npFile['a'], compression="lzf")
        #dset = grp.create_dataset('imgWarped', data=npFile['imgWarped'], compression="lzf")
        #dset = grp.create_dataset('feat', data=npFile['feat'], compression="lzf")
        #dset = grp.create_dataset('featWarped', data=npFile['featWarped'], compression="lzf")
