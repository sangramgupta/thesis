# Test scipt to read one dataset and apply a pretrained model on it


import os,sys,random,math
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader

from dataset import TurbDataset
# from DfpNet import TurbNetG, weights_init
from VecLearnNet import TurbNetGVec
from VecLearnNet import TurbNetGVecPoly
import utils
from utils import log
import matplotlib.pyplot as plt
from torchvision import models
from torchsummary import summary


dataset = np.load('/home/kumpf/projects/Neural_networks/physics_unet/data/Polynoms/testImages/Lena_128_q_noise.npz')
dataInput = np.zeros((1,1,128,128))
dataTarget = np.zeros((1,1,128,128))
dataInput[0,:,:] = dataset['inp']
dataTarget[0,:,:] = dataset['out']

# normalize the dataset
v_norm = np.max(np.abs(dataInput))


dataInput = dataInput / v_norm
dataTarget = dataTarget / v_norm

targets = Variable(torch.FloatTensor(1, 1, 128, 128))
inputs = Variable(torch.FloatTensor(1, 1, 128, 128))
inputs = torch.tensor(dataInput)
inputs_prepared = Variable(inputs.float().cuda())


#input = torch.tensor(dataInput)
#target = torch.tensor(dataTarget)



#input.cuda()
#target.cuda()

expo = 5
dropout    = 0.

netG = TurbNetGVecPoly(channelExponent=expo, dropout=dropout)


netG.load_state_dict( torch.load('/home/kumpf/projects/Neural_networks/physics_unet/modelG') )
netG.cuda()
netG.eval()
#model = models.vgg16()
#print(model)

#vgg = models.vgg16()
#summary(vgg, (3, 224, 224))

#testLoader = DataLoader(dataInput, batch_size=1)
#inputs_cpu = []
#for i, traindata in enumerate(testLoader, 0):
#    inputs_cpu = traindata

output = netG(inputs_prepared)

