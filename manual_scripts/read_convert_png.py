import imageio
import numpy as np

im = imageio.imread('Lena_128_q_noise.png')

np.savez_compressed('Lena_128_q_noise',  inp = im[:,:,0], out = im[:,:,0])
