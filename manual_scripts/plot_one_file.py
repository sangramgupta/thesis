import numpy as np
import matplotlib.pyplot as plt
a = np.load('/home/kumpf/projects/Neural_networks/physics_unet/data/Polynoms/test/polyDeg15_DZ0.npz')

filen = '/home/kumpf/projects/Neural_networks/physics_unet/data/FrameSkip1/YveT/YvetTime0Ens0Sec1LevSkip1.npz'
a = np.load(filen)



plt.figure()
X, Y = np.meshgrid(np.arange(128), np.arange(128))
U = a['inp'][0, :, :]
V = a['inp'][1, :, :]
L = np.sqrt(U ** 2 + V ** 2)
C = np.array([i / 80.0 for i in L])
#W = np.array([0.005 * l / 80.0 for l in L])
plt.imshow(a['inp'][2, :, :])
Q = plt.quiver(X, Y, U, V, C, pivot='mid', units='xy', headwidth=3, headlength=5)
plt.show()


plt.figure()
plt.pcolormesh(a['a'][0,:, :])
plt.show()
