

import torch
import torch.nn as nn
import torchvision.models as models
from torch.autograd import Variable
import numpy as np
import sys

def LoadAndReturnResNet18FirstConvLayerStride1(saveToFile = False, pathToFile = ''):
    # Load the pretrained model, its downloaded (43 MB) from the internet each time
    resNet = models.resnet18(pretrained=True)

    # Extract the first convolutional layer from the network
    modules=list(resNet.children())[0:1]

    # Create a new network consisting of only that layer
    resNet2 = nn.Sequential(*modules)

    # Create a new conv layer with stride one but the same size
    resNetSingleLayer = nn.Sequential()
    name = '0'

    # Padding has to be used in order to return the same output size as input
    resNetSingleLayer.add_module('%s' % name, nn.Conv2d(3, 64, kernel_size=7, stride=1, padding=3, bias=False))

    # Transfer state to the new model
    resNetSingleLayer.load_state_dict(resNet2.state_dict())

    if saveToFile:
        state = {'state_dict': resNet2.state_dict()}
        torch.save(state, pathToFile + "ResNet18Conv1Stride1")
    # Return the model
    return resNetSingleLayer


def LoadAndUseResNet18AdaptedFirstConvLayer(path = '/home/kumpf/projects/Neural_networks/physics_unet/TrainedNets/ResNet18Conv1Stride1'):

    checkpoint = torch.load(path)

    resNetSingleLayer = nn.Sequential()
    name = '0'

    # Padding has to be used in order to return the same output size as input
    resNetSingleLayer.add_module('%s' % name, nn.Conv2d(3, 64, kernel_size=7, stride=1, padding=3, bias=False))
    resNetSingleLayer.load_state_dict(checkpoint['state_dict'])

    return resNetSingleLayer




# Code courtesy of Rahel Mengyu Chu 2019

import cv2

def warp_flow(img, flowo, reverseXY = False):

    h, w = flowo.shape[:2]

    if reverseXY:
        flow = -flowo[:, :, ::-1]   # the flow is first y channel then x channel. This is corrected here
    else:
        flow = np.copy(flowo)

    flow[:, :, 0] += np.arange(w)
    flow[:, :, 1] += np.arange(h)[:, np.newaxis]

    ih, iw = img.shape[:2]

    flow[:, :, 0] = np.clip(flow[:, :, 0], 0, iw)

    flow[:, :, 1] = np.clip(flow[:, :, 1], 0, ih)

    res = cv2.remap(img, np.float32(flow), None, cv2.INTER_LINEAR) # opencv considers the new positions, not the flow vector, meaning, the value is sampled at the new position.

    return res





def ExtractFeatureMapsWithResNet18Con1LayerAndWarp(filesToProcessPath = '/home/kumpf/projects/Neural_networks/physics_unet/data/Temperature/filesToWarp.txt',
                                            extractFeatureMaps = True,
                                            warpImages = True,
                                            warpFeatureMaps = True):
    resNetLayerNet = LoadAndUseResNet18AdaptedFirstConvLayer()
    resNetLayerNet.cuda()
    resNetLayerNet.eval()
    #resNetLayerNet.no_grad()



    if filesToProcessPath != '':
        with open(filesToProcessPath, 'r') as f:
            for line in f:
                line = line.rstrip()
                print(line)
                # Open .npz file
                npFile = np.load(line)

                # Read img1 and img2
                img1_oneChannel = npFile['a'][0, :, :]
                img2_oneChannel = npFile['a'][1, :, :]

                # Transfrom values into 0-1 range
                # Scale the scalar fields to 0-1 range
                minImg1 = np.min(img1_oneChannel)
                maxImg1 = np.max(img1_oneChannel)
                minImg2 = np.min(img2_oneChannel)
                maxImg2 = np.max(img2_oneChannel)


                img1_oneChannel2 = (img1_oneChannel - minImg1) / (maxImg1 - minImg1)
                img2_oneChannel2 = (img2_oneChannel - minImg2) / (maxImg2 - minImg2)

                img1 = np.zeros((img1_oneChannel2.shape[0] , img1_oneChannel.shape[1], 3))
                img2 = np.zeros((img2_oneChannel2.shape[0] , img2_oneChannel.shape[1], 3))

                # Create RGB Image from scalar fields
                img1[:, :, 0] = img1_oneChannel2
                img1[:, :, 1] = img1_oneChannel2
                img1[:, :, 2] = img1_oneChannel2

                img2[:, :, 0] = img2_oneChannel2
                img2[:, :, 1] = img2_oneChannel2
                img2[:, :, 2] = img2_oneChannel2

                flow = npFile['flow']
                # flow dimensions: (img0->img2 , img2->img0), x, y, (u,v)


                # t = npFile['t']
                t = 0.5

                #img1Feature = []
                #img2Feature = []
                #img1_oneChannel_warped = []
                #img2_oneChannel_warped = []
                #img1FeatureWarped = []
                #img2FeatureWarped = []
                feat = []
                featWarped = []
                imgWarped = []
                xSize = img1_oneChannel.shape[0]
                ySize = img1_oneChannel.shape[1]

                if extractFeatureMaps:
                    # Generate tensor from images
                    img1Input = np.zeros((1, 3, xSize, ySize))
                    img1Input[0,:,:,:] = np.moveaxis(img1, -1, 0)
                    img2Input = np.zeros((1, 3, xSize, ySize))
                    img2Input[0, :, :, :] = np.moveaxis(img2, -1, 0)


                    inputs1 = torch.tensor(img1Input)
                    inputs2 = torch.tensor(img2Input)

                    inputs1_prepared = Variable(inputs1.float().cuda())
                    inputs2_prepared = Variable(inputs2.float().cuda())

                    feat = np.zeros((2, xSize, ySize))

                    #temp = resNetLayerNet(inputs1_prepared)
                    feat[0, :, :] = resNetLayerNet(inputs1_prepared).cpu().data.numpy()[0,0,:,:]
                    feat[1, :, :] = resNetLayerNet(inputs2_prepared).cpu().data.numpy()[0,0,:,:]

                if warpImages:
                    imgWarped = np.zeros((2, xSize, ySize))
                    imgWarped[0, :, :] = warp_flow(img1_oneChannel, flow[0,:,:,:] * t, True)
                    imgWarped[1, :, :] = warp_flow(img2_oneChannel, flow[1,:,:,:] * t, True)

                    # Rescale it to its original range ## Not needed anymore
                    #imgWarped[0, :, :] = imgWarped[0, :, :] * (maxImg1 - minImg1) + minImg1
                    #imgWarped[1, :, :] = imgWarped[1, :, :] * (maxImg2 - minImg2) + minImg2

                if warpFeatureMaps:
                    featWarped = np.zeros((2, xSize, ySize))
                    featWarped[0, :, :] = warp_flow(feat[0, :, :], flow[0,:,:,:] * t, True)
                    featWarped[1, :, :] = warp_flow(feat[1, :, :], flow[1,:,:,:] * t, True)





                # Save everything into the file.
                if extractFeatureMaps and warpImages and warpFeatureMaps:
                    np.savez_compressed(line, a=npFile['a'], flow=flow, feat = feat, imgWarped = imgWarped, featWarped = featWarped, stepSize = t)


    return

def AppendFeatureMapsAndWarpedMapsToFiles():
    return



def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if len(sys.argv) >= 3:
    print('sys.argv[0]: ' + sys.argv[0])
    print('sys.argv[1]: ' + sys.argv[1])
    print('sys.argv[2]: ' + sys.argv[2])

if len(sys.argv) == 2:
    ExtractFeatureMapsWithResNet18Con1LayerAndWarp(filesToProcessPath = sys.argv[1])

elif len(sys.argv) == 3:
    ExtractFeatureMapsWithResNet18Con1LayerAndWarp(filesToProcessPath=sys.argv[1], extractFeatureMaps = str2bool(sys.argv[2]))

elif len(sys.argv) == 4:
    ExtractFeatureMapsWithResNet18Con1LayerAndWarp(filesToProcessPath=sys.argv[1],
                                                   extractFeatureMaps=str2bool(sys.argv[2]),
                                                   warpImages=str2bool(sys.argv[3]))
elif len(sys.argv) == 5:
    ExtractFeatureMapsWithResNet18Con1LayerAndWarp(filesToProcessPath=sys.argv[1],
                                                   extractFeatureMaps=str2bool(sys.argv[2]),
                                                   warpImages=str2bool(sys.argv[3]),
                                                   warpFeatureMaps=str2bool(sys.argv[4]))

# Lines of code which execute parts #

#ExtractFeatureMapsWithResNet18Con1LayerAndWarp()

## Extract the first conv layer of ResNet18 and save it to file
# LoadAndReturnResNet18FirstConvLayerStride1(saveToFile = True, pathToFile = '/home/kumpf/projects/Neural_networks/physics_unet/')