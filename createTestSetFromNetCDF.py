from  enstools.io import read as ensread
from enstools.io import write as enswrite
from enstools.io import drop_unused
import numpy as np
from os import listdir
from os import path
from os import makedirs
import sys
import random

# import subprocess


xSize = 300
ySize = 300
ecmwfGrib = False # If this is true, a different shape is assumed for the input data...
ecmwfGribDimAdaptor = 0 # This variable will be adjusted in the script. Do not change it here.

    #data_dir = "/home/local/data/2018/Karl_Marlene/"
#  data_dir = '/home/kumpf/net/KumpfBackup/AusgelagerteDaten/2019/Mars_2019_01_Analysis_0000_1200/FC/'
#  exportDir = '/home/kumpf/net/KumpfBackup/AusgelagerteDaten/2019/Mars_2019_01_Analysis_0000_1200/'


data_dir = '/home/kumpf/net/KumpfBackup/AusgelagerteDaten/2019/Highres_250m_Daten_Christian_Barthlott/z/Train/'
exportDir = '/home/kumpf/net/KumpfBackup/AusgelagerteDaten/2019/Highres_250m_Daten_Christian_Barthlott/z/Train/npz/'


ensDim = False


    #exportDir = "/home/kumpf/projects/Neural_networks/physics_unet/data/FrameDataMarlenePT/" #"/home/kumpf/projects/Neural_networks/physics_unet/data/Temperature/"

## Optional input arguments:
## argv1 = data_dir
## argv2 = exportDir
## argv3 = stop script after exporting x files
## argv4 = only files containing x, can be skipped using "None"
## argv5 = specify the variable name
## argv6 = "2D" or "3D" data


stopScriptAfterFile = 100000
if len(sys.argv) >= 2:
    data_dir = sys.argv[1]
    exportDir = sys.argv[2]

if len(sys.argv) >= 3:
    stopScriptAfterFile = int(sys.argv[3])

fileFilter = []
#fileFilter = "wind"
#fileFilter = "THerrorENS"
if len(sys.argv) >= 4:
    if sys.argv[4] != "None" and sys.argv[4] != "none":
        fileFilter = sys.argv[4]

passedVarName = []
#passedVarName = "Potential_temperature_member"
if len(sys.argv) >= 5:
    if sys.argv[5] != "None" and sys.argv[5] != "none":
        passedVarName = sys.argv[5]

cutFrom2Dor3D = "3D"
if len(sys.argv) >= 6:
    cutFrom2Dor3D = sys.argv[6]

print('read from directory: ' + data_dir)
print('write to directory: ' + exportDir)
print('export at least ' + str(stopScriptAfterFile) + ' files')


if not path.exists(exportDir):
    makedirs(exportDir)

files = listdir(data_dir)
files.sort()


variable = "Temperature" #"10mWind" # or "10mWind" or "Temperature"

allFileNames = []
stopScript = False   # Simple way to stop the script at a certain point. Finishes traversing the current file, then breaks the loop

ifileNr = -1
print('starting reading data...')
print('files will be exported to ' + str(exportDir))
for i, file in enumerate(files):
    if stopScript:
        break

    if fileFilter != []:
        if not file.__contains__(fileFilter):
            continue

    print(str(i))
    currFilename = []
    variableNames = []
    variableDim2D = False
    variableDim3D = False
    maxLevDist = 1         # Specify, which spacing the levels have. If greater than one, the number is randomed for each piece

    includeWarpedInp = False  # Will be mapped to channels ... in the file
    includeContext = False  # Will be mapped to channels ... in the file
    includeWarpedContext = False  # Will be mapped to channels ... in the file

    variableDimShift = 0
    if cutFrom2Dor3D == "Temperature" or cutFrom2Dor3D == "3D":
        currFilename = "wind"
        variableNames = ["t"]
        # The variable name here specifies, which variable will be read. The variable itself just refers to the method used, i.e.,
        # 'Temperature' means that the data is taken out of a 3D domain, etc...
        #variableNames = ["pres"]
        if passedVarName != []:
            variableNames = [passedVarName]


        variableDim3D = True

        includeWarpedInp = True
        maxLevDist = 4

        if variableDim3D == True:
            variableDimShift = int(ensDim)
            print('Check variableDimShift for 3D. It has been set to 0 for this dataset, but 1 might be the correct one. check line 186 where endY is compared.')

        if ecmwfGrib:
            variableDimShift = 0
            ecmwfGribDimAdaptor = -1


    elif variable == "10mWind" or cutFrom2Dor3D == "2D":
        currFilename = "z0"
        variableNames = ["10u", "10v"]
        outFormat = 'only data channels' # Whether the output should be only the 2 channels with data, or more channels where some are empty

        if passedVarName != []:
            variableNames = [passedVarName]
        variableDim2D = True
    if not path.isdir(data_dir + file):
        if True: #file.__contains__(currFilename):
            ds = ensread("{0}{1}".format(data_dir, file))

            ifileNr +=1
            print(file)
            print(str(variableNames[0]))

            variableShape=ds[variableNames[0]].shape

            # for each ensemble member

            if True:
                for j in range(0, variableShape[1 + ecmwfGribDimAdaptor]):
                    startLev = 0
                    endLev = 0
                    k = -1
                    while(True):
                        if stopScript:
                            break
                        startLev = endLev
                        #currLevSkip = round(random.uniform(1,maxLevDist))
                        currLevSkip = 1
                        endLev = startLev + 2 * currLevSkip + 1

                        # If the end level is too high (in 3D only...), the next file is taken
                        if ensDim:
                            if endLev > variableShape[2 + ecmwfGribDimAdaptor] and variableDim3D == True:
                                break
                        else:
                            if endLev > variableShape[1 + ecmwfGribDimAdaptor] and variableDim3D == True:
                                break

                        startX = 0
                        startY = 0
                        endX = 0
                        while(True):
                            if stopScript:
                                break
                            startX = endX
                            endX = endX + xSize
                            if (endX > variableShape[2 + variableDimShift]):
                                break

                            startY = 0
                            endY = 0
                            while(True):
                                if stopScript:
                                    break

                                startY = endY
                                endY = endY + ySize
                                if (endY > variableShape[3 + variableDimShift]):
                                    break

                                k += 1

                                # Cut out pieces of prescribed size until no more are possible.
                                # We start at the south, since the alps are more interesting than the north
                                if variable == "10mWind":
                                    print('length of all filenames: ' + str(len(allFileNames)))

                                    if len(allFileNames) == stopScriptAfterFile:
                                        stopScript = True
                                        print('Trying to come to an end with exporting files')
                                        break

                                    allFileNames.append(exportDir + filenameNPZ + '.npz')

                                    uWindParcel = np.array(ds["10u"].data[0, j, startX:endX, startY:endY])
                                    vWindParcel = np.array(ds["10v"].data[0, j, startX:endX, startY:endY])

                                    currParcel = []
                                    if outFormat == 'only data channels':
                                        # write parcels to file
                                        currParcel = np.zeros((2, xSize, ySize))
                                        currParcel[0, :, :] = uWindParcel
                                        currParcel[1, :, :] = vWindParcel
                                    else:
                                        # write parcels to file
                                        currParcel = np.zeros((6, xSize, ySize))
                                        currParcel[4, :, :] = uWindParcel
                                        currParcel[5, :, :] = vWindParcel

                                    filenameNPZ = "YveTime" + str(ifileNr) + "Ens" + str(j) + "Sec" + str(k)
                                    np.savez_compressed(exportDir + filenameNPZ, a=currParcel)

                                elif variable == "Temperature":
                                    # missing loop through levels

                                    # temperature = np.array(ds[variableNames].data[0, j, startLev:endLev, startX:endX, startY:endY])
                                    temperature = []
                                    if ecmwfGrib:
                                        temperature = np.array(ds[variableNames[0]].data[j,
                                                               [startLev, startLev + currLevSkip,
                                                                startLev + 2 * currLevSkip], startX:endX, startY:endY])
                                    else:
                                        if ensDim:
                                            temperature = np.array(ds[variableNames[0]].data[0, j, [startLev, startLev + currLevSkip , startLev + 2 * currLevSkip], startX:endX, startY:endY])
                                        else:
                                            temperature = np.array(ds[variableNames[0]].data[0, [startLev, startLev + currLevSkip, startLev + 2 * currLevSkip], startX:endX, startY:endY])

                                    #addDim = 0
                                    #if includeWarpedInp == True:
                                    #    addDim = addDim + 2
                                    #if includeContext == True:
                                    #    addDim = addDim + 2
                                    #if includeWarpedContext == True:
                                    #    addDim = addDim + 2

                                    # write parcels to file
                                    currParcel = np.zeros((3, xSize, ySize))
                                    currParcel[0, :, :] = np.squeeze(temperature)[0,:,:]
                                    currParcel[1, :, :] = np.squeeze(temperature)[2,:,:]
                                    currParcel[2, :, :] = np.squeeze(temperature)[1,:,:]


                                    #filenameNPZ = "Yve" + variableNames[0] + "Time" + str(ifileNr) + "Ens" + str(j) + "Sec" + str(k) + "LevSkip" + str(currLevSkip)
                                    filenameNPZ = "Hres" + variableNames[0] + "Time" + str(ifileNr) + "Ens" + str(j) + "Sec" + str(k) + "LevSkip" + str(currLevSkip)
                                    if ecmwfGrib:
                                        filenameNPZ = "ECMWF" + variableNames[0] + "Time" + str(j) + "Sec" + str(k) + "LevSkip" + str(currLevSkip)

                                    if includeWarpedInp == True:
                                        print('length of all filenames: ' + str(len(allFileNames)))

                                        if len(allFileNames) == stopScriptAfterFile:
                                            stopScript = True
                                            print('Trying to come to an end with exporting files')
                                            break

                                        allFileNames.append(exportDir + filenameNPZ + '.npz')



                                    np.savez_compressed(exportDir + filenameNPZ, a=currParcel)

                        # In 2D, there is only one level...
                        if variableDim2D == True:
                            break
                        if stopScript == True:
                            break


    if stopScript == True:
        break


if allFileNames != []:
    with open(exportDir + '/filesToWarp.txt', 'w') as f:
        for item in allFileNames:
            f.write("%s\n" % item)