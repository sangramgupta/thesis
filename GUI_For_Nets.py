

# By Alexander Kumpf
# GUI for applying Neural Networks to input data, to evaluate them on specific datasets etc...

from tkinter import *
from tkinter import font
from tkinter import filedialog
from tkinter import ttk
import os, sys, random
import numpy as np
import copy
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torch.optim as optim
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from mpl_toolkits.axes_grid1 import make_axes_locatable

import pickle

import argparse

from os import listdir
from os import path
from os import walk
from pathlib import Path

from DfpNet import TurbNetG, weights_init
from VecLearnNet import UNetFrame
from models.enhancenet import EnhanceNet

import dataset
import utils
from utils import str2bool
import math
from dotmap import DotMap
from natsort import natsorted, ns

from GridNetUtils import GridNetFrame as gnf

from tensorboardX import SummaryWriter
import torchviz

from tqdm import tqdm
from console_progressbar import ProgressBar
from pytictoc import TicToc

import losses




class ModifiedMixin:
    '''
    Class to allow a Tkinter Text widget to notice when it's modified.

    To use this mixin, subclass from Tkinter.Text and the mixin, then write
    an __init__() method for the new class that calls _init().

    Then override the beenModified() method to implement the behavior that
    you want to happen when the Text is modified.
    '''

    def _init(self):
        '''
        Prepare the Text for modification notification.
        '''

        # Clear the modified flag, as a side effect this also gives the
        # instance a _resetting_modified_flag attribute.
        self.clearModifiedFlag()

        # Bind the <<Modified>> virtual event to the internal callback.
        self.bind('<<Modified>>', self._beenModified)

    def _beenModified(self, event=None):
        '''
        Call the user callback. Clear the Tk 'modified' variable of the Text.
        '''

        # If this is being called recursively as a result of the call to
        # clearModifiedFlag() immediately below, then we do nothing.
        if self._resetting_modified_flag: return

        # Clear the Tk 'modified' variable.
        self.clearModifiedFlag()

        # Call the user-defined callback.
        self.beenModified(event)

    def beenModified(self, event=None):
        '''
        Override this method in your class to do what you want when the Text
        is modified.
        '''
        pass

    def clearModifiedFlag(self):
        '''
        Clear the Tk 'modified' variable of the Text.

        Uses the _resetting_modified_flag attribute as a sentinel against
        triggering _beenModified() recursively when setting 'modified' to 0.
        '''

        # Set the sentinel.
        self._resetting_modified_flag = True

        try:

            # Set 'modified' to 0.  This will also trigger the <<Modified>>
            # virtual event which is why we need the sentinel.
            self.tk.call(self._w, 'edit', 'modified', 0)

        finally:
            # Clean the sentinel.
            self._resetting_modified_flag = False

class TextM(ModifiedMixin, Text):
    '''
    Subclass both ModifiedMixin and Tkinter.Text.
    '''
    def emptyFn(self):
        return None

    def __init__(self,  *a, **b):
        # Create self as a Text.
        Text.__init__(self, *a, **b)

        self.count = 0
        self.fn = self.emptyFn
        # Initialize the ModifiedMixin.
        self._init()

    def beenModified(self, event=None):
        '''
        Override this method do do work when the Text is modified.
        '''
        self.count += 1
        if self.count % 2 != 0:
            print('Hi there.')
            self.fn()

    def setFn(self, fn):
        self.fn = fn








class Application(Frame):

    def loadPytorchModel(self):
        selectedModelFileDir = self.model_dir_label['text'] + '/' + self.chosenModel_var.get()
        print(self.device)
        checkpoint = torch.load(selectedModelFileDir, map_location=lambda storage, loc: storage)
        #self.model.load_state_dict(checkpoint['model'])
        self.model = checkpoint['model']
        #optimizer.load_state_dict(checkpoint['optimizer'])  # pytorch 0.3.1 has a bug on this, it's fix in master
        del checkpoint  # dereference seems crucial
        torch.cuda.empty_cache()

        if str(self.device) != 'cpu':
            print('GPU is selected')
            self.model.to(self.device)

        if False:
            checkpoint = torch.load(selectedModelFileDir, map_location='cpu')
            self.model = checkpoint['model']
            print(self.device)
            self.model.to(self.device)

        #self.model.cuda()
        for p in self.model.parameters():
            p.requires_grad = False
        self.model.eval()


        # Check, whether a keylist was exported
        keysFile = Path(self.model_dir_label['text'] + '/' + 'PCRenderKeysToLoad.pkl')
        if keysFile.exists():
            with open(keysFile, 'rb') as f:
                self.keysToLoad= pickle.load(f)
                self.bkeysToLoadPickle = True

                self.fillDropDowns()

                self.setDataVars()

    def update_option_menu(self, om, strs, var):

        menu = om['menu']
        menu.delete(0, "end")
        menu.option_clear()
        stdStr = 'none'
        menu.add_command(label=stdStr,
                         command=lambda value=stdStr: var.set(value))
        for string in strs:
            menu.add_command(label=string,
                             command=lambda value=string: var.set(value))

    def get_om_index(*x, om_var):
        s = om_var.get()
        a = int(s[s.rfind(" "):])
        print(a)
        return a

    def get_om_index2(self, om_var, list):
        s = om_var.get()
        a = list.index(s)
        return a

    def get_om_index2withCheck(self, om_var, list):
        s = om_var.get()
        if s in list:
            a = list.index(s)
        else:
            a = -1
        return a

    def get_str_index2withCheck(self, s, list):

        if s in list:
            a = list.index(s)
        else:
            a = -1
        return a

    def fillDropDowns(self):
        inpKeys, tarKeys, inpKeysStr, tarKeysStr = utils.keylistInpTarKeys(self.keysToLoad)

        self.inpKeys = inpKeys
        self.tarKeys = tarKeys

        self.inpKeysLst = inpKeysStr
        self.tarKeysLst = tarKeysStr

        self.update_option_menu(self.dropDown1, inpKeysStr, self.dropDown1_var)
        self.update_option_menu(self.dropDown2, tarKeysStr, self.dropDown2_var)
        #self.update_option_menu(self.dropDown3, inpKeysStr, self.dropDown3_var)

        self.update_option_menu(self.dropDown4, inpKeysStr, self.dropDown4_var)
        self.update_option_menu(self.dropDown5, tarKeysStr, self.dropDown5_var)
        #self.update_option_menu(self.dropDown6, inpKeysStr, self.dropDown6_var)



        # TODO: Implement


    # Looks for trained models in a given directory
    def collectAvailableModels(self, model_dir):
        files = listdir(model_dir)
        files = [k for k in files if '.pth' in k]
        #files.sort(key=int)
        files = natsorted(files, alg=ns.IGNORECASE)
        availableModels = files

        self.chosenModel_dropdown['values'] = files

        #menu = self.chosenModel_dropdown["menu"]
        #menu.delete(0, "end")
        #for string in files:
        #    menu.add_command(label=string,
        #                     command=lambda value=string:
        #                     self.chosenModel_var.set(value))



    def browse_button_model(self):
        # Allow user to select a directory and store it in global var
        # called folder_path
        global folder_path
        filename = filedialog.askdirectory(initialdir=self.model_dir_label['text'])
        model_dir = filename
        self.model_dir_label['text'] = filename
        self.collectAvailableModels(filename)
        print(filename)

    def initializeDataset(self):

        self.data = dataset.PCRDataset([], shuffle=0, dataDir=self.data_dir_label['text'], dataDirTest=self.dataDir, includeSubDirsArg=self.includeSubDirs, \
                                       maxFiles=1000000, readDataOnTheFly=True, readDataFromH5Py=self.useHDF5Files,
                                       useRawInput=self.useRawInput, useGradientOfInput=self.useGradientOfInput, useLinearInterpInput=self.useLinearInterpInput,
                                       useWarpedInput=self.useWarpedInput, useRawFeatureMaps=self.useRawFeatureMaps,
                                       useWarpedFeatureMaps=self.useWarpedFeatureMaps, gridSizex=self.gridSizex, gridSizey=self.gridSizey, filterword=self.filterByFileName,
                                       flipXNow=False, flipYNow=False, cropInputBasedOnChannel=self.cropInputBasedOnChannel,
                                       centerPatch=self.centerValidationPatch,
                                       dataResolutionX=self.dataResolutionX, dataResolutionY=self.dataResolutionY, maxValFiles=1,
                                       pcopts=self.pcrenderopts)
        self.dataLoader = DataLoader(self.data, batch_size=1, shuffle=False, drop_last=True)

        # set scale min max
        self.dataScale.configure(to=self.data.totalLength - 1)

    def browse_button_data(self):
        # Allow user to select a directory and store it in global var
        # called folder_path
        global folder_path
        filename = filedialog.askdirectory(initialdir=self.data_dir_label['text'])
        model_dir = filename
        self.data_dir_label['text'] = filename
        self.dataDir = filename

        self.dataResolutionX = int(self.dataResX_tbox.get("1.0",END))
        self.dataResolutionY = int(self.dataResY_tbox.get("1.0",END))

        self.gridSizex = int(self.gridSizex_tbox.get("1.0",END))
        self.gridSizey = int(self.gridSizey_tbox.get("1.0",END))

        self.initializeDataset()
        print(filename)

    def change_selectedModel(self, *args):
        print( self.chosenModel_var.get() )
        self.loadPytorchModel()
        self.dataScaleMoved()

    def setDataVars(self):
        self.topRowSkip = 0

        self.dataDir = self.data_dir
        self.includeSubDirs = False
        self.useHDF5Files = True
        self.filterByFileName = ""  # ""Sec1"

        self.useRawInput = True
        self.useGradientOfInput = True
        self.useLinearInterpInput = True
        self.useWarpedInput = False
        self.useRawFeatureMaps = False
        self.useWarpedFeatureMaps = False

        self.renderedDataType = 'shaded'
        self.useColor = False
        self.useTargetColor = False
        self.useTarSplatsColor = False




        self.useNormals = False
        self.useSparseSplatsNormal=False
        self.useNormalsScreen=False
        self.useSparseSplatsNormalScreen=False

        self.useDepth = False
        self.useSparseSplatsDepth=True
        self.useTargetNormals = False
        self.useTarNormalsScreen=False
        self.useTarSplatsNormals = False
        self.useTarSplatsNormalsScreen=False


        self.useTargetDepth = True
        self.useTarSplatsDepth = False

        self.useLearnDepthMask = True
        self.useLearnDepthMaskSplats = False
        self.useGTMask = False

        self.bSparseSplatsColor = False


        # Dataaugmentation and permutation of inputChannels
        self.flipXNow = True
        self.flipYNow = True

        self.cropInputBasedOnChannel = -1

        self.gridSizex = 896
        self.gridSizey = 896

        self.dataResolutionX = 1024
        self.dataResolutionY = 1024

        self.centerValidationPatch = True

        self.useDataDirAlt = False
        prop = []

        keysToLoad = []
        if self.renderedDataType == 'shaded':
            if self.bkeysToLoadPickle!=True:
                # keysToLoad = utils.createNewInputKeysList(bNormals=self.useNormals, bTarNormals=self.useTargetNormals, bColors=self.useColor,
                #                                      bTarColors=self.useTargetColor, bDepth=self.useDepth, bTarDepth=self.useTargetDepth,
                #                                      bLearnDepthMask=self.useLearnDepthMask, bLearnDepthMaskSplats = self.useLearnDepthMaskSplats,
                #                                      bUseGTMask=self.useGTMask,
                #                                      bTarSplatsDepth=self.useTarSplatsDepth, bTarSplatsNormals=self.useTarSplatsNormals,
                #                                      bTarSplatsColor=self.useTarSplatsColor,
                #                                      bSparseSplatsColor = self.bSparseSplatsColor)
                keysToLoad = utils.createNewInputKeysList(bNormals=self.useNormals, bSparseSplatsNormal=self.useSparseSplatsNormal,
                                                          bNormalsScreen=self.useNormalsScreen, bSparseSplatsNormalScreen=self.useSparseSplatsNormalScreen,
                                                          bTarNormals=self.useTargetNormals, bTarNormalsScreen=self.useTarNormalsScreen,
                                                          bColors=self.useColor,
                                                          bSparseSplatsColor=self.bSparseSplatsColor,
                                                          bTarColors=self.useTargetColor, bDepth=self.useDepth, bSparseSplatsDepth=self.useSparseSplatsDepth,
                                                          bTarDepth=self.useTargetDepth,
                                                          bTarSplatsDepth=self.useTarSplatsDepth, bTarSplatsNormals=self.useTarSplatsNormals,
                                                          bTarSplatsNormalsScreen=self.useTarSplatsNormalsScreen,
                                                          bTarSplatsColor=self.useTarSplatsColor,
                                                          bLearnDepthMask=self.useLearnDepthMask, bLearnDepthMaskSplats=self.useLearnDepthMaskSplats,
                                                          bUseGTMask=self.useGTMask)
            else:
                keysToLoad = self.keysToLoad


        self.pcrenderopts = DotMap()
        self.pcrenderopts.useNormals = self.useNormals
        self.pcrenderopts.useDepth = self.useDepth
        self.pcrenderopts.useTargetNormals = self.useTargetNormals
        self.pcrenderopts.useTargetDepth = self.useTargetDepth
        self.pcrenderopts.useLearnDepthMask = self.useLearnDepthMask
        self.pcrenderopts.useGTMask = self.useGTMask
        self.pcrenderopts.keysToLoad = keysToLoad




        self.initializeDataset()

    def depthGTInputChanged(self):
        self.useGTMask = (self.depthGTInput.get()==1)
        self.pcrenderopts.useGTMask = self.useGTMask
        self.initializeDataset()

    def shadedInputChanged(self):
        if (self.shadedInput.get()==1):
            self.renderedDataType = 'shaded'
        else:
            self.renderedDataType = ''
        self.setDataVars()
        self.initializeDataset()

    def renderTopRowFromChVarChanged(self):
        self.topRowSkip = int(self.renderTopRowFromChVar_tbox.get("1.0", END))
        self.renderPCRenderimages()




    def addColorbar(self, subplt,  ax1, currMin, currMax):
        divider = make_axes_locatable(subplt)
        cax = divider.append_axes('right', size='5%', pad=0.05)
        cbar = self.f.colorbar(ax1, cax=cax, orientation='vertical')

        cbar.ax.set_xticklabels(['{}'.format(currMin), '{}'.format(currMax)])

        pos = cbar.ax.get_position()
        # cbar.ax.set_aspect('auto')
        ax2 = cbar.ax.twinx()
        ax2.set_ylim([currMin, currMax])

        # resize the colorbar (otherwise it overlays the plot)
        pos.x0 = pos.x1  # - 0.05

        ax2.set_position(pos)

        #self.allAxs.append(cax)
        #self.allAxs.append(cbar.ax)
        self.allAxs.append(ax2)

    def dataSampleSpecified(self, event):
        #sample = int(self.curDataSample.get())
        sample = int(eval(self.curDataSample.get()))
        if sample < self.data.totalLength:
            self.dataScale.set(sample)

    def reconstructNormalFromDepth(self, depth):
        depth = np.squeeze(depth)

        zy, zx = np.gradient(depth)
        # You may also consider using Sobel to get a joint Gaussian smoothing and differentation
        # to reduce noise
        # zx = cv2.Sobel(d_im, cv2.CV_64F, 1, 0, ksize=5)
        # zy = cv2.Sobel(d_im, cv2.CV_64F, 0, 1, ksize=5)

        normal = np.dstack((-zx, -zy, np.ones_like(depth)))
        n = np.linalg.norm(normal, axis=2)
        normal[:, :, 0] /= n
        normal[:, :, 1] /= n
        normal[:, :, 2] /= n

        # offset and rescale values to be in 0-255
        normal += 1
        normal /= 2
        # normal *= 255
        normal = np.moveaxis(normal, 2,0)
        return normal

    def dataScaleMoved(self, blank = []):
        with torch.no_grad():
            idx = self.dataScale.get()

            #self.dataLoader.
            self.selectedItem = self.data.__getitem__(idx)
            input = []
            target = []
            idxes = []
            damaged = []
            if str(self.device) != 'cpu':
                input, target, idxes, damaged, maskPC = torch.from_numpy(np.float32(self.selectedItem[0])).to(self.device).unsqueeze(0), torch.from_numpy(np.float32(self.selectedItem[1])).to(self.device).unsqueeze(0), \
                                            self.selectedItem[4], self.selectedItem[5], torch.from_numpy(np.float32(self.selectedItem[6])).to(self.device).unsqueeze(0)
            else:
                input, target, idxes, damaged, maskPC = torch.from_numpy(np.float32(self.selectedItem[0])).unsqueeze(0), torch.from_numpy(np.float32(self.selectedItem[1])).unsqueeze(0), \
                                                self.selectedItem[4], self.selectedItem[5], torch.from_numpy(np.float32(self.selectedItem[6])).to(self.device).unsqueeze(0)

            self.tic.tic()
            prediction = []
            if self.model == []:
                prediction = target
            else:
                prediction = self.model(input[:, 0:input.shape[1] - 3*int(self.bSparseSplatsColor) ,: ,:],maskPC)
            self.tic.toc('Inference took ')

            if isinstance(prediction, tuple):
                prediction = prediction[0]

            # prediction = torch.clamp(prediction, 0, 1)

            if self.lossBorder > 0:
                input = losses.LossNet.pad(input, self.lossBorder)
                target = losses.LossNet.pad(target, self.lossBorder)
                prediction = losses.LossNet.pad(prediction, self.lossBorder)

            self.input = input
            self.target = target
            self.prediction = prediction

            self.depthComparison_target = []
            self.depthComparison_prediction = []
            self.depthComparison_difference = []
            curridx = -1
            if self.depthComparisonB.get() == 1 and  self.model != []:
                curridx = self.get_str_index2withCheck('PointsDepth' ,self.tarKeysLst)
                if curridx == -1:
                    curridx = self.get_str_index2withCheck('SplatsDepth', self.tarKeysLst)
                    if curridx == -1:
                        print('No depth in the field... ')

            if curridx >= 0:
                findTarDepthChannel = self.tarKeys[curridx][4]


                #self.depthComparison_target = np.expand_dims( self.data.computeDepthLearnMask1(target.cpu().numpy()[:,3], input.cpu().numpy()[:,3]), axis = 0 )
                #self.depthComparison_prediction = np.expand_dims(prediction.cpu().numpy()[:,4,:,:], axis = 0 )

                # This is only computed to render the difference map, where the depth does not match after training.
                #findTarDepthChannel = 0
                #for iKey in self.pcrenderopts.keysToLoad:
                #    if iKey[2] == 'tar' and iKey[0] != 'PointsDepth':
                #        findTarDepthChannel += iKey[1]
                #    elif iKey[2] == 'tar' and iKey[0] == 'PointsDepth':
                #        break

                self.depthComparison_difference = np.expand_dims( self.prediction.cpu().numpy()[:,findTarDepthChannel,:,:] - self.target.cpu().numpy()[:,findTarDepthChannel,:,:], axis = 0 )

            self.renderImages()


    def renderImages(self):
        modeForTraining = 'pcrender'
        if modeForTraining == 'pcrender':
            self.renderPCRenderimages()


    def renderPCRenderimages(self):

        input = self.input
        prediction = self.prediction
        target = self.target


        # inputs_denormalized_n = self.data.denormalizeFrame(input.cpu().numpy(), [idx], 'inp')
        # outputs_denormalized = self.data.denormalizeFrame(prediction.cpu().numpy(), [idx], 'tar')
        # targets_denormalized = self.data.denormalizeFrame(target.cpu().numpy(), [idx], 'tar')

        # Test on not denormalized images
        inputs_denormalized_n = input.cpu().numpy()
        outputs_denormalized = prediction.cpu().numpy()
        targets_denormalized = target.cpu().numpy()


        if self.depthComparisonB.get() == 1 and  self.model != []:

            inputs_denormalized_n = np.concatenate([inputs_denormalized_n, self.depthComparison_difference], axis=1)

            findInpDepthChannel = 0
            for iKey in self.pcrenderopts.keysToLoad:
                if iKey[2] == 'inp' and iKey[0] != 'PointsSparseDepth':
                    findInpDepthChannel += iKey[1]
                elif iKey[2] == 'inp' and iKey[0] == 'PointsSparseDepth':
                    break


            # Assumes that the depth mask is stored in the last channel of the target and prediction
            a = np.sum((inputs_denormalized_n[:,findInpDepthChannel,:,:] < 0.9999)- targets_denormalized[:,-1,:,:])
            b = np.sum(outputs_denormalized[:,-1]- targets_denormalized[:,-1,:,:])
            print(" Input depth != 1 -  Mask_target ")
            print(a)
            print(" Mask_prediction -  Mask_target ")
            print(b)


            #inputs_denormalized_n = np.concatenate([inputs_denormalized_n, self.depthComparison_prediction], axis=1)

            #outputs_denormalized = np.concatenate([outputs_denormalized, self.depthComparison_difference], axis=1)
            #targets_denormalized = np.concatenate([targets_denormalized, self.depthComparison_target], axis=1)

            #np.sum(inputs_denormalized_n[:,3,:,:] * (inputs_denormalized_n[:,3,:,:] !=1)) - np.sum(targets_denormalized[:,4,:,:])

        self.globMinNormals = float(self.upperClimMin_tbox.get("1.0", END))
        self.globMaxNormals = float(self.upperClimMax_tbox.get("1.0", END))

        self.globMinDepth = float(self.lowerClimMin_tbox.get("1.0", END))
        self.globMaxDepth = float(self.lowerClimMax_tbox.get("1.0", END))

        self.upperRowChnlStart = int(self.channelForTopRowStart_tbox.get("1.0", END))
        self.upperRowChnlAdd = int(self.channelForTopRowNr_tbox.get("1.0", END))

        self.lowerRowChnlStart = int(self.channelForBottomRowStart_tbox.get("1.0", END))
        self.lowerRowChnlAdd = int(self.channelForBottomRowNr_tbox.get("1.0", END))


        globMinDepth = self.globMinDepth
        globMaxDepth = self.globMaxDepth
        globMinNormals = self.globMinNormals
        globMaxNormals = self.globMaxNormals

        currMin = []
        currMax = []

        lossBorder = 0
        drawLossBorder = False

        [a.remove() for a in self.allAxs]
        self.allAxs = []
        # [a.remove() for a in self.allcBars]

        # CARE: GT and Prediction show the same channels atm!!
        if self.dropDown1_var.get() != 'none':
            idx = self.get_om_index2(self.dropDown1_var, self.inpKeysLst)
            imgTRStart = self.inpKeys[idx][4]
            imgTRRange = self.inpKeys[idx][1]
        else:
            imgTRStart = self.upperRowChnlStart
            imgTRRange = self.upperRowChnlAdd

        if self.dropDown2_var.get() != 'none':
            idx = self.get_om_index2(self.dropDown2_var, self.tarKeysLst)
            imgTMStart = self.tarKeys[idx][4]
            imgTMRange = self.tarKeys[idx][1]
        else:
            imgTMStart = self.upperRowChnlStart
            imgTMRange = self.upperRowChnlAdd

        if self.dropDown2_var.get() != 'none':
            idx = self.get_om_index2(self.dropDown2_var, self.tarKeysLst)
            imgTLStart = self.tarKeys[idx][4]
            imgTLRange = self.tarKeys[idx][1]
        else:
            imgTLStart = self.upperRowChnlStart
            imgTLRange = self.upperRowChnlAdd

        if self.dropDown4_var.get() != 'none':
            idx = self.get_om_index2(self.dropDown4_var, self.inpKeysLst)
            imgBRStart = self.inpKeys[idx][4]
            imgBRRange = self.inpKeys[idx][1]
        else:
            imgBRStart = self.lowerRowChnlStart
            imgBRRange = self.lowerRowChnlAdd

        if self.dropDown5_var.get() != 'none':
            idx = self.get_om_index2(self.dropDown5_var, self.tarKeysLst)
            imgBMStart = self.tarKeys[idx][4]
            imgBMRange = self.tarKeys[idx][1]
        else:
            imgBMStart = self.lowerRowChnlStart
            imgBMRange = self.lowerRowChnlAdd

        if self.dropDown5_var.get() != 'none':
            idx = self.get_om_index2(self.dropDown5_var, self.tarKeysLst)
            imgBLStart = self.tarKeys[idx][4]
            imgBLRange = self.tarKeys[idx][1]
        else:
            imgBLStart = self.lowerRowChnlStart
            imgBLRange = self.lowerRowChnlAdd



        if True:#(self, data, nrChannel, plotObj, lossBorder, drawLossBorder):

            self.renderOneOr3ChannelArray(data = inputs_denormalized_n[0:1,imgTRStart : imgTRStart + imgTRRange ],
                                          nrChannel= imgTRRange,
                                          plotObj = self.plota,
                                          lossBorder = lossBorder,
                                          drawLossBorder = drawLossBorder,
                                          min = globMinNormals,
                                          max = globMaxNormals)
            self.renderOneOr3ChannelArray(data = targets_denormalized[0:1,imgTMStart : imgTMStart + imgTMRange],
                                          nrChannel= imgTMRange,
                                          plotObj = self.plotc,
                                          lossBorder = lossBorder,
                                          drawLossBorder = drawLossBorder,
                                          min = globMinNormals,
                                          max = globMaxNormals)
            self.renderOneOr3ChannelArray(data = outputs_denormalized[0:1,imgTLStart : imgTLStart + imgTLRange],
                                          nrChannel= imgTLRange,
                                          plotObj = self.plote,
                                          lossBorder = lossBorder,
                                          drawLossBorder = drawLossBorder,
                                          min = globMinNormals,
                                          max = globMaxNormals)

            if inputs_denormalized_n.shape[1]>= self.lowerRowChnlStart + self.lowerRowChnlAdd:
                self.renderOneOr3ChannelArray(data = inputs_denormalized_n[0:1,imgBRStart : imgBRStart + imgBRRange ],
                                          nrChannel= imgBRRange,
                                          plotObj = self.plotb,
                                          lossBorder = lossBorder,
                                          drawLossBorder = drawLossBorder,
                                          min = globMinDepth,
                                          max = globMaxDepth)
            if targets_denormalized.shape[1] >= self.lowerRowChnlStart + self.lowerRowChnlAdd:
                self.renderOneOr3ChannelArray(data = targets_denormalized[0:1,imgBMStart : imgBMStart + imgBMRange],
                                          nrChannel= imgBMRange,
                                          plotObj = self.plotd,
                                          lossBorder = lossBorder,
                                          drawLossBorder = drawLossBorder,
                                          min = globMinDepth,
                                          max = globMaxDepth)
            if outputs_denormalized.shape[1] >= self.lowerRowChnlStart + self.lowerRowChnlAdd:
                self.renderOneOr3ChannelArray(data = outputs_denormalized[0:1,imgBLStart : imgBLStart + imgBLRange],
                                          nrChannel= imgBLRange,
                                          plotObj = self.plotf,
                                          lossBorder = lossBorder,
                                          drawLossBorder = drawLossBorder,
                                          min = globMinDepth,
                                          max = globMaxDepth)


        if False:
            currChannel = 0
            if self.data.useNormals:
                # currImg = self.plota.imshow(np.moveaxis(utils.normalsToRGB(inputs_denormalized_n[:, currChannel:currChannel + 3, :, :], lossBorder, drawLossBorder, globMaxNormals), 0, -1))
                imgToPlot = utils.insertBoundaryInImg(inputs_denormalized_n[:, currChannel + self.topRowSkip:currChannel + self.topRowSkip + 3, :, :], self.globMinNormals, lossBorder, drawLossBorder)
                imgToPlot = (np.moveaxis(np.squeeze(imgToPlot), 0, -1) - self.globMinNormals) / (self.globMaxNormals - self.globMinNormals)
                currImg = self.plota.imshow(imgToPlot)
                # self.f.colorbar(currImg)
                if self.drawColorbars:
                    divider = make_axes_locatable(self.plota)
                    cax = divider.append_axes('right', size='5%', pad=0.05)
                    self.f.colorbar(currImg, cax=cax, orientation='vertical')

                currChannel += 3

            if self.data.useDepth:
                currAdd = self.lowerRowChnlAdd * ((currChannel + self.lowerRowChnlAdd) < inputs_denormalized_n.shape[1])
                if self.globMinDepth == []:
                    currMin = np.min(inputs_denormalized_n[0, currChannel + currAdd])
                else:
                    currMin = self.globMinDepth
                if self.globMaxDepth == []:
                    currMax = np.max(inputs_denormalized_n[0, currChannel + currAdd])
                else:
                    currMax = self.globMaxDepth

                # imgInpDepths = np.moveaxis(utils.colorize(inputs_denormalized_n[0, currChannel], vmin=currMin, vmax=currMax), 2, 0)
                imgCurrDepths = inputs_denormalized_n[0, currChannel + currAdd]
                currImg = self.plotb.imshow(utils.insertBoundaryInImg(imgCurrDepths, currMin, lossBorder, drawLossBorder), vmin=currMin, vmax=currMax)

                # currImg = self.plotb.imshow(np.moveaxis(utils.insertBoundaryInImg(imgInpDepths, currMin, lossBorder, drawLossBorder), 0, -1), vmin=20, vmax = 31)
                # currImg.set_clim(4,5)
                if self.drawColorbars:
                    self.addColorbar(self.plotb, currImg, currMin, currMax)
                currChannel += 1

            currChannel = 0
            if self.data.useTargetNormals:
                imgToPlot = utils.insertBoundaryInImg(targets_denormalized[:, currChannel:currChannel + 3, :, :], self.globMinNormals, lossBorder, drawLossBorder)
                imgToPlot = (np.moveaxis(np.squeeze(imgToPlot), 0, -1) - self.globMinNormals) / (self.globMaxNormals - self.globMinNormals)
                currImg = self.plotc.imshow(imgToPlot)
                # currImg = self.plotc.imshow(np.moveaxis(utils.normalsToRGB(targets_denormalized[:, currChannel:currChannel + 3, :, :], lossBorder, drawLossBorder, globMax), 0, -1), vmin=0, vmax = 1)
                if self.drawColorbars:
                    divider = make_axes_locatable(self.plotc)
                    cax = divider.append_axes('right', size='5%', pad=0.05)
                    self.f.colorbar(currImg, cax=cax, orientation='vertical')
                currChannel += 3

            if self.data.useTargetDepth:
                self.plotd.cla()
                currAdd = self.lowerRowChnlAdd * ((currChannel + self.lowerRowChnlAdd) < targets_denormalized.shape[1])
                if self.globMinDepth == []:
                    currMin = np.min(targets_denormalized[0, currChannel + currAdd])
                else:
                    currMin = self.globMinDepth
                if self.globMaxDepth == []:
                    currMax = np.max(targets_denormalized[0, currChannel + currAdd])
                else:
                    currMax = self.globMaxDepth

                imgCurrDepths = targets_denormalized[0, currChannel + currAdd]
                currImg = self.plotd.imshow(utils.insertBoundaryInImg(imgCurrDepths, currMin, lossBorder, drawLossBorder), vmin=currMin, vmax=currMax)

                # imgTarDepths = np.moveaxis(utils.colorize(targets_denormalized[0, currChannel], vmin=currMin, vmax=currMax), 2, 0)
                # currImg = self.plotd.imshow(np.moveaxis(utils.insertBoundaryInImg(imgTarDepths, currMin, lossBorder, drawLossBorder), 0, -1))
                if self.drawColorbars:
                    self.addColorbar(self.plotd, currImg, currMin, currMax)
                currChannel += 1
            currChannel = 0
            if self.data.useTargetNormals:
                reconstructNormals = False
                if reconstructNormals:
                    self.reconstructedNormals = self.reconstructNormalFromDepth(outputs_denormalized[:, currChannel + 3:currChannel + 4, :, :])
                    outputs_denormalized[:, currChannel:currChannel + 3, :, :] = self.reconstructedNormals

                imgToPlot = utils.insertBoundaryInImg(outputs_denormalized[:, currChannel:currChannel + 3, :, :], self.globMinNormals, lossBorder, drawLossBorder)
                imgToPlot = (np.moveaxis(np.squeeze(imgToPlot), 0, -1) - self.globMinNormals) / (self.globMaxNormals - self.globMinNormals)
                currImg = self.plote.imshow(imgToPlot)
                # currImg = self.plote.imshow(np.moveaxis(utils.normalsToRGB(outputs_denormalized[:, currChannel:currChannel + 3, :, :], lossBorder, drawLossBorder, globMax), 0, -1))
                if self.drawColorbars:
                    divider = make_axes_locatable(self.plote)
                    cax = divider.append_axes('right', size='5%', pad=0.05)
                    self.f.colorbar(currImg, cax=cax, orientation='vertical')
                currChannel += 3
            if self.data.useTargetDepth:
                currAdd = self.lowerRowChnlAdd * ((currChannel + self.lowerRowChnlAdd) < outputs_denormalized.shape[1])
                if self.globMinDepth == []:
                    currMin = np.min(outputs_denormalized[0, currChannel + currAdd])
                else:
                    currMin = self.globMinDepth
                if self.globMaxDepth == []:
                    currMax = np.max(outputs_denormalized[0, currChannel + currAdd])
                else:
                    currMax = self.globMaxDepth

                imgCurrDepths = outputs_denormalized[0, currChannel + currAdd]
                currImg = self.plotf.imshow(utils.insertBoundaryInImg(imgCurrDepths, currMin, lossBorder, drawLossBorder), vmin=currMin, vmax=currMax)

                # imgOutDepths = np.moveaxis(utils.colorize(outputs_denormalized[0, currChannel], vmin=currMin, vmax=currMax), 2, 0)
                # currImg = self.plotf.imshow(np.moveaxis(utils.insertBoundaryInImg(imgOutDepths, currMin, lossBorder, drawLossBorder), 0, -1))
                if self.drawColorbars:
                    self.addColorbar(self.plotf, currImg, currMin, currMax)

                currChannel += 1

        self.canvas.draw()

    def renderOneOr3ChannelArray(self, data, nrChannel, plotObj, lossBorder, drawLossBorder, min = 0, max = 1):
        # This function contains the code needed to render 1 or 3 channels of data depending on the parameters passed.

        # Min max only used when rendering one channel atm.

        if nrChannel == 3:
            imgToPlot = utils.insertBoundaryInImg(data, self.globMinNormals, lossBorder, drawLossBorder)
            imgToPlot = (np.moveaxis(np.squeeze(imgToPlot), 0, -1) - self.globMinNormals) / (self.globMaxNormals - self.globMinNormals)
            imgToPlot[0,0,:] = 0
            imgToPlot[0,1,:] = 1
            imgToPlot = np.clip(imgToPlot,0,1)

            currImg = plotObj.imshow(imgToPlot)
            # self.f.colorbar(currImg)
            if self.drawColorbars:
                divider = make_axes_locatable(plotObj)
                cax = divider.append_axes('right', size='5%', pad=0.05)
                self.f.colorbar(currImg, cax=cax, orientation='vertical')

        if nrChannel == 1:
            # if self.globMinDepth == []:
            #     currMin = np.min(data)
            # else:
            #     currMin = self.globMinDepth
            # if self.globMaxDepth == []:
            #     currMax = np.max(data)
            # else:
            #     currMax = self.globMaxDepth
            data = np.squeeze(data)

            # currImg = plotObj.imshow(utils.insertBoundaryInImg(data, currMin, lossBorder, drawLossBorder), vmin=currMin, vmax=currMax)
            currImg = plotObj.imshow(utils.insertBoundaryInImg(data, min, lossBorder, drawLossBorder), vmin=min, vmax=max)

            # currImg = self.plotb.imshow(np.moveaxis(utils.insertBoundaryInImg(imgInpDepths, currMin, lossBorder, drawLossBorder), 0, -1), vmin=20, vmax = 31)
            # currImg.set_clim(4,5)
            if self.drawColorbars:
                self.addColorbar(self.plotb, currImg, min, max)






    def __init__(self, master=None):
        #self.device = torch.device('cpu')

        self.master=master
        self.device = torch.cuda.current_device()

        self.tic = TicToc()
        self.allAxs = []
        self.allcBars = []

        self.drawColorbars = False
        self.lossBorder = 16

        px = 50
        py = 10
        #self.model_dir = "/home/kumpf/net/gpusrv01st/resultsFromTraining2/frame/run00281/"
        self.model_dir = "/home/gupta/masterArbeit/physics_unet/results/dunet_conv_partialconv/"
        self.model_dir_label = Label(master=master, bg='#FFCFC9', text=self.model_dir)
        self.button_models = Button(text="Browse models", command=self.browse_button_model)
        self.button_models.place(x=px, y=py, width=200, height=40)
        py += 40
        self.model_dir_label.place(x=px, y=py, width=800, height=40)
        py += 40



        self.availableModels = ['none selected']
        self.chosenModel_var = StringVar(master)
        #self.chosenModel_dropdown = OptionMenu(master, self.chosenModel_var, *self.availableModels)

        self.chosenModel_dropdown = ttk.Combobox(master, textvariable=self.chosenModel_var, values=self.availableModels)
        # link function to change dropdown
        self.chosenModel_var.trace('w', self.change_selectedModel)
        self.chosenModel_dropdown.place(x=px, y=py, width=200, height=40)
        py += 40

        self.collectAvailableModels(self.model_dir)

#        self.data_dir = "/home/kumpf/net/Lehrstuhl/Share/Mathias/Neuschwanstein_small_10x200_pixels/"
        #self.data_dir = "/home/kumpf/net/KumpfBackup/AusgelagerteDaten/2019/PCR_Rendering_und_Daten/PCR/lucy_close_0.02_screenSNormals/train/"
        self.data_dir = "/home/gupta/masterArbeit/Datasets/data_7Nov/Train/"
        self.data_dir_label = Label(master=master, bg='#FFCFC9', text=self.data_dir)
        self.button_data1 = Button(text="Browse data", command=self.browse_button_data)
        self.button_data1.place(x=px, y=py, width=200, height=40)
        py += 40
        self.data_dir_label.place(x=px, y=py, width=800, height=40)
        py += 50

        self.dataScale = Scale(master, from_=0, to=100,  orient=HORIZONTAL, command=self.dataScaleMoved)
        self.dataScale.place(x=px, y=py, width=800, height=40)
        py += 60

        self.curDataSample = Entry(master)
        #self.curDataSample.bind(func=self.dataSampleSpecified)
        self.curDataSample.bind("<Return>", self.dataSampleSpecified)
        self.curDataSample.place(x=px, y=py, width=80, height=40)
        py += 40

        # Selection for dataset
        self.bkeysToLoadPickle=False
        self.setDataVars()


        self.f = Figure(figsize=(10, 10), dpi=100)

        self.plota = self.f.add_subplot(231, aspect=1)
        #self.plota.plot([1, 2, 3, 4, 5, 6, 7, 8], [5, 6, 1, 3, 8, 9, 3, 5])

        self.plotb = self.f.add_subplot(234, aspect=1)
        #self.plotb.plot([1, 2, 3, 4, 5, 6, 7, 8], [5, 6, 1, 3, 8, 9, 3, 5])
        #colorbar()

        self.plotc = self.f.add_subplot(232, aspect=1)
        #self.plotc.plot([1, 2, 3, 4, 5, 6, 7, 8], [5, 6, 1, 3, 8, 9, 3, 5])

        self.plotd = self.f.add_subplot(235, aspect=1)
        #self.plotd.plot([1, 2, 3, 4, 5, 6, 7, 8], [5, 6, 1, 3, 8, 9, 3, 5])
        self.plote = self.f.add_subplot(233, aspect=1)
        #self.plote.plot([1, 2, 3, 4, 5, 6, 7, 8], [5, 6, 1, 3, 8, 9, 3, 5])
        self.plotf = self.f.add_subplot(236, aspect=1)
        #self.plotf.plot([1, 2, 3, 4, 5, 6, 7, 8], [5, 6, 1, 3, 8, 9, 3, 5])

        self.canvas = FigureCanvasTkAgg(self.f, master=master)
        self.canvas.draw()
        self.canvas.get_tk_widget().place(x=0, y=0, width=10, height=10)

        #toolbar = NavigationToolbar2TkAgg(self.canvas, self)
        #toolbar.update()
        self.canvas._tkcanvas.place(x=0, y=330, width=1900, height=700)


        ### Parameters ####
        px1 = 900
        px2 = 1100
        px3 = 1300
        px4 = 1500
        px5 = 1700
        px6 = 2000
        px7 = 2400
        px8 = 2700
        px9 = 3000

        py = 10
        self.dataResX_tbox_label = Label(master=master, text='data x res')
        self.dataResX_tbox_label.place(x=px1, y=py, width=120, height=40)

        self.dataResX_tbox = Text(master)
        self.dataResX_tbox.insert(INSERT, str(self.dataResolutionX))
        self.dataResX_tbox.place(x=px2, y=py, width=70, height=40)
        py += 40

        self.dataResY_tbox_label = Label(master=master, text='data y res')
        self.dataResY_tbox_label.place(x=px1, y=py, width=120, height=40)

        self.dataResY_tbox = Text(master)
        self.dataResY_tbox.insert(INSERT, str(self.dataResolutionY))
        self.dataResY_tbox.place(x=px2, y=py, width=70, height=40)
        py += 40




        self.gridSizex_tbox_label = Label(master=master, text='grid size x')
        self.gridSizex_tbox_label.place(x=px1, y=py, width=120, height=40)

        self.gridSizex_tbox = Text(master)
        self.gridSizex_tbox.insert(INSERT, str(self.gridSizex))
        self.gridSizex_tbox.place(x=px2, y=py, width=70, height=40)
        py += 40

        self.gridSizey_tbox_label = Label(master=master, text='grid size y')
        self.gridSizey_tbox_label.place(x=px1, y=py, width=120, height=40)

        self.gridSizey_tbox = Text(master)
        self.gridSizey_tbox.insert(INSERT, str(self.gridSizey))
        self.gridSizey_tbox.place(x=px2, y=py, width=70, height=40)

        self.globMinDepth = 0
        self.globMaxDepth = 0.3
        self.globMinNormals = 0
        self.globMaxNormals = 1

        py = 10
        self.upperClimMin_tbox_label = Label(master=master, text='upper clim min')
        self.upperClimMin_tbox_label.place(x=px3, y=py, width=160, height=40)

        self.upperClimMin_tbox = TextM(master)
        self.upperClimMin_tbox.setFn(self.renderImages)
        self.upperClimMin_tbox.insert(INSERT, str(self.globMinNormals))
        self.upperClimMin_tbox.place(x=px4, y=py, width=70, height=40)
        py += 40


        self.upperClimMax_tbox_label = Label(master=master, text='upper clim max')
        self.upperClimMax_tbox_label.place(x=px3, y=py, width=160, height=40)

        self.upperClimMax_tbox = TextM(master)
        self.upperClimMax_tbox.setFn(self.renderImages)
        self.upperClimMax_tbox.insert(INSERT, str(self.globMaxNormals))
        self.upperClimMax_tbox.place(x=px4, y=py, width=70, height=40)
        py += 40


        self.lowerClimMin_tbox_label = Label(master=master, text='lower clim min')
        self.lowerClimMin_tbox_label.place(x=px3, y=py, width=160, height=40)

        self.lowerClimMin_tbox = TextM(master)
        self.lowerClimMin_tbox.setFn(self.renderImages)
        self.lowerClimMin_tbox.insert(INSERT, str(self.globMinDepth))
        self.lowerClimMin_tbox.place(x=px4, y=py, width=70, height=40)
        py += 40


        self.lowerClimMax_tbox_label = Label(master=master, text='lower clim max')
        self.lowerClimMax_tbox_label.place(x=px3, y=py, width=160, height=40)

        self.lowerClimMax_tbox = TextM(master)
        self.lowerClimMax_tbox.setFn(self.renderImages)
        self.lowerClimMax_tbox.insert(INSERT, str(self.globMaxDepth))
        self.lowerClimMax_tbox.place(x=px4, y=py, width=70, height=40)
        py += 40

        py = 20
        self.channelForTopRowStart_tbox_label = Label(master=master, text='Top row channel start')
        self.channelForTopRowStart_tbox_label.place(x=px5, y=py, width=320, height=40)

        self.channelForTopRowStart_tbox = TextM(master)
        self.channelForTopRowStart_tbox.setFn(self.renderImages)
        self.channelForTopRowStart_tbox.insert(INSERT, '0')
        self.channelForTopRowStart_tbox.place(x=px6, y=py, width=70, height=40)

        py += 40
        self.channelForTopRowStart_tbox_label = Label(master=master, text='Top row channel range')
        self.channelForTopRowStart_tbox_label.place(x=px5, y=py, width=320, height=40)

        self.channelForTopRowNr_tbox = TextM(master)
        self.channelForTopRowNr_tbox.setFn(self.renderImages)
        self.channelForTopRowNr_tbox.insert(INSERT, '1')
        self.channelForTopRowNr_tbox.place(x=px6, y=py, width=70, height=40)

        py += 40


        self.channelForBottomRowStart_tbox_label = Label(master=master, text='bottom row channel start')
        self.channelForBottomRowStart_tbox_label.place(x=px5, y=py, width=320, height=40)

        self.channelForBottomRowStart_tbox = TextM(master)
        self.channelForBottomRowStart_tbox.setFn(self.renderImages)
        self.channelForBottomRowStart_tbox.insert(INSERT, '1')
        self.channelForBottomRowStart_tbox.place(x=px6, y=py, width=70, height=40)

        py += 40
        self.channelForBottomRowNr_tbox_label = Label(master=master, text='bottom row channel range')
        self.channelForBottomRowNr_tbox_label.place(x=px5, y=py, width=320, height=40)

        self.channelForBottomRowNr_tbox = TextM(master)
        self.channelForBottomRowNr_tbox.setFn(self.renderImages)
        self.channelForBottomRowNr_tbox.insert(INSERT, '1')
        self.channelForBottomRowNr_tbox.place(x=px6, y=py, width=70, height=40)
        py +=40
        self.depthComparisonB = IntVar()
        self.depthComparisonB.set(1)

        # (only applied to input for rendering)
        self.depthComparison_checkbutton = Checkbutton(master, text="depth comparison", variable = self.depthComparisonB, command=self.dataScaleMoved)
        self.depthComparison_checkbutton.place(x=px5, y=py, width=270, height=40)

        py += 40
        self.depthGTInput = IntVar()
        self.depthGTInput.set(0)
        self.depthGTInput_checkbutton = Checkbutton(master, text="depth GT to input", variable = self.depthGTInput, command=self.depthGTInputChanged)
        self.depthGTInput_checkbutton.place(x=px5, y=py, width=270, height=40)

        py += 40
        self.shadedInput = IntVar()
        self.shadedInput.set(1)
        self.shadedInput_checkbutton = Checkbutton(master, text="shaded input", variable = self.shadedInput, command=self.shadedInputChanged)
        self.shadedInput_checkbutton.place(x=px5, y=py, width=270, height=40)

        py += 40
        #self.renderTopRowFromChVar = IntVar()
        #self.renderTopRowFromChVar.set(0)
        self.renderTopRowFromChVar_tbox = TextM(master)
        self.renderTopRowFromChVar_tbox.setFn(self.renderTopRowFromChVarChanged)
        self.renderTopRowFromChVar_tbox.place(x=px5, y=py, width=270, height=40)



        ## Drop downs for rendering. They will overwrite the channel stuff...
        py = 10
        self.dropDown1_var = StringVar(master)
        self.dropDown1_var.set("none")
        self.dropDown1 = OptionMenu(master, self.dropDown1_var, "none")
        self.dropDown1.place(x=px7, y=py, width=270, height=40)


        self.dropDown2_var = StringVar(master)
        self.dropDown2_var.set("none")
        self.dropDown2 = OptionMenu(master, self.dropDown2_var, "none")
        self.dropDown2.place(x=px8, y=py, width=270, height=40)


        self.dropDown3_var = StringVar(master)
        self.dropDown3_var.set("none")
        self.dropDown3 = OptionMenu(master, self.dropDown3_var, "unused")
        self.dropDown3.place(x=px9, y=py, width=270, height=40)
        py += 40

        self.dropDown4_var = StringVar(master)
        self.dropDown4_var.set("none")
        self.dropDown4 = OptionMenu(master, self.dropDown4_var, "none")
        self.dropDown4.place(x=px7, y=py, width=270, height=40)


        self.dropDown5_var = StringVar(master)
        self.dropDown5_var.set("none")
        self.dropDown5 = OptionMenu(master, self.dropDown5_var, "none")
        self.dropDown5.place(x=px8, y=py, width=270, height=40)


        self.dropDown6_var = StringVar(master)
        self.dropDown6_var.set("none")
        self.dropDown6 = OptionMenu(master, self.dropDown6_var, "unused")
        self.dropDown6.place(x=px9, y=py, width=270, height=40)
        py += 40







        self.inpKeys = []
        self.tarKeys = []

        self.inpKeysLst = []
        self.tarKeysLst = []

        # self.depthComparison_checkbutton.c

        ######################  Code for the model etc. #################################

        self.model = []
        self.dataScaleMoved()

# Fenster
tkFenster = Tk()
tkFenster.title('Test Trained Models')
tkFenster.geometry('1900x1900')
default_font = font.nametofont("TkDefaultFont")
default_font.configure(size=12)

app = Application(master=tkFenster)
tkFenster.mainloop()
#app.mainloop()
tkFenster.destroy()

