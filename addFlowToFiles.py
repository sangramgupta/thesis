# Alexander Kumpf Feb 2019

print('importing numpy')
import numpy as np

import sys
sys.path.append('/home/kumpf/projects/Neural_networks/PWC-Net/PyTorch/')

print('importing PWC-FlowNet')
import script_pwc_returnArray as PWC

print('all imports done')

# Run this function with Python2 (Anaconda with pytorch 0.2)!! Some dependencies for PWC only exist for python2 / pytorch 0.2
def addOptFlowToFiles():
    # The first argument contains the .txt file with all paths to files to add the optical flow to
    if len(sys.argv) > 1:
        filesToProcessPath = sys.argv[1]
    else:
        filesToProcessPath = '/home/kumpf/net/KumpfBackup/AusgelagerteDaten/2019/Highres_250m_Daten_Christian_Barthlott/z/Test/npz/filesToWarp.txt'

        #return

    if filesToProcessPath != '':
        with open(filesToProcessPath, 'r') as f:
            for line in f:
                line = line.rstrip()
                print(line)
                # Open .npz file
                npFile = np.load(line)


                # Read img1 and img2
                img1_oneChannel = npFile['a'][0, :, :]
                img2_oneChannel = npFile['a'][1, :, :]

                # Scale values to 0-1

                # Scale the scalar fields to 0-1 range
                minImg1 = np.min(img1_oneChannel)
                maxImg1 = np.max(img1_oneChannel)
                minImg2 = np.min(img2_oneChannel)
                maxImg2 = np.max(img2_oneChannel)

                img1_oneChannel = (img1_oneChannel - minImg1) / (maxImg1 - minImg1)
                img2_oneChannel = (img2_oneChannel - minImg2) / (maxImg2 - minImg2)

                img1 = np.zeros((img1_oneChannel.shape[0] , img1_oneChannel.shape[1], 3))
                img2 = np.zeros((img2_oneChannel.shape[0] , img2_oneChannel.shape[1], 3))

                # Create RGB Image from scalar fields
                img1[:, :, 0] = img1_oneChannel
                img1[:, :, 1] = img1_oneChannel
                img1[:, :, 2] = img1_oneChannel

                img2[:, :, 0] = img2_oneChannel
                img2[:, :, 1] = img2_oneChannel
                img2[:, :, 2] = img2_oneChannel


                images = [img1, img2]
                # Compute the flow in both directions.
                flow = np.zeros((2,) + img1_oneChannel.shape + (2,))
                flow[0,:,:,:] = PWC.PWC_Flow(img1, img2)
                flow[1,:,:,:] = PWC.PWC_Flow(img2, img1)



                # Add optical flow



                # Overwrite file
                np.savez_compressed(line, a=npFile['a'], flow=flow)



addOptFlowToFiles()