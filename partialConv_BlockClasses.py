import torch
import torch.nn as nn
from kernels.partialconv2d import PartialConv2d as PartialConv2d

class firstBlockUNet(nn.Module):
    def __init__(self, in_c, out_c, name, size=4, stride=1, pad = -1, bias = True, relu=True, bn=True, useDeformConv=False):
        super(firstBlockUNet, self).__init__()
        if pad == -1:
            pad = size - 1 - 2 * (stride - 1)

        self.layer00_pc = PartialConv2d(in_c, out_c, kernel_size=size, stride=1, padding=(size - 1) // 2, bias=True, return_mask=True)

        if relu:
            self.layers_activation = nn.ReLU(inplace=True)
        else:
            self.layers_activation = nn.LeakyReLU(0.2, inplace=True)

        self.layer01_pc = PartialConv2d(out_c, out_c, kernel_size=size, stride=1, padding=(size - 1) // 2, bias=True, return_mask=True)

        self.batchNormalisation = bn
        if bn:
            self.layer01_pc_bn = nn.BatchNorm2d(out_c)

    def forward(self, x, maskPC):
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
        # maskPC = torch.empty((x.shape[0],1,x.shape[2],x.shape[3]))
        # for i in range(0,x.shape[0]):
        #      maskPC[i,0] = (x[i, -1] !=1)

        res, mask = self.layer00_pc(x,maskPC)
        res = self.layers_activation(res)
        res, mask = self.layer01_pc(res,mask)
        if self.batchNormalisation:
            self.layer01_pc_bn(res)
        res = self.layers_activation(res)

        return res,mask



class blockUNetAdaptAndAutoPad(nn.Module):
    def __init__(self, in_c, out_c, name, transposed=False, bn=True, relu=True, size=4, pad=-1, dropout=0., dilation=1, resX = 256, maxPool = False,useDeformConv=False, twoConvsPerLev = True):
        super(blockUNetAdaptAndAutoPad, self).__init__()
        if pad != -1:
            padConvDownscale = pad
        else:
            if not transposed:
                padConvDownscale = int((size - 1) / 2)
            else:
                padConvDownscale = int((size - 1) / 2)

        self.layers_maxpooling = nn.MaxPool2d(kernel_size=2)
        self.layers_upsample = nn.Upsample(scale_factor=2)
        self.dropout = dropout
        if dropout > 0.:
            self.layers_dropout = nn.Dropout2d(dropout, inplace=True)

        self.transposed = transposed
        self.batchNormalisation = bn
        if bn:
            self.layers_bn = nn.BatchNorm2d(out_c)

        if relu:
            self.layers_activation = nn.ReLU(inplace=True)
        else:
            self.layers_activation = nn.LeakyReLU(0.2, inplace=True)

        self.maxPool = maxPool
        if not transposed:
            if not maxPool:
                self.layer10_pc = PartialConv2d(in_c, out_c, kernel_size=size, stride=2, padding=(size - 1) // 2, bias=True, return_mask= True)
            else:
                self.layer10_pc = PartialConv2d(in_c, out_c, kernel_size=size, stride=1, padding=(size - 1) // 2, bias=True, return_mask= True)
        else:
            self.layer10_pc = PartialConv2d(in_c, out_c, kernel_size=size, stride=1, padding=(size - 1) // 2, bias=True, return_mask=True)

        self.twoConvsPerLev = twoConvsPerLev
        if twoConvsPerLev:
            self.layer11_pc = PartialConv2d(out_c, out_c, kernel_size=size, stride=1, padding=(size - 1) // 2, bias=True, return_mask= True)

    def forward(self, out, mask):
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
        if not self.transposed:
            if not self.maxPool:
                res1, mask1 = self.layer10_pc(out, mask)
                if self.twoConvsPerLev:
                    res1 = self.layers_activation(res1)
                    res1, mask1 = self.layer11_pc(res1, mask1)
            else:
                out = self.layers_maxpooling(out)
                mask = self.layers_maxpooling(mask)
                res1, mask1 = self.layer10_pc(out, mask)
                if self.twoConvsPerLev:
                    res1 = self.layers_activation(res1)
                    res1, mask1 = self.layer11_pc(res1, mask1)
        else:
            mask = self.layers_upsample(mask)
            out = self.layers_upsample(out)
            res1, mask1 = self.layer10_pc(out, mask)
            if self.twoConvsPerLev:
                res1 = self.layers_activation(res1)
                res1, mask1 = self.layer11_pc(res1, mask1)

        if self.dropout:
            res1 = self.layers_dropout(res1)
        if self.batchNormalisation:
            res1 = self.layers_bn(res1)
        res1 = self.layers_activation(res1)
        return res1,mask1

