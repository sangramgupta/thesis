################
#
# Deep Flow Prediction - N. Thuerey, K. Weissenov, H. Mehrotra, N. Mainali, L. Prantl, X. Hu (TUM)
# Adapted by A. Kumpf (TUM)
# Compute errors for a test set and visualize. This script can loop over a range of models in
# order to compute an averaged evaluation.
#
################

import os,sys,random,math
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader

from dataset import TurbDataset, FrameDataset
# from DfpNet import TurbNetG, weights_init
from VecLearnNet import TurbNetGVec
from VecLearnNet import TurbNetGVecPoly
from GridNetUtils import GridNetFrame as gnf

import utils
from utils import log
from utils import str2bool
import matplotlib.pyplot as plt
import matplotlib
import argparse


## Settings ##

output_figures = True
output_as_pdf = False
output_as_png = True
nrOutputFiles = 1
nrFilesToLoad = 30

gridSizex = 256
gridSizey = 256

dataDirTest = "/home/kumpf/projects/Neural_networks/physics_unet/data/FrameData_Test_ECMWF_Jan19/"
prefix=""
modelName = 'modelG'
maxNrFiles = 10
figsDirectory = "results/"

exportAsPDF = False
exportAsPNG = True

# mode can be 'stamped vec', 'stamped scalar', 'noise poly',....
# only 'noise poly' and 'stamped vec' are implemented so far...
modeForTraining = 'frame'#''stamped vec' #'noise poly'
useHDF5Files = False
## ######## ##

# Parse arguments:
parser = argparse.ArgumentParser(description='Variables which can be specified.')
parser.add_argument('--dataDirTest', type=str, help='Directory for the test dataset. Default: ...')
parser.add_argument('--prefix', type=str, help='Prefix. Default: ...')
parser.add_argument('--modelName', type=str, help='Name of model to load. Default: modelG')
parser.add_argument('--maxNrFiles', type=int, help='Maximum number of files to be loaded: 10000')
parser.add_argument('--nrOutputFiles', type=int, help='Maximum number of files to be tested: Default: 1')

parser.add_argument('--figsDirectory', type=str, help='Name of model to save. Default: fig_train_output')
parser.add_argument('--exportAsPDF', type=str, help='Export one validation file (trained vs untrained) as .pdf. Default: False')
parser.add_argument('--exportAsPNG', type=str, help='Export one validation file (trained vs untrained) as .png. Default: False')

parser.add_argument('--modeForTraining', type=str, help='Model etc. which was used for traingin. E.g., stamped vec, noise poly, frame. Default: stamped vec')

parser.add_argument('--useHDF5Files', type=str, help='Option to read from hdf5 instead of .npz. Default: False')


args = parser.parse_args()
if args.dataDirTest:
    dataDirTest = args.dataDirTest
if args.prefix:
    if args.prefix != "None":
        prefix = args.prefix
if args.modelName:
    modelName = args.modelName
if args.maxNrFiles:
    maxNrFiles = args.maxNrFiles
if args.nrOutputFiles:
    nrOutputFiles = args.nrOutputFiles
if args.figsDirectory:
    figsDirectory = args.figsDirectory
if args.exportAsPDF:
    exportAsPDF = str2bool(args.exportAsPDF)
if args.exportAsPNG:
    exportAsPNG = str2bool(args.exportAsPNG)
if args.modeForTraining:
    modeForTraining = args.modeForTraining
if args.useHDF5Files:
    useHDF5Files = str2bool(args.useHDF5Files)
###############


suffix = "" # customize loading & output if necessary
#prefix = ""
#if len(sys.argv)>1:
#    prefix = sys.argv[1]
#    print("Output prefix: {}".format(prefix))

expo = 5
dropout    = 0.




dataset = []
testLoader = []
if modeForTraining == 'noise poly':
    if dataDirTest == "":
        dataDirTest = "/data/Polynoms/testImages/"
    dataset = TurbDataset(None, mode=TurbDataset.TEST, dataDirTest=dataDirTest, maxFiles = nrFilesToLoad, maxNrFiles=maxNrFiles)
    testLoader = DataLoader(dataset, batch_size=1, shuffle=False)
    targets = torch.FloatTensor(1, 1, gridSizex, gridSizey)
    targets = Variable(targets)
    targets = targets.cuda()
    inputs = torch.FloatTensor(1, 1, gridSizex, gridSizey)
    inputs = Variable(inputs)
    inputs = inputs.cuda()

    targets_dn = torch.FloatTensor(1, 1, gridSizex, gridSizey)
    targets_dn = Variable(targets_dn)
    targets_dn = targets_dn.cuda()
    outputs_dn = torch.FloatTensor(1, 1, gridSizex, gridSizey)
    outputs_dn = Variable(outputs_dn)
    outputs_dn = outputs_dn.cuda()

    netG = TurbNetGVecPoly(channelExponent=expo, dropout=dropout)
elif modeForTraining == 'frame':
    if dataDirTest == "":
        dataDirTest = "data/10mWindTest/"
    dataset = FrameDataset(None, mode=TurbDataset.TEST, dataDirTest=dataDirTest, maxFiles=maxNrFiles, readDataFromH5Py = useHDF5Files)
    testLoader = DataLoader(dataset, batch_size=1, shuffle=False)
    targets = torch.FloatTensor(1, 1, gridSizex, gridSizey)
    targets = Variable(targets)
    targets = targets.cuda()
    inputs = torch.FloatTensor(1, 4, gridSizex, gridSizey)
    inputs = Variable(inputs)
    inputs = inputs.cuda()

    targets_dn = torch.FloatTensor(1, 1, gridSizex, gridSizey)
    targets_dn = Variable(targets_dn)
    targets_dn = targets_dn.cuda()
    outputs_dn = torch.FloatTensor(1, 1, gridSizex, gridSizey)
    outputs_dn = Variable(outputs_dn)
    outputs_dn = outputs_dn.cuda()

    netG = gnf(inpChannels = 4)
else: # 'stamped vec'
    if dataDirTest == "":
        dataDirTest = "data/Masks_on_10m_wind/"
    dataset = TurbDataset(None, mode=TurbDataset.TEST, dataDirTest=dataDirTest, maxFiles=maxNrFiles)
    testLoader = DataLoader(dataset, batch_size=1, shuffle=False)
    targets = torch.FloatTensor(1, 2, gridSizex, gridSizey)
    targets = Variable(targets)
    targets = targets.cuda()
    inputs = torch.FloatTensor(1, 3, gridSizex, gridSizey)
    inputs = Variable(inputs)
    inputs = inputs.cuda()

    targets_dn = torch.FloatTensor(1, 2, gridSizex, gridSizey)
    targets_dn = Variable(targets_dn)
    targets_dn = targets_dn.cuda()
    outputs_dn = torch.FloatTensor(1, 2, gridSizex, gridSizey)
    outputs_dn = Variable(outputs_dn)
    outputs_dn = outputs_dn.cuda()

    netG = TurbNetGVec(channelExponent=expo)




lf = "./" + prefix + "testout{}.txt".format(suffix)
utils.makeDirs(["results_test"])

# loop over different trained models
avgLoss = 0.
losses = []
models = []


def mapValueToColor(minVal, maxVal, val):
    mappedVal = (val - minVal) / (maxVal - minVal)

    return np.maximum(0, np.minimum(1, mappedVal))

for si in range(25):
    s = chr(96+si)
    if(si==0):
        s = "" # check modelG, and modelG + char
    modelFn = "./" + prefix + "{}{}{}".format(modelName, suffix,s)
    if not os.path.isfile(modelFn):
        continue

    models.append(modelFn)

    log(lf, "Loading " + modelFn)
    # load checkpoint
    checkpoint = torch.load(modelFn)
    netG.load_state_dict(checkpoint['state_dict'])
    log(lf, "Loaded " + modelFn)
    netG.cuda()

    criterionL1 = nn.L1Loss()
    criterionL1.cuda()
    L1val_accum = 0.0
    L1val_dn_accum = 0.0
    lossPer_p_accum = 0
    lossPer_v_accum = 0
    lossPer_accum = 0

    netG.eval()

    for i, data in enumerate(testLoader, 0):
        inputs_cpu, targets_cpu, usedXRange, usedYRange, idxes = data
        targets_cpu, inputs_cpu = targets_cpu.float().cuda(), inputs_cpu.float().cuda()
        inputs.data.resize_as_(inputs_cpu).copy_(inputs_cpu)
        targets.data.resize_as_(targets_cpu).copy_(targets_cpu)

        inputs_cpu_np = np.copy(inputs_cpu.cpu().numpy()[0:1])

        outputs = netG(inputs)
        outputs_cpu = outputs.data.cpu().numpy()[0:1]
        targets_cpu = targets_cpu.cpu().numpy()[0:1]


        lossL1 = criterionL1(outputs, targets)
        L1val_accum += lossL1.item()

        # precentage loss by ratio of means which is same as the ratio of the sum
        #lossPer_p = np.sum(np.abs(outputs_cpu[0] - targets_cpu[0]))/np.sum(np.abs(targets_cpu[0]))
        lossPer_v = []
        lossPer = []
        # lossPer_p_accum = 0
        lossPer_v_accum = 0
        outputs_denormalized = []
        targets_denormalized = []
        inputs_denormalized = []
        if modeForTraining == 'noise poly':
            lossPer_v =  np.sum(np.abs(outputs_cpu[0] - targets_cpu[0])) /  np.sum(np.abs(targets_cpu[0]))
            lossPer = np.sum(np.abs(outputs_cpu - targets_cpu))/np.sum(np.abs(targets_cpu))
            lossPer_v_accum += lossPer_v.item()

            errorRemaining = np.sum(np.abs(outputs_cpu[0] - targets_cpu[0]))
            orgError = np.sum(np.abs(inputs_cpu_np[0] - targets_cpu[0]))
            log(lf, "Test sample %d" % i)
            log(lf, "Original error: %f" % (orgError))
            log(lf, "    scalar values:  abs. difference, ratio: %f , %f " % (errorRemaining, lossPer_v.item()))
            log(lf, "    error reduced by percentage:%f " % (errorRemaining / orgError))

            # Calculate the norm
            input_ndarray = inputs_cpu.cpu().numpy()[0]
            v_norm = np.max(np.abs(input_ndarray[0, :, :]))

            outputs_denormalized = dataset.denormalizePoly(outputs_cpu, v_norm)
            targets_denormalized = dataset.denormalizePoly(targets_cpu, v_norm)

            inputs_denormalized = dataset.denormalizePoly(inputs_cpu_np, v_norm)

        elif modeForTraining == 'frame':

            inputs_denormalized = dataset.getRawInput(idxes.data.numpy(), torch.stack(usedXRange), torch.stack(usedYRange))

            outputs_denormalized = dataset.denormalizeFrame(outputs_cpu, idxes.data.numpy(), 'tar')
            targets_denormalized = dataset.denormalizeFrame(targets_cpu, idxes.data.numpy(), 'tar')

            inputs_warped_denormalized = dataset.denormalizeFrame(inputs_cpu.cpu().numpy(), idxes.data.numpy(), 'tar')

            figsNameStart = "test_{}_".format(i)
            utils.saveFrameTestImages(figsNameStart, figsDirectory, exportAsPDF, exportAsPNG,
                                      inputs_denormalized[0:1, 0:2, :, :],
                                      outputs_denormalized[0:1, 0:1, :, :],
                                      targets_denormalized[0:1, 0:1, :, :],
                                      inputs_warped_denormalized[0:1, 0:2, :, :])
            continue



        elif modeForTraining.__contains__('vec'): # inpatining vec
            lossPer_v = ( np.sum(np.abs(outputs_cpu[0,0] - targets_cpu[0,0])) + np.sum(np.abs(outputs_cpu[0,1] - targets_cpu[0,1])) ) / ( np.sum(np.abs(targets_cpu[0,0])) + np.sum(np.abs(targets_cpu[0,1])) )
            lossPer = np.sum(np.abs(outputs_cpu - targets_cpu))/np.sum(np.abs(targets_cpu))
            #lossPer_p_accum += lossPer_p.item()
            lossPer_v_accum += lossPer_v.item()
            #lossPer_accum += lossPer.item()

            log(lf, "Test sample %d"% i )
            #log(lf, "    pressure:  abs. difference, ratio: %f , %f " % (np.sum(np.abs(outputs_cpu[0] - targets_cpu[0])), lossPer_p.item()) )
            log(lf, "    velocity:  abs. difference, ratio: %f , %f " % (np.sum(np.abs(outputs_cpu[0,0] - targets_cpu[0,0])) + np.sum(np.abs(outputs_cpu[0,1] - targets_cpu[0,1])) , lossPer_v.item() ) )
            #log(lf, "    aggregate: abs. difference, ratio: %f , %f " % (np.sum(np.abs(outputs_cpu    - targets_cpu   )), lossPer.item()) )

            # Calculate the norm
            input_ndarray = inputs_cpu.cpu().numpy()[0:1]
            v_norm = ( np.max(np.abs(input_ndarray[0,0,:,:]))**2 + np.max(np.abs(input_ndarray[0,1,:,:]))**2 )**0.5

            outputs_denormalized = dataset.denormalize(outputs_cpu, v_norm, idxes)
            targets_denormalized = dataset.denormalize(targets_cpu, v_norm, idxes)

            inputs_uv = input_ndarray[0:2]
            inputs_denormalized = dataset.denormalize(inputs_uv, v_norm, idxes)

            # inputs_denormalized = dataset.denormalize(inputs_cpu_np, v_norm)

        # denormalized error
        outputs_denormalized_comp=np.array([outputs_denormalized])
        outputs_denormalized_comp=torch.from_numpy(outputs_denormalized_comp)
        targets_denormalized_comp=np.array([targets_denormalized])
        targets_denormalized_comp=torch.from_numpy(targets_denormalized_comp)



        targets_denormalized_comp, outputs_denormalized_comp = targets_denormalized_comp.float().cuda(), outputs_denormalized_comp.float().cuda()

        outputs_dn.data.resize_as_(outputs_denormalized_comp).copy_(outputs_denormalized_comp)
        targets_dn.data.resize_as_(targets_denormalized_comp).copy_(targets_denormalized_comp)

        lossL1_dn = criterionL1(outputs_dn, targets_dn)
        L1val_dn_accum += lossL1_dn.item()


        if modeForTraining == 'noise poly':
            fig = plt.figure()
            if i == 0:

                gray = True
                if gray:
                    plt.set_cmap('gray')

                ax1 = fig.add_subplot(3, 1, 1)
                ax1.pcolormesh(inputs_denormalized[0, :, :])


                ax2 = fig.add_subplot(3, 1, 2)
                ax2.pcolormesh(outputs_denormalized[0, :, :])


                ax3 = fig.add_subplot(3, 1, 3)

                ax3.pcolormesh(targets_denormalized[0, :, :])

                if gray:
                    ax1.invert_yaxis()
                    ax2.invert_yaxis()
                    ax3.invert_yaxis()
                    ax1.set_aspect(aspect=1)
                    ax2.set_aspect(aspect=1)
                    ax3.set_aspect(aspect=1)


                plt.savefig("in_out_target.pdf", format='pdf')
                break

        elif False:
            fig = plt.figure()
            X, Y = np.meshgrid(np.arange(128), np.arange(128))
            U = outputs_denormalized[0,:,:]
            V = outputs_denormalized[1,:,:]
            L = np.sqrt(U ** 2 + V ** 2)
            C = np.array([i / 80.0 for i in L])
            Q = plt.quiver(X, Y, U, V, C, pivot='mid', units='xy', headwidth=3, headlength=5)
            #Q = plt.quiver(X, Y, U, V, C, edgecolor='k', facecolor='None', linewidth=0.1, headwidth=4, headlength=6, pivot='mid', units='inches')
            #plt.show()
            plt.savefig("output.pdf", format='pdf')

            plt.figure()
            X, Y = np.meshgrid(np.arange(128), np.arange(128))
            U = targets_denormalized[0, :, :]
            V = targets_denormalized[1, :, :]
            L = np.sqrt(U ** 2 + V ** 2)
            C = np.array([i / 80.0 for i in L])
            #W = np.array([0.005 * l / 80.0 for l in L])
            Q = plt.quiver(X, Y, U, V, C, pivot='mid', units='xy', headwidth=3, headlength=5)
            plt.savefig("target.pdf", format='pdf')

            plt.figure()
            X, Y = np.meshgrid(np.arange(128), np.arange(128))
            U = inputs_denormalized[0, :, :]
            V = inputs_denormalized[1, :, :]
            L = np.sqrt(U ** 2 + V ** 2)
            C = np.array([i / 80.0 for i in L])
            #W = np.array([0.005 * l / 80.0 for l in L])
            Q = plt.quiver(X, Y, U, V, C, pivot='mid', units='xy', headwidth=3, headlength=5)
            plt.savefig("input.pdf", format='pdf')

            # write output image, note - this is currently overwritten for multiple models
            #os.chdir("./results_test/")
            #utils.imageOut("%04d"%(i), outputs_cpu, targets_cpu, normalize=False, saveMontage=True) # write normalized with error
            #os.chdir("../")
        elif modeForTraining == 'stamped vec':
            if i <= nrOutputFiles:
            # inputs_denormalized[0,0,0,0:10]
                fileNamePrefix = str(i) + "_"
                utils.saveVectorfieldAsImage(inputs_denormalized[0], outputs_denormalized[0], targets_denormalized[0],
                                         figsDirectory,
                                         gridSizex, gridSizey, exportAsPDF, exportAsPNG, fileNamePrefix)
            else:
                break

        else:
            M = input_ndarray[0,2, :, :]
            maskIndices = np.where(M > 0.4)
            minX = np.min(maskIndices[0])
            maxX = np.max(maskIndices[0])
            minY = np.min(maskIndices[1])
            maxY = np.max(maskIndices[1])
            maskIdY = [minX, minX, maxX, maxX]
            maskIdX = [minY, maxY, maxY, minY]

            UTemp = targets_denormalized[0,0, :, :]
            VTemp = targets_denormalized[0,1, :, :]

            airfoilIndices = np.where(M == 1.0 / 3)
            stampedAirfoilIndices = np.where(M == 3.0 / 3.0)

            magnTemp = np.sqrt(UTemp ** 2 + VTemp ** 2)
            minMagn = np.min(magnTemp)
            maxMagn = np.max(magnTemp)

            print("minU, maxU, minV, maxV: {},{},{},{}".format(np.min(UTemp), np.max(UTemp), np.min(VTemp),
                                                               np.max(VTemp)))

            if i < nrOutputFiles and output_figures:
                targetAirfoilFN = "results/case_{}_target_airfoil_sample.pdf".format(i)
                inputFN = "results/case_{}_input_sample.pdf".format(i)
                outputFN = "results/case_{}_output_sample.pdf".format(i)
                targetFN = "results/case_{}_target_sample.pdf".format(i)
                targetAirfoilFNpng = "results/case_{}_target_airfoil_sample.png".format(i)
                inputFNpng = "results/case_{}_input_sample.png".format(i)
                outputFNpng = "results/case_{}_output_sample.png".format(i)
                targetFNpng = "results/case_{}_target_sample.png".format(i)
                diffUFN = "results/case_{}_diff_velu.png".format(i)
                diffVFN = "results/case_{}_diff_velv.png".format(i)

                X, Y = np.meshgrid(np.arange(gridSizex), np.arange(gridSizey))
                UD = np.abs(outputs_denormalized[0,0, :, :] - targets_denormalized[0,0, :, :])
                VD = np.abs(outputs_denormalized[0,1, :, :] - targets_denormalized[0,1, :, :])

                minUD = np.min(UD)
                maxUD = np.max(UD)
                minVD = np.min(VD)
                maxVD = np.max(VD)
                C = np.array([mapValueToColor(minUD, maxUD, e) for e in UD])

                reds_cm = matplotlib.cm.get_cmap('Reds')

                # # Set the colormap and norm to correspond to the data for which
                # # the colorbar will be used.
                # norm = matplotlib.colors.Normalize(vmin=0, vmax=0.5)
                #
                # # ColorbarBase derives from ScalarMappable and puts a colorbar
                # # in a specified axes, so it has everything needed for a
                # # standalone colorbar.  There are many more kwargs, but the
                # # following gives a basic continuous colorbar with ticks
                # # and labels.
                # fig = plt.figure()
                # ax1 = fig.add_axes([0.05, 0.80, 0.9, 0.15])
                # matplotlib.colorbar.ColorbarBase(ax1, cmap=reds_cm,
                #                                 norm=norm,
                #                                 orientation='horizontal')
                # plt.savefig("Reds_colorbar.png")
                # plt.close()

                # reds_r_cm = matplotlib.colors.LinearSegmentedColormap(
                #     'Reds_r', matplotlib.cm.revcmap(reds_cm._segmentdata))

                plt.figure()
                xmin = 0
                xmax = gridSizex
                ymin = 0
                ymax = gridSizey

                plt.gca().set_xlim(xmin, xmax)
                plt.gca().set_ylim(ymin, ymax)

                plt.pcolormesh(X, Y, UD, cmap=reds_cm, vmin=0, vmax=0.5)
                plt.savefig(diffUFN)

                plt.pcolormesh(X, Y, VD, cmap=reds_cm, vmin=0, vmax=0.5)
                plt.savefig(diffVFN)
                plt.close()

                X, Y = np.meshgrid(np.arange(128), np.arange(128))
                U = inputs_denormalized[0,0, :, :]
                V = inputs_denormalized[0,1, :, :]

                L = np.sqrt(U ** 2 + V ** 2)
                C = np.array([mapValueToColor(minMagn, maxMagn, i) for i in L])
                Q = plt.quiver(X, Y, U, V, C, pivot='mid', units='width')  # headwidth=3, headlength=5)
                # Q = plt.quiver(X, Y, U, V, C, edgecolor='k', facecolor='None', linewidth=0.1, headwidth=4, headlength=6, pivot='mid', units='inches')
                # plt.show()

                for i in range(len(airfoilIndices[0])):
                    idx = airfoilIndices[0][i]
                    idy = airfoilIndices[1][i]

                    offset = 0.25

                    fillX = [idx - offset, idx - offset, idx + offset, idx + offset]
                    fillY = [idy - offset, idy + offset, idy + offset, idy - offset]

                    plt.fill(fillY, fillX, 'k', alpha=1.0)

                plt.fill(maskIdX, maskIdY, 'r', alpha=1.0)
                plt.gca().set_xlim(xmin, xmax)
                plt.gca().set_ylim(ymin, ymax)
                if output_as_pdf:
                    plt.savefig(inputFN, format='pdf')
                elif output_as_png:
                    plt.savefig(inputFNpng, format='png')
                plt.close()

                plt.figure()
                X, Y = np.meshgrid(np.arange(128), np.arange(128))
                U = outputs_denormalized[0,0, :, :]
                V = outputs_denormalized[0,1, :, :]
                L = np.sqrt(U ** 2 + V ** 2)
                C = np.array([mapValueToColor(minMagn, maxMagn, i) for i in L])
                Q = plt.quiver(X, Y, U, V, C, pivot='mid', units='width')  # , headwidth=3, headlength=5)
                # Q = plt.quiver(X, Y, U, V, C, edgecolor='k', facecolor='None', linewidth=0.1, headwidth=4, headlength=6, pivot='mid', units='inches')
                # plt.show()
                plt.gca().set_xlim(xmin, xmax)
                plt.gca().set_ylim(ymin, ymax)
                if output_as_pdf:
                    plt.savefig(outputFN, format='pdf')
                elif output_as_png:
                    plt.savefig(outputFNpng, format='png')
                plt.close()

                plt.figure()
                X, Y = np.meshgrid(np.arange(128), np.arange(128))
                U = targets_denormalized[0,0, :, :]
                V = targets_denormalized[0,1, :, :]
                L = np.sqrt(U ** 2 + V ** 2)
                C = np.array([mapValueToColor(minMagn, maxMagn, i) for i in L])
                # W = np.array([0.005 * l / 80.0 for l in L])
                Q = plt.quiver(X, Y, U, V, C, pivot='mid', units='width')  # headwidth=3, headlength=5)
                plt.gca().set_xlim(xmin, xmax)
                plt.gca().set_ylim(ymin, ymax)
                if output_as_pdf:
                    plt.savefig(targetFN, format='pdf')
                elif output_as_png:
                    plt.savefig(targetFNpng, format='png')

                for i in range(len(airfoilIndices[0])):
                    idx = airfoilIndices[0][i]
                    idy = airfoilIndices[1][i]
                    offset = 0.25

                    fillX = [idx - offset, idx - offset, idx + offset, idx + offset]
                    fillY = [idy - offset, idy + offset, idy + offset, idy - offset]
                    plt.fill(fillY, fillX, 'k', alpha=1.0)

                for i in range(len(stampedAirfoilIndices[0])):
                    idx = stampedAirfoilIndices[0][i]
                    idy = stampedAirfoilIndices[1][i]
                    offset = 0.25

                    fillX = [idx - offset, idx - offset, idx + offset, idx + offset]
                    fillY = [idy - offset, idy + offset, idy + offset, idy - offset]
                    plt.fill(fillY, fillX, 'k', alpha=1.0)
                if output_as_pdf:
                    plt.savefig(targetAirfoilFN, format='pdf')
                elif output_as_png:
                    plt.savefig(targetAirfoilFNpng, format='png')
                plt.close()


    log(lf, "\n")
    L1val_accum     /= len(testLoader)
    lossPer_p_accum /= len(testLoader)
    lossPer_v_accum /= len(testLoader)
    lossPer_accum   /= len(testLoader)
    L1val_dn_accum  /= len(testLoader)
    log(lf, "Loss percentage (p, v, combined): %f %%    %f %%    %f %% " % (lossPer_p_accum*100, lossPer_v_accum*100, lossPer_accum*100 ) )
    log(lf, "L1 error: %f" % (L1val_accum) )
    log(lf, "Denormalized error: %f" % (L1val_dn_accum) )
    log(lf, "\n")

    avgLoss += lossPer_accum
    losses.append(lossPer_accum)

avgLoss /= len(losses)
lossStdErr = np.std(losses) / math.sqrt(len(losses))
log(lf, "Averaged relative error and std dev:   %f , %f " % (avgLoss,lossStdErr) )
