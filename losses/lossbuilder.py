import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.model_zoo as model_zoo
import torchvision.models as models
import numpy as np
import pytorch_ssim


try:
    from LovaszSoftmax.pytorch.lovasz_losses import lovasz_hinge
except ImportError:
    pass

class LossBuilder:
    def __init__(self, device):
        self.vgg_path = 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth'
        self.vgg_used = False
        self.device = device

    @staticmethod
    def _rgb_subtraction(images): # images is BCWH
        channels = list(torch.split(images, split_size_or_sections=1, dim=1))
        for i in range(3):
            channels[i] -= VGG_MEAN[i]
        return torch.cat(channels, dim=1)

    # create a module to normalize input image so we can easily put it in a
    # nn.Sequential
    class VGGNormalization(nn.Module):
        def __init__(self, device):
            super().__init__()
            # .view the mean and std to make them [C x 1 x 1] so that they can
            # directly work with image Tensor of shape [B x C x H x W].
            # B is batch size. C is number of channels. H is height and W is width.
            self.mean = torch.tensor([0.485, 0.456, 0.406]).view(-1, 1, 1).to(device)
            self.std = torch.tensor([0.229, 0.224, 0.225]).view(-1, 1, 1).to(device)

        def forward(self, img):
            # normalize img
            return (img[:,0:3,:,:] - self.mean) / self.std

    def mse(self):
        #class MSE(nn.Module):
        #    def __init__(self):
        #        super(MSE, self).__init__()
        #        self.mseLoss = nn.MSELoss()
        #    def forward(self, gt, pred):
        #        #gt = LossBuilder._preprocess(gt)
        #        #pred = LossBuilder._preprocess(pred)
        #        return self.mseLoss(gt, pred)
        #return MSE()
        return nn.MSELoss()

    def ssim(self):
        return pytorch_ssim.SSIM()

    def bcedice(self):
        class BCEDiceLoss(nn.Module):
            def __init__(self):
                super(BCEDiceLoss, self).__init__()

            def forward(self, input, target):
                bce = F.binary_cross_entropy_with_logits(input, target)
                smooth = 1e-5
                input = torch.sigmoid(input)
                num = target.size(0)
                input = input.view(num, -1)
                target = target.view(num, -1)
                intersection = (input * target)
                dice = (2. * intersection.sum(1) + smooth) / (input.sum(1) + target.sum(1) + smooth)
                dice = 1 - dice.sum() / num
                return 0.5 * bce + dice

    def lovaszhinge(self):
        class LovaszHingeLoss(nn.Module):
            def __init__(self):
                super(LovaszHingeLoss, self).__init__()

            def forward(self, input, target):
                input = input.squeeze(1)
                target = target.squeeze(1)
                loss = lovasz_hinge(input, target, per_image=True)

                return loss

    def inverse_mse(self, gt, pred):
        class InverseMSE(nn.Module):
            def __init__(self):
                super(InverseMSE, self).__init__()
                self.mseLoss = nn.MSELoss()
            def forward(self, gt, pred):
                gt = LossBuilder._preprocess(gt)
                pred = LossBuilder._preprocess(pred)
                pred = F.interpolate(pred, size=[gt.shape[1], gt.shape[2]], mode='bilinear')
                return self.mseLoss(gt, pred)
        return InverseMSE()
    
    def fft_mse(self):
        class FFT_MSE(nn.Module):
            def __init__(self):
                super(FFT_MSE, self).__init__()
                self.mseLoss = nn.MSELoss()
            def forward(self, gt, pred):
                gt = LossBuilder._preprocess(gt)
                pred = LossBuilder._preprocess(pred)
                gt = torch.rfft(gt, signal_ndim=3)
                pred = torch.rfft(pred, signal_ndim=3)
                return self.mseLoss(gt, pred)
        return FFT_MSE()

    def l1_loss(self):
        #class L1Loss(nn.Module):
        #    def __init__(self):
        #        super(L1Loss, self).__init__()
        #        self.l1Loss = nn.L1Loss()
        #    def forward(self, gt, pred):
        #        gt = LossBuilder._preprocess(gt)
        #        pred = LossBuilder._preprocess(pred)
        #        return self.l1Loss(gt, pred)
        #return L1Loss()
        return nn.L1Loss()

    def bce_loss(self):
        return nn.BCELoss()

    def bceLogits_loss(self):
        return nn.BCEWithLogitsLoss()

    def cos_angle_loss(self):
        #class COS_ANGLE_LOSS(nn.Module):
        #    def __init__(self):
        #        super(COS_ANGLE_LOSS, self).__init__()
        #        self.l1 = nn.L1Loss()
        #        self.cos = nn.CosineSimilarity()

        #    def forward(self, gt, pred):
        #        #val = torch.mean(abs(abs(gt[:,0]*gt[:,1]*gt[:,2]) - abs(pred[:,0]*pred[:,1]*pred[:,2])))
        #        #return val
        #        return self.l1(  self.cos( gt, pred)  )
        ##return nn.CosineSimilarity()
        #return COS_ANGLE_LOSS()
        return nn.CosineEmbeddingLoss()


    def grad_loss(self, device):
        class GRAD_LOSS(nn.Module):
            def __init__(self, device):
                super(GRAD_LOSS, self).__init__()
                #self.mseLoss = nn.MSELoss()
                #self.gradfilter = torch.nn.Conv1d(in_channels=1, out_channels=1, kernel_size=2, stride=1, padding=1, groups=1, bias=False)gradfilteryc
                inChannels = 4
                outChannels = 4

                self.gradfilterx = nn.Sequential()
                self.gradfiltery = nn.Sequential()
                gradfilterxc = nn.Conv2d(in_channels=inChannels, out_channels=outChannels, kernel_size=3, stride=1, padding=1, groups=1, bias=False)
                gradfilteryc = nn.Conv2d(in_channels=inChannels, out_channels=outChannels, kernel_size=3, stride=1, padding=1, groups=1, bias=False)
                #kernel = np.array([-1.0, 1.0])
                #kernel = torch.from_numpy(kernel).view(1, 1, 2)
                #self.gradfilter.weight.data = kernel
                #self.gradfilter.weight.requires_grad = False

                kernelx = np.array([0, 0, 0, -0.5, 0, 0.5, 0, 0, 0])
                kernely = np.array([0, -0.5, 0, 0, 0, 0, 0, 0.5, 0])
                kernelx = np.float32(np.tile(kernelx, [outChannels, inChannels, 1, 1]))
                kernely = np.float32(np.tile(kernely, [outChannels, inChannels, 1, 1]))
                kernelx = torch.from_numpy(kernelx).view(outChannels, inChannels, 3, 3)
                kernely = torch.from_numpy(kernely).view(outChannels, inChannels, 3, 3)
                gradfilterxc.weight.data = kernelx
                gradfilterxc.weight.requires_grad = False
                gradfilteryc.weight.data = kernely
                gradfilteryc.weight.requires_grad = False

                self.gradfilterx.add_module('gradx', gradfilterxc.float())
                self.gradfiltery.add_module('grady', gradfilteryc.float())
                self.gradfilterx.to(device)
                self.gradfiltery.to(device)
                self.criterion = nn.L1Loss()

            def forward(self, gt, pred):
                gtgradsum = torch.mean(torch.abs(gt[:, :, :, :-1] - gt[:, :, :, 1:])) + torch.mean(torch.abs(gt[:, :, :-1, :] - gt[:, :, 1:, :]))
                predgradsum = torch.mean(torch.abs(pred[:, :, :, :-1] - pred[:, :, :, 1:])) + torch.mean(torch.abs(pred[:, :, :-1, :] - pred[:, :, 1:, :]))

                return abs(gtgradsum - predgradsum)

                #gt = LossBuilder._preprocess(gt)
                #pred = LossBuilder._preprocess(pred)
                #gtGradx = self.gradfilterx(gt)
                #gtGrady = self.gradfiltery(gt)
                #predGradx= self.gradfilterx(pred)
                #predGrady = self.gradfiltery(pred)
                #gradLoss = torch.add(self.criterion(gtGradx, predGradx), self.criterion(gtGrady, predGrady))
                #return gradLoss
        return GRAD_LOSS(device)

    @staticmethod
    def _gram_matrix(features):
        dims = features.shape
        #features = torch.reshape(features, [-1, dims[-3], dims[-2] * dims[-1]])
        features = features.view(-1, dims[-3], dims[-2] * dims[-1])
        
        gram_matrix = torch.matmul(features, torch.transpose(features, 1, 2))
        normalized_gram_matrix = gram_matrix / (dims[-3] * dims[-2] * dims[-1])
        
        return normalized_gram_matrix #tf.matmul(features, features, transpose_a=True)

    @staticmethod
    def _normalize(features):
        dims = features.get_shape().as_list()
        return features / (dims[1] * dims[2] * dims[3])
    
    @staticmethod
    def _preprocess(images):
        #return (images / 255.0) * 2.0 - 1.0
        return images * 2.0 - 1.0 # images are already in [0,1]

    @staticmethod
    def _texture_loss(features, patch_size=16):
        '''
        the front part of features : gt features
        the latter part of features : pred features
        I will do calculating gt and pred features at once!
        '''

        #features = self._normalize(features)
        if patch_size is not None:
            batch_size, c, h, w = features.shape
            features = torch.unsqueeze(features, 1) # Bx1xCxHxW
            features = F.pad(features, (0, w % patch_size, 0, h % patch_size)) #Bx1xCxHpxWp
            features = torch.cat(torch.split(features, patch_size, 3), 1) # B x (H//patch_size) x C x patch_size x W
            features = torch.cat(torch.split(features, patch_size, 4), 1) # B x (H//patch_size)*(W//patchsize) x C x patch_size x patch_size
        patches_gt, patches_pred = torch.chunk(features, 2, dim=0)

        #features = tf.space_to_batch_nd(features, [patch_size, patch_size], [[0, 0], [0, 0]])
        #features = torch.reshape(features, [patch_size, patch_size, -1, h // patch_size, w // patch_size, c])
        #features = tf.transpose(features, [2, 3, 4, 0, 1, 5])
        #patches_gt, patches_pred = tf.split(features, 2, axis=0)
        #patches_gt = tf.reshape(patches_gt, [-1, patch_size, patch_size, c])
        #patches_pred = tf.reshape(patches_pred, [-1, patch_size, patch_size, c])
        
        gram_matrix_gt = LossBuilder._gram_matrix(patches_gt)
        gram_matrix_pred = LossBuilder._gram_matrix(patches_pred)
        
        #tl_features = torch.mean(torch.sum((gram_matrix_gt - gram_matrix_pred)**2, dim=1))
        tl_features = F.mse_loss(gram_matrix_gt, gram_matrix_pred)
        return tl_features
    
    class TextureLoss(nn.Module):
        def __init__(self, weight):
            super().__init__()
            self.weight = weight
        def forward(self, x):
            self.loss = LossBuilder._texture_loss(x) * self.weight
            return x

    class PerceptualLoss(nn.Module):
        def __init__(self, weight):
            super().__init__()
            self.weight = weight
        def forward(self, x):
            gt_pool, pred_pool = torch.chunk(x, 2, dim=0)
            #self.loss = torch.mean(torch.sum((gt_pool - pred_pool)**2, dim=1))
            self.loss = F.mse_loss(gt_pool, pred_pool) * self.weight
            return x
   
    def get_style_and_content_loss(self,
                                   content_layers,
                                   style_layers):

        cnn = models.vgg19(pretrained=True).features.eval()
        for p in cnn.parameters():
            p.requires_grad = False

        # normalization module
        normalization = LossBuilder.VGGNormalization(self.device)

        # just in order to have an iterable access to or list of content/syle
        # losses
        content_losses = []
        style_losses = []

        # assuming that cnn is a nn.Sequential, so we make a new nn.Sequential
        # to put in modules that are supposed to be activated sequentially
        model = nn.Sequential(normalization)

        i = 0  # increment every time we see a conv
        for layer in cnn.children():
            if isinstance(layer, nn.Conv2d):
                i += 1
                name = 'conv_{}'.format(i)
            elif isinstance(layer, nn.ReLU):
                name = 'relu_{}'.format(i)
                # The in-place version doesn't play very nicely with the ContentLoss
                # and StyleLoss we insert below. So we replace with out-of-place
                # ones here.
                layer = nn.ReLU(inplace=False)
            elif isinstance(layer, nn.MaxPool2d):
                name = 'pool_{}'.format(i)
            elif isinstance(layer, nn.BatchNorm2d):
                name = 'bn_{}'.format(i)
            else:
                raise RuntimeError('Unrecognized layer: {}'.format(layer.__class__.__name__))

            model.add_module(name, layer)

            if name in content_layers.keys():
                # add content loss:
                weight = content_layers[name]
                content_loss = LossBuilder.PerceptualLoss(weight)
                model.add_module("content_loss_{}".format(i), content_loss)
                content_losses.append(content_loss)

            if name in style_layers.keys():
                # add style loss:
                weight = style_layers[name]
                style_loss = LossBuilder.TextureLoss(weight)
                model.add_module("style_loss_{}".format(i), style_loss)
                style_losses.append(style_loss)

        # now we trim off the layers after the last content and style losses
        for i in range(len(model) - 1, -1, -1):
            if isinstance(model[i], LossBuilder.PerceptualLoss) or isinstance(model[i], LossBuilder.TextureLoss):
                break

        model = model[:(i + 1)]

        return model, style_losses, content_losses

    def _gan_loss(self):
        # gt_logits -> real, pred_logits -> fake
        # all values went through tf.nn.sigmoid
        class AdversarialLoss(nn.Module):
            def __init__(self):
                super().__init__()
            def forward(self, x): # x=[gt, pred]
                x = torch.sigmoid(x)
                valid_label = 1
                fake_label = 0
                gt_logits, pred_logits = torch.chunk(x, 2, dim=0)
                #statistics
                mean_gt_logits = torch.mean(gt_logits).item()
                mean_pred_logits = torch.mean(pred_logits).item()
                #print('Mean gt_logits:', mean_gt_logits)
                #print('Mean pred_logits:', mean_pred_logits)
                #for training the generator
                adv_g_loss = F.binary_cross_entropy(
                    pred_logits, 
                    torch.full(gt_logits.shape, valid_label, device=gt_logits.device))
                #for training the discrimator
                adv_gt_loss = F.binary_cross_entropy(
                    gt_logits, 
                    torch.full(gt_logits.shape, valid_label, device=gt_logits.device))
                adv_pred_loss = F.binary_cross_entropy(
                    pred_logits,
                    torch.full(pred_logits.shape, fake_label, device=pred_logits.device))
                adv_d_loss = adv_gt_loss + adv_pred_loss
                return adv_g_loss, adv_d_loss, mean_gt_logits, mean_pred_logits
            def train_discr(self, x, discr):
                """
                Trains the discriminator 'discr' with this loss.
                This is effectivly equal to calling 'loss,_ = self(discr(x))'.
                """
                return self(discr(x))

        return AdversarialLoss()

    def _wgan_loss(self, gradient_penalty):
        LAMBDA = 10 # Gradient penalty lambda hyperparameter
        # gt_logits -> real, pred_logits -> fake
        class WassersteinAdversarialLoss(nn.Module):
            def __init__(self, gradient_penalty):
                super().__init__()
                self.use_gradient_penalty = gradient_penalty
            def forward(self, x): # x=[gt, pred]
                # x = F.sigmoid(x) # IMPORTANT, no sigmoid!
                gt_f, pred_f = torch.chunk(x, 2, dim=0)
                #statistics
                mean_gt_logits = torch.mean(gt_f).item()
                mean_pred_logits = torch.mean(pred_f).item()
                #cost for training the generator
                adv_g_loss = -torch.mean(pred_f)
                #cost for training the discrimator
                adv_d_loss = torch.mean(pred_f) - torch.mean(gt_f)
                #done
                return adv_g_loss, adv_d_loss, mean_gt_logits, mean_pred_logits

            def forward_gradient_penalty(self, x, discr):
                #see https://github.com/igul222/improved_wgan_training/blob/master/gan_64x64.py#L476

                z = discr(x)
                disc_real, disc_fake = torch.chunk(z, 2, dim=0)
                b = x.shape[0]

                gen_cost = -torch.mean(disc_fake)
                disc_cost = torch.mean(disc_fake) - torch.mean(disc_real)

                alpha = torch.rand((b//2, 1, 1, 1), dtype=x.dtype, device=x.device, requires_grad=True)
                real_data, fake_data = torch.chunk(x, 2, dim=0)
                interpolates = real_data + alpha*(fake_data - real_data)
                #interpolates.requires_grad = True
                grad_outputs = torch.ones(b//2, 1, dtype=x.dtype, device=x.device, requires_grad=True)
                gradients = torch.autograd.grad(
                    discr(interpolates), 
                    interpolates, 
                    grad_outputs=grad_outputs,
                    create_graph=True,
                    only_inputs=True)[0]
                slopes = torch.norm(gradients.view(b//2, -1))
                gradient_penalty = torch.mean((slopes-1.0)**2)
                disc_cost += LAMBDA * gradient_penalty

                #done, compute additional statistics
                mean_gt_logits = torch.mean(disc_real).item()
                mean_pred_logits = torch.mean(disc_fake).item()
                return gen_cost, disc_cost, mean_gt_logits, mean_pred_logits

            def train_discr(self, x, discr):
                """
                Trains the discriminator 'discr' with this loss.
                This is effectivly equal to calling 'loss,_ = self(discr(x))'
                 with some additional effects like gradient penalties.
                """
                if self.use_gradient_penalty:
                    return self.forward_gradient_penalty(x, discr)
                else:
                    return self(discr(x))

        return WassersteinAdversarialLoss(gradient_penalty)

    def build_discriminator(self, model, resolution, input_channels):
        '''
        build_discriminator is only used for training!
        '''
        if model.lower()=='enhanceNetSmall'.lower():
            from .enhancenetsmall import EnhanceNetSmallDiscriminator
            return EnhanceNetSmallDiscriminator(resolution, input_channels)
        elif model.lower()=='enhanceNetLarge'.lower():
            from .enhancenetlarge import EnhanceNetLargeDiscriminator
            return EnhanceNetLargeDiscriminator(resolution, input_channels)
        elif model.lower()=='tecoGAN'.lower():
            from .tecogan import TecoGANDiscriminator
            return TecoGANDiscriminator(resolution, input_channels)
        else:
            raise ValueError('Unsupported discriminator model: %s'%model)

    def gan_loss(self, model, resolution, input_channels):
        """
        Builds the adversarial loss. Model is the name of the discriminator model.
        Currently supported: EnhanceNetSmall, EnhanceNetLarge, TecoGAN
        """
        discriminator = self.build_discriminator(model, resolution, input_channels)
        adv_loss = self._gan_loss()
        return discriminator, adv_loss

    def wgan_loss(self, model, resolution, input_channels, gradient_penalty=False):
        """
        Builds the adversarial loss with the Wasserstein GAN loss. 
        Model is the name of the discriminator model.
        Currently supported: EnhanceNetSmall, EnhanceNetLarge, TecoGAN
        """
        discriminator = self.build_discriminator(model, resolution, input_channels)
        adv_loss = self._wgan_loss(gradient_penalty)
        return discriminator, adv_loss