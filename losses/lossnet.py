import torch
import torch.nn as nn
import torch.nn.functional as F
import torchsummary
import numpy as np

from .lossbuilder import LossBuilder


class LossNet(nn.Module):
    """
    Main Loss Module.

    device: cpu or cuda
    opt: command line arguments (Namespace object) with:
     - losses: list of loss terms with weighting as string
     - further parameters depending on the losses

    """
    def __init__(self, device, input_channels, output_channels, high_res, padding, opt):
        super(LossNet, self).__init__()
        self.padding = padding
        self.upsample = opt.upsample
        #List of tuples (name, weight or None)
        self.loss_list = [(s.split(':')[0],s.split(':')[1]) if ':' in s else (s,None) for s in opt.losses.split(',')]
        
        # Build losses and weights
        builder = LossBuilder(device)
        self.loss_dict = {}
        self.weight_dict = {}

        #self.loss_dict['mse'] = builder.mse() #always use mse for psnr
        #self.weight_dict['mse'] = 0.0

        # For Cosine loss
        self.oneTensor = torch.ones(1).to(device)


        content_layers = []
        style_layers = []
        self.discriminator = None
        for name,weight in self.loss_list:
            if 'mse'==name or 'l2'==name or 'l2_loss'==name:
                self.loss_dict['mse'] = builder.mse()
                self.weight_dict['mse'] = float(weight) if weight is not None else 10
            elif 'bcedice'==name:
                self.loss_dict['bcedice'] = builder.bcedice()
                self.weight_dict['bcedice'] = float(weight) if weight is not None else 1.0
            elif 'lovaszhinge'==name:
                self.loss_dict['lovaszhinge'] = builder.lovaszhinge()
                self.weight_dict['lovaszhinge'] = float(weight) if weight is not None else 1.0
            elif 'ssim'==name:
                self.loss_dict['ssim'] = builder.ssim()
                self.weight_dict['ssim'] = float(weight) if weight is not None else 1.0
            elif 'inverse_mse'==name:
                self.loss_dict['inverse_mse'] = builder.inverse_mse()
                self.weight_dict['inverse_mse'] = float(weight) if weight is not None else 1.0
            elif 'fft_mse'==name:
                self.loss_dict['fft_mse'] = builder.fft_mse()
                self.weight_dict['fft_mse'] = float(weight) if weight is not None else 1.0
            elif 'l1'==name or 'l1_loss'==name:
                self.loss_dict['l1'] = builder.l1_loss()
                self.weight_dict['l1'] = float(weight) if weight is not None else 1.0
            elif 'bce'==name or 'bce_loss'==name:
                self.loss_dict['bce'] = builder.bce_loss()
                self.weight_dict['bce'] = float(weight) if weight is not None else 1.0
            elif 'bceLogits'==name or 'bceLogits_loss'==name:
                self.loss_dict['bceLogits'] = builder.bceLogits_loss()
                self.weight_dict['bceLogits'] = float(weight) if weight is not None else 1.0
            elif 'grad_loss'==name:
                self.loss_dict['grad_loss'] = builder.grad_loss(device)
                self.weight_dict['grad_loss'] = float(weight) if weight is not None else 1.0
            elif 'perceptual'==name:
                content_layers = [(s.split(':')[0],float(s.split(':')[1])) if ':' in s else (s,1) for s in opt.perceptualLossLayers.split(',')]
                #content_layers = [('conv_4',1), ('conv_12',1)]
                self.weight_dict['perceptual'] = float(weight) if weight is not None else 1.0
            elif 'cos_angle'==name:
                self.loss_dict['cos_angle'] = builder.cos_angle_loss()
                self.weight_dict['cos_angle'] = float(weight) if weight is not None else 1.0
            elif 'texture'==name:
                style_layers = [(s.split(':')[0],float(s.split(':')[1])) if ':' in s else (s,1) for s in opt.textureLossLayers.split(',')]
                #style_layers = [('conv_1',1), ('conv_3',1), ('conv_5',1)]
                self.weight_dict['texture'] = float(weight) if weight is not None else 1.0
            elif 'adv'==name or 'gan'==name:
                self.discriminator, self.adv_loss = builder.gan_loss(
                    opt.discriminator, high_res,
                    input_channels + output_channels)
                self.weight_dict['adv'] = float(weight) if weight is not None else 1.0
                self.discriminator_use_previous_image = False
                self.discriminator_clip_weights = False
            elif 'wgan'==name:
                self.discriminator, self.adv_loss = builder.wgan_loss(
                    opt.discriminator, high_res,
                    input_channels + output_channels)
                self.weight_dict['adv'] = float(weight) if weight is not None else 1.0
                self.discriminator_use_previous_image = False
                self.discriminator_clip_weights = True
            elif 'wgan-gp'==name: # Wasserstein-GAN with gradient penalty
                self.discriminator, self.adv_loss = builder.wgan_loss(
                    opt.discriminator, high_res,
                    input_channels + output_channels,
                    gradient_penalty = True)
                self.weight_dict['adv'] = float(weight) if weight is not None else 1.0
                self.discriminator_use_previous_image = False
                self.discriminator_clip_weights = False
            elif 'tadv'==name or 'tgan'==name: #temporal adversary
                self.discriminator, self.adv_loss = builder.gan_loss(
                    opt.discriminator, high_res,
                    input_channels + 2*output_channels)
                self.weight_dict['adv'] = float(weight) if weight is not None else 1.0
                self.discriminator_use_previous_image = True
                self.discriminator_clip_weights = False
            elif 'twgan'==name: #temporal Wassertein GAN
                self.discriminator, self.adv_loss = builder.wgan_loss(
                    opt.discriminator, high_res,
                    input_channels + 2*output_channels)
                self.weight_dict['adv'] = float(weight) if weight is not None else 1.0
                self.discriminator_use_previous_image = True
                self.discriminator_clip_weights = True
            elif 'twgan-gp'==name: #temporal Wassertein GAN with gradient penalty
                self.discriminator, self.adv_loss = builder.wgan_loss(
                    opt.discriminator, high_res,
                    input_channels + 2*output_channels,
                    gradient_penalty = True)
                self.weight_dict['adv'] = float(weight) if weight is not None else 1.0
                self.discriminator_use_previous_image = True
                self.discriminator_clip_weights = False
            else:
                raise ValueError('unknown loss %s'%name)

        if len(content_layers)>0 or len(style_layers)>0:
            self.pt_loss, self.style_losses, self.content_losses = \
                    builder.get_style_and_content_loss(dict(content_layers), dict(style_layers))

        self.loss_dict = nn.ModuleDict(self.loss_dict)
        print('Loss weights:', self.weight_dict)

    def print_summary(self, gt_shape, pred_shape, input_shape, prev_pred_warped_shape, num_batches):
        #Print networks for VGG + Discriminator
        if 'perceptual' in self.weight_dict.keys() or 'texture' in self.weight_dict.keys():
            print('VGG (Perceptual + Style loss)')
            torchsummary.summary(self.pt_loss, gt_shape, 2*num_batches)
        if self.discriminator is not None:
            print('Discriminator:')
            res = gt_shape[1]
            if self.discriminator_use_previous_image:
                input_images_shape = (gt_shape[0]+input_shape[0]+prev_pred_warped_shape[0], res, res)
            else:
                input_images_shape = (gt_shape[0]+input_shape[0], res, res)
            torchsummary.summary(self.discriminator, input_images_shape, 2*num_batches)


    @staticmethod
    def pad(img, border):
        """
        overwrites the border of 'img' with zeros.
        The size of the border is specified by 'border'.
        The output size is not changed.
        """
        if border==0: 
            return img
        b,c,h,w = img.shape
        img_crop = img[:,:,border:h-border,border:h-border]
        img_pad = F.pad(img_crop, (border, border, border, border), 'constant', 0)
        _,_,h2,w2 = img_pad.shape
        assert(h==h2)
        assert(w==w2)
        return img_pad

    def forward(self, gt, pred, input, prev_pred_warped):
        """
        gt: ground truth high resolution image (B x C=output_channels x 4W x 4H)
        pred: predicted high resolution image (B x C=output_channels x 4W x 4H)
        input: low resolution input image (B x C=input_channels x W x H)
               Only used for the discriminator, can be None if only the other losses are used
        prev_pred_warped: predicted image from the previous frame warped by the flow
               Shape: B x C=output_channels x 4W x 4H
               Only used for temporal losses, can be None if only the other losses are used
        """
        generator_loss = 0.0
        loss_values = {}

        # zero border padding
        gt = LossNet.pad(gt, self.padding)
        pred = LossNet.pad(pred, self.padding)
        if prev_pred_warped is not None:
            prev_pred_warped = LossNet.pad(prev_pred_warped, self.padding)

        # normal, simple losses
        for name in ['mse','inverse_mse','fft_mse','l1', 'bce', 'bceLogits', 'grad_loss']:
            if name in self.weight_dict.keys():
                loss = self.loss_dict[name](pred, gt)
                loss_values[name] = loss
                generator_loss += self.weight_dict[name] * loss

        if 'ssim' in self.weight_dict.keys():
            loss = 1 - self.loss_dict['ssim'](gt, pred)
            loss_values[name] = loss
            generator_loss += self.weight_dict['ssim'] * loss

        # Cosine_loss
        if 'cos_angle'  in self.weight_dict.keys():
            loss = self.loss_dict['cos_angle'](gt, pred, self.oneTensor)
            loss_values['cos_angle'] = loss
            generator_loss += self.weight_dict['cos_angle'] * loss

        # special losses
        if 'perceptual' in self.weight_dict.keys() or 'texture' in self.weight_dict.keys():
            style_weight=self.weight_dict.get('texture', 0)
            content_weight=self.weight_dict.get('perceptual', 0)
            style_score = 0
            content_score = 0

            input_images = torch.cat([gt, pred], dim=0)
            self.pt_loss(input_images)

            for sl in self.style_losses:
                style_score += sl.loss
            for cl in self.content_losses:
                content_score += cl.loss

            generator_loss += style_weight * style_score + content_weight * content_score
            if 'perceptual' in self.weight_dict.keys():
                loss_values['perceptual'] = content_score
            if 'texture' in self.weight_dict.keys():
                loss_values['texture'] = style_score

        if 'adv' in self.weight_dict.keys():
            input_high = F.interpolate(input, 
                                       size=(gt.shape[-2],gt.shape[-1]),
                                      mode=self.upsample)
            input_high = LossNet.pad(input_high, self.padding)
            if self.discriminator_use_previous_image:
                input_images = torch.cat([
                    torch.cat([gt, input_high, prev_pred_warped], dim=1), 
                    torch.cat([pred, input_high, prev_pred_warped], dim=1)
                    ], dim=0)
            else:
                input_images = torch.cat([
                    torch.cat([gt, input_high], dim=1), 
                    torch.cat([pred, input_high], dim=1)
                    ], dim=0)
            self.adv_g_loss, self.adv_d_loss, mean_gt_logits, mean_pred_logits = self.adv_loss(self.discriminator(input_images))
            loss_values['adv_g'] = self.adv_g_loss
            loss_values['adv_d'] = self.adv_d_loss
            loss_values['adv_mean_gt_logits'] = mean_gt_logits
            loss_values['adv_mean_pred_logits'] = mean_pred_logits
            generator_loss += self.weight_dict['adv'] * self.adv_g_loss

        return generator_loss, loss_values


# Adapted for frame interpolation
# Not needed atm
class LossNetFrame(LossNet):

    def forward(self, gt, pred, input):
        """
        gt: ground truth middle frame (B x C=output_channels x W x H)
        pred: predicted middle frame (B x C=output_channels x W x H)
        input: enveloping frames + additional channels (B x C=input_channels x W x H)
               Only used for the discriminator, can be None if only the other losses are used
        """
        generator_loss = 0.0
        loss_values = {}

        # zero border padding -> not used right now
        zero_padding = False
        if zero_padding:
            gt = LossNet.pad(gt, self.padding)
            pred = LossNet.pad(pred, self.padding)

        # normal, simple losses
        for name in ['mse', 'inverse_mse', 'fft_mse', 'l1', 'bce', 'bceLogits']:
            if name in self.weight_dict.keys():
                loss = self.loss_dict[name](gt, pred)
                loss_values[name] = loss
                generator_loss += self.weight_dict[name] * loss


