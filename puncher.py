import numpy as np
import random
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
import os
from pathlib import Path
import imageio
from skimage import transform

# Mapping for stamps
# 0 = no airfoil -> 2 stamped out
# 1 = airfoil -> 3 stamped out
stamps = { 0 : 2, 1 : 3 }

class Puncher(object):
    # Initializer of puncher
    # stamp_size = size of stamp kernel
    # num_stamps = number of stamps per file
    def __init__(self, stamp_size=4, num_stamps=1,  in_folder="data/train", out_folder="train",
                 specExportDir = '', usePrecomputedMasks = False, masksFolder = '/home/kumpf/projects/Neural_networks/image_inpainting/mask/testing_mask_dataset/'):
        self.stamp_size = stamp_size
        self.num_stamps = num_stamps
        self.enableNPZExport = True
        self.enablePDFExport = False
        self.inputFolder = in_folder
        self.usePrecomputedMasks = usePrecomputedMasks
        self.masksFolder = masksFolder
        self.masksFiles = []
        self.currMaskNr = 0

        if specExportDir == '':
            self.exportDir = "stampData/kernel_{}x{}/{}/".format(stamp_size, stamp_size, out_folder)
        else:
            self.exportDir = out_folder

    # Routine to stamp out kernels from the whole dataset
    def stampout(self, dataset):
        numFiles = len(dataset.loaded_files)
        newDataSize = numFiles * self.num_stamps
        #inputStamps = np.empty((newDataSize,) + (3, 128, 128))
        #targets = np.empty((newDataSize,) + (2, 128, 128))

        dataset.totalLength = newDataSize
        # repeat targets for at least num_stamps times
        #dataset.targets = np.repeat(dataset.targets, [self.num_stamps], axis=0)

        for i, file in enumerate(dataset.loaded_files):
            if i ==99:
                bbb= 7
            print("process file {}...".format(i))

            npFile = np.load(self.inputFolder + file)
            rawData = npFile['a']

            inputData = []
            target = []
            if self.usePrecomputedMasks:
                # now use velocities and mask as input
                inputData = np.zeros((3,rawData.shape[1], rawData.shape[2]))
                inputData[0:2] = rawData[0:2]
                #print('input data shape')
                #print(inputData.shape)
                target = rawData[0:2]
            else:
                # now use velocities and mask as input
                indices = np.array([4, 5, 2])
                inputData = rawData[indices]
                target = rawData[4:6]

            # inputData = dataset.inputs[i]
            # target = dataset.targets[i]
            # v_norm = (np.max(np.abs(inputData[0, :, :])) ** 2 + np.max(np.abs(inputData[1, :, :])) ** 2) ** 0.5
            #targetDenorm = dataset.denormalize(target, v_norm)

            # save target image
            if not os.path.exists(self.exportDir):
                os.makedirs(self.exportDir)


            # If already existing masks should be used, get all possible files

            if self.usePrecomputedMasks:
                self.masksFiles = os.listdir(self.masksFolder)

            # create mask
            # mask = inputData[2, :, :]
            # imgMask = np.zeros(mask.shape, dtype=bool)
            # imgMask[np.where(mask == 1)] = True
            #
            # plt.figure()
            # X, Y = np.meshgrid(np.arange(128), np.arange(128))
            # U = target[0, :, :]
            # V = target[1, :, :]
            #
            # C = np.sqrt(U ** 2 + V ** 2).flatten()
            # norm = Normalize()
            # norm.autoscale(C)

            # U = np.ma.array(U, mask=imgMask)
            # V = np.ma.array(V, mask=imgMask)

            # UN = np.ma.array(U, mask=~imgMask)
            # VN = np.ma.array(V, mask=~imgMask)

            # U[mask == 1] = np.nan
            # V[mask == 1] = np.nan
            #
            # colormap = plt.get_cmap("jet")
            # Q = plt.quiver(X, Y, U, V, color=colormap(norm(C)), pivot='mid', units='xy') #headwidth=3, headlength=5)
            # plt.imshow(~imgMask, extent=(0, 128, 0, 128), alpha=0.85, interpolation='nearest', cmap=plt.cm.gray)
            # filename = self.exportDir + ("target_file_{}.pdf".format(i))
            # plt.savefig(filename, format='pdf')
            # plt.close()

            # fig = ff.create_quiver(X, Y, U, V)
            # py.iplot(fig, filename="Quiver example")

            # compute possible stamp boundaries
            offset = int(self.stamp_size / 2)
            stampMin = offset
            stampMax = inputData.shape[1] - 1 - offset


            # use bounding box to stamp out velocities
            # stampRangeX = np.linspace(stampMin, stampMax, stampMax - stampMin + 1, dtype=int)
            # stampRangeY = np.linspace(stampMin, stampMax, stampMax - stampMin + 1, dtype=int)

            # find indices where airfoil is marked
            stampIndicesX = np.where(inputData[2] == 1)[0]
            stampIndicesY = np.where(inputData[2] == 1)[1]

            for ni in range(self.num_stamps):
                index = self.num_stamps * i + ni

                filenameNPZ = self.exportDir + ("stamped_file_{}_stamp_{}.npz".format(i, ni))

                pathToFile = Path(filenameNPZ)

                if pathToFile.is_file():
                    continue

                # stampListIdx = random.randint(0, len(stampRangeX) - 1)
                # stampListIdy = random.randint(0, len(stampRangeY) - 1)
                stampListIdx = []
                centerIdx = []
                centerIdy = []
                if len(stampIndicesX) == 0 or len(stampIndicesY) == 0:
                    centerIdx = random.randint(0, rawData.shape[1] - 1)

                    centerIdy = random.randint(0, rawData.shape[2] - 1)
                else:
                    stampListIdx = random.randint(0, len(stampIndicesX) - 1)
                    # stampListIdy = random.randint(0, len(stampIndicesY) - 1)
                    # centerIdx = stampRangeX[stampListIdx]
                    # centerIdy = stampRangeX[stampListIdy]

                    #stampRangeX = np.delete(stampRangeX, stampListIdx)
                    #stampRangeY = np.delete(stampRangeY, stampListIdy)
                    centerIdx = stampIndicesX[stampListIdx]
                    centerIdy = stampIndicesY[stampListIdx]

                    # remove index from selection
                    stampIndicesX = np.delete(stampIndicesX, stampListIdx)
                    stampIndicesY = np.delete(stampIndicesY, stampListIdx)

                stampedInput = []
                usedMask = []
                maskNr = -1

                if self.usePrecomputedMasks:
                    # Careful: StampedInput is the final input field. This is different to the method above,
                    # where the stamps are then subtracted
                    stampedInput12, stampedMask, maskNr = self.__stamp_data_using_reg_maks(inputData, 0, True)
                    stampedInput = np.zeros((stampedInput12.shape[0]+1, stampedInput12.shape[1], stampedInput12.shape[2]))
                    stampedInput[0:2] = stampedInput12
                    stampedInput[2] = stampedMask

                    
                    filenameNPZ = self.exportDir + ("stamped_file_{}_stamp_{}_mask_{}.npz".format(i, ni, maskNr))
                else:
                    stampedInput = self.__stamp_data(inputData, centerIdx, centerIdy)
                #inputStamps[index] = stampedInput
                #targets[index] = target

                #stampedDenorm = dataset.denormalize(stampedInput, v_norm)

                #print("\t-> output stamped data {}".format(ni))
                #filenameNPZ = self.exportDir + ("stamped_file_{}_stamp_{}.npz".format(i, ni))
                if self.usePrecomputedMasks:
                    np.savez_compressed(filenameNPZ, inp=stampedInput, out=target)
                else:
                    np.savez_compressed(filenameNPZ, inp=stampedInput, out=target)

                if (self.enablePDFExport):
                    mask = stampedInput[2, :, :]
                    imgMask = np.zeros(mask.shape, dtype=bool)
                    imgMask[np.where(mask >= 1)] = True

                    plt.figure()
                    X, Y = np.meshgrid(np.arange(128), np.arange(128))
                    U = stampedInput[0, :, :]
                    V = stampedInput[1, :, :]

                    C = np.sqrt(U ** 2 + V ** 2).flatten()

                    U = np.ma.array(U, mask=imgMask)
                    V = np.ma.array(V, mask=imgMask)

                    U[mask >= 1] = np.nan
                    V[mask >= 1] = np.nan

                    colormap = plt.get_cmap("jet")
                    Q = plt.quiver(X, Y, U, V, color=colormap(norm(C)), pivot='mid', units='xy', headwidth=3, headlength=5)
                    plt.imshow(~imgMask, extent=(0, 128, 0, 128), alpha=0.85, interpolation='nearest', cmap=plt.cm.gray)
                    filename = self.exportDir + ("stamped_file_{}_stamp_{}.pdf".format(i, ni))
                    plt.savefig(filename, format='pdf')
                    plt.close()

                    l1 = np.linalg.norm(inputData[[0, 1]] - stampedInput[[0, 1]])
                    print(l1)

        # dataset.inputs = inputStamps
        # dataset.targets = targets

        return dataset

    # Stamp out single file
    def __stamp_data(self, data, centerIdx, centerIdy):
        stamp = np.copy(data)
        offset = int(self.stamp_size / 2)

        # for y in range(data.shape[2]):
        #     for x in range(data.shape[1]):

        cyRange = range(max(centerIdy - offset, 0), min(centerIdy + offset + 1, data.shape[2]), 1)
        cxRange = range(max(centerIdx - offset, 0), min(centerIdx + offset + 1, data.shape[1]), 1)


        for cy in cyRange:
            for cx in cxRange:
                stamp[2, cx, cy] = stamps[data[2, cx, cy]]
                stamp[0, cx, cy] = 0
                stamp[1, cx, cy] = 0

        return stamp

    # Apply a .png mask to the file.
    def __stamp_data_using_reg_maks(self, data, maskSamplOpt = 0, randomMask = True):
        stamp = np.copy(data[0:2])
        #print(data.shape)
        currMask = []
        rd = []
        if randomMask:
            maxFileNr = 5000 # The masks thereafter have too much cut out for the beginning.

            rd = round(random.uniform(0,min(len(self.masksFiles) - 1, maxFileNr)))
            #print('random number')
            #print(rd)

            currMask = imageio.imread(self.masksFolder + self.masksFiles[rd])
        else:
            currMask = imageio.imread(self.masksFolder + self.masksFiles[self.currMaskNr])
            self.currMaskNr += 1
            if self.currMaskNr >= len(self.masksFiles):
                self.currMaskNr = 0
            rd = self.currMaskNr

        # The mask is loaded, however, might have the wrong size / shape
        sizeData = data.shape[1:3]
        ## Todo: check the shape. should be 128x128 or something alike
        resizedMask = transform.resize(currMask, sizeData) > 0.5 # returns a resized array with true false as mask.
        resizedMask = resizedMask.astype(int)
        #print(resizedMask[0])
        #print(max(resizedMask))
        #print(min(resizedMask))
        print(sum(sum(resizedMask)))
        #print(resizedMask)
        # masked == 1
        # Since stamp refers to the final inputField, 1-mask has to be taken
        stamp = stamp * (1 - resizedMask)
        #print(stamp.shape)
        #print(resizedMask.shape)
        return stamp, resizedMask, rd



        ## TODO: continue coding here




