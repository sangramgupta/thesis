# adapted by A. Kumpf (TUM)


from torch.utils.data import Dataset
import numpy as np
from os import listdir
from os import path
from os import walk

import random
from puncher import Puncher
from math import isnan
import sys
import h5py
import math
from utils import str2bool
from dotmap import DotMap

from natsort import natsorted, ns


# set relative paths to data
#DEFAULT_DIR_TRAIN = "data/train/"
DEFAULT_DIR_TRAIN = "stampData/kernel_15x15/train/"
DEFAULT_DIR_STAMPED = "stampData/10mWind_kernel_15x15/" #"data/10mWind/"
DEFAULT_DIR_TEST = "stampData/10mWind_kernel_15x15_test/"
DEFAULT_DIR_STAMP_OUTPUT = "stampData/kernel_15x15_2_test"
DEFAULT_DIR_POLYNOMS = "data/Polynoms/outlierDataset/"
#DEFAULT_DIR_POLYNOMS = "data/Polynoms/"
DEFAULT_DIR_TEST_POLY = "data/Polynoms/testImages/"

DEFAULT_DIR_FRAME = '/home/kumpf/projects/Neural_networks/physics_unet/data/Temperature/'
DEFAULT_DIR_FRAME_TEST = '/home/kumpf/projects/Neural_networks/physics_unet/data/Temperature/'

if False:
    if len(sys.argv) >= 2:
        DEFAULT_DIR_FRAME = '/home/kumpf/projects/physics_unet/data/Temperature/'
        DEFAULT_DIR_FRAME_TEST = '/home/kumpf/projects/physics_unet/data/Temperature/'
    if len(sys.argv) >= 2:
        DEFAULT_DIR_FRAME = sys.argv[2]
        DEFAULT_DIR_FRAME_TEST = sys.argv[3]

# global switch, make data dimensionless?
makeDimLess = True

augmentXFlip = True
augmentYFlip = True

includeSubDirs = True


# helper - compute absolute of inputs or targets
def find_absmax(data, use_targets, x):
    maxval = 0
    for i in range(data.totalLength):
        if use_targets == 0:
            temp_tensor = data.inputs[i]
        else:
            temp_tensor = data.targets[i]
        temp_max = np.max(np.abs(temp_tensor[x]))
        if temp_max > maxval:
            maxval = temp_max
    return maxval


# turbulence airfoil data
class TurbDataset(Dataset):

    TRAIN = 0
    TEST = 2

    def __init__(self, dataProp=None, mode=TRAIN,
                 dataDir = DEFAULT_DIR_STAMPED, dataDirTest=DEFAULT_DIR_TEST, dataDirPolynom=DEFAULT_DIR_POLYNOMS, dataDirTestPoly=DEFAULT_DIR_TEST_POLY,shuffle=0,
                 normMode=0, fileSequence = [], use_prestamped_data = True, bPolynom = False, outputFolderStamps = '',
                 stampSize = 15, numStamps = 1, maxFiles = 10000, masksFolder = "", usePrecomputedMasks = False):




        # member attributes
        self.dataDir = dataDir              # directory to training data
        self.dataDirTest = dataDirTest      # directory to test data

        self.stampSize = stampSize                 # size of stamp kernel per dataset
        self.numStamps = numStamps                 # number of stamps per file
        self.outputFolderStamps = outputFolderStamps

        self.masksFolder = masksFolder
        self.usePrecomputedMasks = usePrecomputedMasks

        self.mode = mode                    # training or test mode
        self.splitFactor = 0.2              # percentage of total portion reserved for validation data
        self.maxSplitNum = 400              # maximum number of allowed datasets for validation
        self.inputShape = (3, 128, 128)     # dimension of input data
        self.outputShape = (2, 128, 128)    # dimension of output data
        self.shuffle = shuffle              # number of shuffles of input data for randomized training / testing
        self.maxFiles = maxFiles              # maximum number of training files
        self.fileSequence = fileSequence    # sequence of files for iterative training

        self.normMax = []                   # array to save the norms in the normalization step. Not used yet

        self.use_prestamped_data = use_prestamped_data    # load pre-generated datasets
        self.loaded_files = []

        self.dataDirPolynom = dataDirPolynom  # directory to polynom dataset
        self.bPolynom = bPolynom              # if polynoms shall be generated/loaded, a different code is executed
        self.inputShapePoly = (1,128,128)  # dimension on input data (polynoms)
        self.outputShapePoly = (1,128,128)
        self.dataDirTestPoly = dataDirTestPoly

        # preprocess normalized data, generate training and validation data
        if (self.bPolynom):
            print("Preprocess polynom data")
        else:
            print("Preprocess turbulence data")
        self.__preprocess()

    def setbpolynom(self, bPolynom):
        self.bPolynom = bPolynom

    def __len__(self):
        return self.totalLength

    def __getitem__(self, idx):
        damaged = False
        # damaged = self.damaged
        # self.damaged = False
        return self.inputs[idx], self.targets[idx], False, False, idx, damaged

    def denormalize(self, data, v_norm, idxes):
        denorm_data = data.copy()
        for i in range(0, data.shape[0]):
            for j in range(min(2,denorm_data.shape[1])): # Targets have less channels than inputs, however, the input mask (3rd channel) should not be scaled...
                denorm_data[i, j, :, :] = denorm_data[i, j, :, :] * (
                            #self.normMaxTarg[idxes[i]] - self.normMinTarg[idxes[i]]) + self.normMinTarg[idxes[i]]
                            self.normMax[idxes[i]] )

        # denorm_data[0, :, :] *= self.max_inputs_velx
        # denorm_data[1, :, :] *= self.max_inputs_vely
        #
        # if makeDimLess:
        #     denorm_data[0, :, :] *= v_norm
        #     denorm_data[1, :, :] *= v_norm

        return denorm_data

    def denormalizePoly(self, data, v_norm):
        denorm_data = data.copy()
        denorm_data[0, :, :] /= (1.0 / self.max_inputs_velx)
        #denorm_data[1, :, :] /= (1.0 / self.max_inputs_vely)

        if makeDimLess:
            denorm_data[0, :, :] *= v_norm
            #denorm_data[1, :, :] *= v_norm

        return denorm_data

    def __loadFiles(self):

        data_dir = ""
        # enable_shuffle = False

        if self.mode == self.TRAIN:
            data_dir = self.dataDir
            # enable_shuffle = True
        elif self.mode == self.TEST:
            data_dir = self.dataDirTest
        else:
            print("wrong mode...")
            return

        files = []
        if includeSubDirs:
            for (dirpath, dirnames, filenames) in walk(data_dir):
                files.extend(filenames)
        else:
            files = listdir(data_dir)




        files.sort()
        files = files[:min(self.maxFiles, +len(files))]

        # if enabled, shuffle the input data
        # if enable_shuffle:
        #     for i in range(self.shuffle):
        #         random.shuffle(files)

        if self.mode == self.TRAIN:
            self.loaded_files = files

        if self.mode == self.TEST:
            self.loaded_files = files

            # self.totalLength = len(files)
            # # input channel (vx, vy, mask)
            # self.inputs = np.empty((len(files),) + self.inputShape)
            # # target channel (vx, vy)
            # self.targets = np.empty((len(files),) + self.outputShape)
            #
            # for i, file in enumerate(files):
            #     # print(file)
            #     npFile = np.load(data_dir + file)
            #     d = []
            #     if (self.use_prestamped_data):
            #         d = npFile['inp']
            #     else:
            #         d = npFile['a']
            #         # now use velocities and mask as input
            #         indices = np.array([4, 5, 2])
            #         self.inputs[i] = d[indices]
            #         self.targets[i] = d[4:6]

    def __loadAndNormalizeFilesPolynoms(self):
        # So far only loads the dataset, there is no normalization step included yet
        data_dir = self.dataDirPolynom
        if self.mode == self.TEST:
            data_dir = self.dataDirTestPoly
        nrFiles = len([name for name in listdir(data_dir) if path.isfile(data_dir + "/" + name)])
        nrFiles = min(self.maxFiles, nrFiles)
        self.nrFiles = nrFiles
        files = listdir(data_dir)
        files.sort()
        files = files[:min(self.maxFiles, +len(files))]

        #if self.mode == self.TRAIN:
        #    self.loaded_files = files

        #if self.mode == self.TEST:
        self.totalLength = nrFiles
        # input channel (scalar)
        self.inputs = np.empty((nrFiles,) + self.inputShapePoly, dtype=np.float32)
        # target channel (scalar)
        self.targets = np.empty((nrFiles,) + self.outputShapePoly, dtype=np.float32)

        # copy data into inputs and targets
        iDirs = 0
        for i, file in enumerate(files):
            if not path.isdir(data_dir + file):
                print(file)
                npFile = np.load(data_dir + file)
                self.inputs[i - iDirs] = npFile['inp']
                self.targets[i - iDirs] = npFile['out']
                if i - iDirs == self.nrFiles -1:
                    break
            else:
                iDirs += 1


    def __generateTrainingData(self):
        # punch out kernels from the data
        # generate new datasets
        # puncher = Puncher(self.stampSize, self.numStamps)

        outputFolder = "train"
        data_dir = ""
        if self.mode == self.TRAIN:
            data_dir = self.dataDir
            # enable_shuffle = True
        elif self.mode == self.TEST:
            data_dir = self.dataDirTest
        if (self.mode == self.TEST):
            outputFolder = "test"

        specExportDir = ''
        if self.outputFolderStamps != '':
            outputFolder = self.outputFolderStamps
            specExportDir = 'Yes'

        if self.usePrecomputedMasks:
            puncher = Puncher(self.stampSize, self.numStamps, data_dir, outputFolder, specExportDir, self.usePrecomputedMasks, self.masksFolder)
        else:
            puncher = Puncher(self.stampSize, self.numStamps, data_dir, outputFolder, specExportDir)


        self = puncher.stampout(self)

    def __loadStampedData(self):
        if (self.mode == self.TRAIN):
            data_dir = self.dataDir
        else:
            data_dir = self.dataDirTest
        enable_shuffle = True


        allFiles = listdir(data_dir)

        print("{} directory, where testset is loaded from".format(data_dir))
        files = []

        for f in allFiles:
            if f.endswith(".npz"):
                files += [f]

                # if len(files) > 10:
                #     break
        files.sort()
        # files = files[:min(self.maxFiles, len(files))]
        #
        # if enable_shuffle:
        #     for i in range(self.shuffle):
        #         random.shuffle(files)

        if len(self.fileSequence):
            files = files[max(self.fileSequence[0],0):min(self.fileSequence[1], len(files))]

            if enable_shuffle:
                for i in range(self.shuffle):
                    random.shuffle(files)
        else:
            if enable_shuffle:
                for i in range(self.shuffle):
                    random.shuffle(files)

            files = files[:min(self.maxFiles, len(files))]


        self.totalLength = len(files)

        # Count the number of files (not directories) available in total.
        self.nrFiles = len(files)
        #nrFiles = len([name for name in listdir(data_dir) if path.isfile(data_dir + "/" + name)])
        #nrFiles = min(self.maxFiles, nrFiles)
        #self.nrFiles = nrFiles

        # input channel (vx, vy, mask)
        self.inputs = np.empty((len(files),) + self.inputShape, dtype=np.float32)
        # target channel (vx, vy)
        self.targets = np.empty((len(files),) + self.outputShape, dtype=np.float32)

        iDirs = 0
        for i, file in enumerate(files):
            if not path.isdir(data_dir + file):
                if (i % 50 == 0):
                    print(file + " which is i: {}".format(i))
                npFile = np.load(data_dir + file)
                self.inputs[i - iDirs] = npFile['inp']
                self.targets[i - iDirs] = npFile['out']
                if i - iDirs == self.nrFiles - 1:
                    break
            else:
                iDirs += 1
        print("{} files with stamps loaded.".format(self.totalLength))


    def __generateValidationData(self):
        # split for train / validation sets (80 / 20)
        targetLength = self.totalLength - min(int(self.totalLength * self.splitFactor),
                                              self.maxSplitNum * self.numStamps)

        # take entries from [x, len - 1]
        self.valiInputs = self.inputs[targetLength:]
        self.valiTargets = self.targets[targetLength:]
        self.valiLength = self.totalLength - targetLength

        # take entries from [0, targetLength - 1]
        self.inputs = self.inputs[:targetLength]
        self.targets = self.targets[:targetLength]
        self.totalLength = self.inputs.shape[0]

    def __normalizeData(self):
        # make targets dimensionless
        if makeDimLess:
            self.normMax = np.zeros((self.totalLength))
            for i in range(self.totalLength):
                # only scale outputs, inputs are scaled by max only
                v_norm = (np.max(np.abs(self.inputs[i, 0, :, :])) ** 2 + np.max(
                    np.abs(self.inputs[i, 1, :, :])) ** 2) ** 0.5
                self.targets[i, 0, :, :] /= v_norm
                self.targets[i, 1, :, :] /= v_norm
                self.inputs[i, 0, :, :] /= v_norm
                self.inputs[i, 1, :, :] /= v_norm
                self.normMax[i] = v_norm
        bb = True

        # # maximum values
        # self.max_inputs_velx = find_absmax(self, 0, 0)
        # self.max_inputs_vely = find_absmax(self, 0, 1)
        # self.max_inputs_mask = find_absmax(self, 0, 2)
        #
        # # normalize data
        # self.targets[:, 0, :, :] *= (1.0 / self.max_inputs_velx)
        # self.targets[:, 1, :, :] *= (1.0 / self.max_inputs_vely)
        # self.inputs[:, 0, :, :] *= (1.0 / self.max_inputs_velx)
        # self.inputs[:, 1, :, :] *= (1.0 / self.max_inputs_vely)
        # self.inputs[:, 2, :, :] *= (1.0 / self.max_inputs_mask)

    def __normalizeDataPoly(self):
        # make targets dimensionless
        if makeDimLess:
            for i in range(self.totalLength):
                # only scale outputs, inputs are scaled by max only
                v_norm = np.max(np.abs(self.inputs[i, 0, :, :]))
                if v_norm != 0 and not isnan(v_norm):
                    self.targets[i, 0, :, :] /= v_norm
                    #self.targets[i, 1, :, :] /= v_norm
                    self.inputs[i, 0, :, :] /= v_norm
                else:
                    self.targets[i, 0, :, :] = 1
                    self.inputs[i, 0, :, :] = 1
                    print("dataset {} might be corrupted. Norm = {}".format(str(i), v_norm))
                    #self.inputs[i, 1, :, :] /= v_norm

        # maximum values
        self.max_inputs_velx = find_absmax(self, 0, 0)
        #self.max_inputs_vely = find_absmax(self, 0, 1)
        #self.max_inputs_mask = find_absmax(self, 0, 2)

        # normalize data
        self.targets[:, 0, :, :] *= (1.0 / self.max_inputs_velx)
        #self.targets[:, 1, :, :] *= (1.0 / self.max_inputs_vely)
        self.inputs[:, 0, :, :] *= (1.0 / self.max_inputs_velx)
        #self.inputs[:, 1, :, :] *= (1.0 / self.max_inputs_vely)
        #self.inputs[:, 2, :, :] *= (1.0 / self.max_inputs_mask)

    def __adaptVariablesForPoly(self):
        self.inputShape = self.inputShapePoly
        self.outputShape = self.outputShapePoly
        self.dataDirTest = self.dataDirTestPoly

    def __preprocess(self):
        if (self.bPolynom):
            self.__adaptVariablesForPoly()
            self.__loadAndNormalizeFilesPolynoms()
            self.__normalizeDataPoly()
            if not self.mode == self.TEST:
                self.__generateValidationData()
            return;



        # if not self.use_prestamped_data or self.mode == self.TEST:
        if not self.use_prestamped_data:
            # train from the prestamped data
            DEFAULT_DIR_TRAIN = DEFAULT_DIR_STAMPED
            # load and normalize data
            self.__loadFiles()
            # stamp out holes and generate new data
            self.__generateTrainingData()

            # exit program
            sys.exit()
        else:
            self.__loadStampedData()

        if not self.use_prestamped_data:
            return

        self.__normalizeData()

        if not self.mode == self.TEST:
            self.__generateValidationData()

            # for i in range(self.valiLength):
            #     input = self.valiInputs[i][[0, 1]]
            #     target = self.valiTargets[i][[0, 1]]
            #
            #     #l1 = np.linalg.norm(input[0] - target[0], 1) + np.linalg.norm(input[1] - target[1], 1)
            #     #print(l1)

# simplified validation data set (main one is TurbDataset above)
class ValiDataset(TurbDataset):
    def __init__(self, dataset):
        #super().__init__(dataset)
        self.inputs = dataset.valiInputs
        self.targets = dataset.valiTargets
        self.totalLength = dataset.valiLength

    def __len__(self):
        return self.totalLength

    def __getitem__(self, idx):
        damaged = False
        # damaged = self.damaged
        # self.damaged = False
        return self.inputs[idx], self.targets[idx], range(0,128), range(0,128), idx, damaged

class FrameDataset(TurbDataset):
    TRAIN = 0
    TEST = 2

    def __init__(self, dataProp=None, mode=TRAIN,
                 dataDir=DEFAULT_DIR_FRAME, dataDirTest=DEFAULT_DIR_FRAME_TEST,
                 shuffle=0,
                 normMode=0, fileSequence=[], use_prestamped_data=False, outputFolderStamps='',
                 stampSize=15, numStamps=1, maxFiles=10000, startFileID = 0,includeSubDirsArg=True,
                 readDataOnTheFly = False, readDataFromH5Py = False,
                 useRawInput = False, useGradientOfInput = False, useWarpedInput = True,
                 useLinearInterpInput = False,
                 useRawFeatureMaps = False, useWarpedFeatureMaps = True,
                 gridSizex = 256, gridSizey =256, splitFactor = 0.2, filterword = "",
                 flipXNow=True, flipYNow=True, cropInputBasedOnChannel = -1,
                 centerPatch = False,
                 dataResolutionX = 300, dataResolutionY=300, pcopts = [],
                 maxValFiles = 400):

        # member attributes
        self.dataDir = dataDir  # directory to training data
        self.dataDirTest = dataDirTest  # directory to test data

        self.stampSize = stampSize  # size of stamp kernel per dataset
        self.numStamps = numStamps  # number of stamps per file
        self.outputFolderStamps = outputFolderStamps

        self.useRawInput = useRawInput            # Whether the network should receive the original input images/fields
        self.useGradientOfInput = useGradientOfInput # Whether the gradients of the input fields should be computed and passed to the network
        self.useLinearInterpInput = useLinearInterpInput # Use interpolated intermediate frame as input
        self.useWarpedInput = useWarpedInput          # Whether the network should receive the warped input fields
        self.useRawFeatureMaps = useRawFeatureMaps      # Whether the network should receive results from conv-1 layer ResNet18
        self.useWarpedFeatureMaps = useWarpedFeatureMaps
        addInpChannels = 2*self.useRawInput + 4*self.useGradientOfInput + useLinearInterpInput + 2*self.useWarpedInput + 2*self.useRawFeatureMaps + 2*self.useWarpedFeatureMaps

        # The following lane has to be adjusted, if changes are made in the datareader part and additional channels are added or removed before the linear interpolation channel.
        self.posOfLinearInterp = 2*self.useRawInput + 4*self.useGradientOfInput

        self.readRawInpNevertheless = True

        self.mode = mode  # training or test mode
        self.splitFactor = splitFactor  # percentage of total portion reserved for validation data
        self.maxSplitNum = maxValFiles  # maximum number of allowed datasets for validation
        self.xResData = dataResolutionX     # If the data has higher resolution, it can be cropped to augment the dataset
        self.yResData = dataResolutionY
        self.xResNet = gridSizex
        self.yResNet = gridSizey

        self.inputShape = (addInpChannels, self.xResData, self.yResData)  # dimension of input data
        self.outputShape = (1, self.xResData, self.yResData)  # dimension of output data
        self.normMax = []  # array to save the norms in the normalization step.
        self.normMin = []
        self.normMaxTarg = []
        self.normMinTarg = []

        self.flipXNow = flipXNow
        self.flipYNow = flipYNow
        self.cropInputBasedOnChannel = cropInputBasedOnChannel

        self.centerPatch = centerPatch

        self.shuffle = shuffle  # number of shuffles of input data for randomized training / testing
        self.maxFiles = maxFiles  # maximum number of training files
        self.startFileID = startFileID
        self.includeSubDirs = includeSubDirsArg
        self.fileSequence = fileSequence  # sequence of files for iterative training
        self.readDataOnTheFly = readDataOnTheFly
        self.readDataFromHDF5 = readDataFromH5Py
        self.filterword = filterword

        self.damaged = False



        self.bPolynom = False

        self.use_prestamped_data = use_prestamped_data  # load pre-generated datasets
        self.loaded_files = []

        if pcopts != []:
            self.readAddParams(pcopts)

        # preprocess normalized data, generate training and validation data
        print("Preprocess Frame data")
        self.__preprocess()


    def readAddParams(self, pcopts):
        return
        # Do nothing

    def __getitem__(self, idx):
        if self.readDataOnTheFly:
            self.__loadFrameData(idx, False, True)
            self.__normalizeFrameData(idx)

            matIdxInp = []
            finalXRange = []
            finalYRange = []
            flipx = 1
            flipy = 1

            dataReady = False
            notReadyCount = 0
            while not dataReady:
                # The flip parameters control, whether in this specific call the variable should be flipped or not.
                # If the value is true, it is still dependent on the global settings in the beginning of this file!

                # return self.inputs[idx], self.targets[idx]
                if self.cropInputBasedOnChannel >= 0:
                    # specify the range in which the random might sample.
                    startRandX = int(round(random.uniform(self.cropVals[0,0], self.xResData - self.cropVals[0,1] - self.xResNet)))
                    startRandY = int(round(random.uniform(self.cropVals[0,2], self.yResData - self.cropVals[0,3] - self.yResNet)))
                else:
                    startRandX = round(random.uniform(0, self.xResData - self.xResNet))
                    startRandY = round(random.uniform(0, self.yResData - self.yResNet))

                if self.centerPatch:
                    startRandX = math.floor((self.xResData - self.xResNet) / 2)
                    startRandY = math.floor((self.yResData - self.yResNet) / 2)

                # new syntax then:  end:start:-1   vs start:end:1
                xIdxes = range(startRandX, startRandX + self.xResNet)
                yIdxes = range(startRandY, startRandY + self.yResNet)


                # If augmentXFlip is active, random between -1 and 1. Then use ::-1/::1 on the corresponding dimension
                # The same has to
                flipx = -(augmentXFlip * self.flipXNow * bool(random.getrandbits(1)) * 2 - 1)
                flipy = -(augmentYFlip * self.flipYNow * bool(random.getrandbits(1)) * 2 - 1)

                # The data was loaded into the first indices in inputs and targets.
                # Only one item is requested at once, so only read one item...
                idxRange = 0 #range(0, len(idx))
                # Here, also a mirror / turn action could be included for further data augmentation


                matIdxInp = np.ix_([idxRange], range(0, self.inputs.shape[1]), xIdxes[::flipx], yIdxes[::flipy])
                matIdxTar = np.ix_([idxRange], range(0, self.targets.shape[1]), xIdxes[::flipx], yIdxes[::flipy])

                finalXRange = xIdxes[::flipx]
                finalYRange = yIdxes[::flipy]

                sumMinVal = 5
                if np.sum(abs(self.inputs[matIdxInp].reshape(self.inputs.shape[1], len(xIdxes[::flipx]), len(yIdxes[::flipy]))[self.cropInputBasedOnChannel]))> sumMinVal:
                    dataReady = True
                else:
                    notReadyCount+=1
                    if notReadyCount > 20:
                        dataReady = True
                        print('The sum of all valid datapoints did not exceed {}, however, since it did not for 20 times, training is continued. This might have stopped the training! Check dataset.py getitem'.format(sumMinVal))

            # damaged = False
            damaged = self.damaged
            self.damaged = False

            matIdxMsk = np.ix_([idxRange], range(0, self.maskFromDepth.shape[1]), xIdxes[::flipx], yIdxes[::flipy])

            return self.inputs[matIdxInp].reshape(self.inputs.shape[1], len(xIdxes[::flipx]), len(yIdxes[::flipy])), \
                   self.targets[matIdxTar].reshape(self.targets.shape[1], len(xIdxes[::flipx]), len(yIdxes[::flipy])), \
                   finalXRange, finalYRange, idx, damaged, self.maskFromDepth[matIdxMsk].reshape(self.maskFromDepth.shape[1], len(xIdxes[::flipx]), len(yIdxes[::flipy]))

        else:
            # The flip parameters control, whether in this specific call the variable should be flipped or not.
            # If the value is true, it is still dependent on the global settings in the beginning of this file!

            #return self.inputs[idx], self.targets[idx]
            if self.cropInputBasedOnChannel >= 0:
                # specify the range in which the random might sample.
                startRandX = int(round(random.uniform(self.cropVals[0,0], self.xResData - self.cropVals[0,1] - self.xResNet)))
                startRandY = int(round(random.uniform(self.cropVals[0,2], self.yResData - self.cropVals[0,3] - self.yResNet)))
            else:
                startRandX = round(random.uniform(0, self.xResData - self.xResNet))
                startRandY = round(random.uniform(0, self.yResData - self.yResNet))

            # If augmentXFlip is active, random between -1 and 1. Then use ::-1/::1 on the corresponding dimension
            # The same has to
            flipx = -(augmentXFlip * self.flipXNow * bool(random.getrandbits(1)) * 2 - 1)
            flipy = -(augmentYFlip * self.flipYNow * bool(random.getrandbits(1)) * 2 - 1)

            # new syntax then:  end:start:-1   vs start:end:1
            xIdxes = range(startRandX,startRandX + self.xResNet)
            yIdxes = range(startRandY,startRandY + self.yResNet)

            # Here, also a mirror / turn action could be included for further data augmentation
            matIdxInp = []
            matIdxInp = np.ix_([idx], range(0, self.inputs.shape[1]), xIdxes[::flipx], yIdxes[::flipy])
            matIdxTar = np.ix_([idx], range(0, self.targets.shape[1]), xIdxes[::flipx], yIdxes[::flipy])

            finalXRange = xIdxes[::flipx]
            finalYRange = yIdxes[::flipy]

            # damaged = False
            damaged = self.damaged
            self.damaged = False

            matIdxMsk = np.ix_([idx], range(0, self.maskFromDepth.shape[1]), xIdxes[::flipx], yIdxes[::flipy])

            return self.inputs[matIdxInp].reshape(self.inputs.shape[1], len(xIdxes[::flipx]), len(yIdxes[::flipy] )), \
                   self.targets[matIdxTar].reshape(self.targets.shape[1], len(xIdxes[::flipx]), len(yIdxes[::flipy] )), \
                   finalXRange, finalYRange, idx, damaged, self.maskFromDepth[matIdxMsk].reshape(self.inputs.shape[1], len(xIdxes[::flipx]), len(yIdxes[::flipy] ))




        #
        #
        # startRandX = round(random.uniform(0,self.xResData - self.xResNet))
        #
        # startRandY = round(random.uniform(0,self.yResData - self.yResNet))
        # # Here, also a mirror / turn action could be included for further data augmentation
        # return self.inputs[idx, :, startRandX:startRandX + self.xResNet, startRandY:startRandY + self.yResNet], \
        #        self.targets[idx, :, startRandX:startRandX + self.xResNet, startRandY:startRandY + self.yResNet], \
        #        idx




    def loadFilesFrame(self):

        data_dir = ""
        # enable_shuffle = False

        if self.mode == self.TRAIN:
            data_dir = self.dataDir
            # enable_shuffle = True
        elif self.mode == self.TEST:
            data_dir = self.dataDirTest
        else:
            print("wrong mode...")
            return

        files = []
        print(str(includeSubDirs))
        if includeSubDirs:
            for (dirpath, dirnames, filenames) in walk(data_dir):
                for name in filenames:
                # ["{}{}".format(b_, a_) for a_, b_ in zip(a, b)]
                    files.extend([path.join(dirpath, name)])
        else:
            files = listdir(data_dir)

        fileHDF5 = []
        fileNamesHDF5 = []
        #fileNameHDF5InFile = []

        currCounter = 0 # Stores, in which hdf5 file the filename was found. Will be needed to retrieve the dataset...
        if self.readDataFromHDF5:
            for file in files:
                if file.__contains__(".hdf5"):
                    fileHDF5.append(file)
            for file in fileHDF5:
                f = h5py.File(file, "r", swmr=True)
                for fname in f:
                    fileNamesHDF5.append((file, fname))
                    #fileNameHDF5InFile.append(currCounter)
                f.close()
                currCounter += 1

            self.hdf5Files = fileHDF5
            files = fileNamesHDF5
            if self.filterword != "":
                # The actual filename is contained as the second entry of the tuple
                files = [k for k in files if self.filterword in k[1]]
        else:
            if self.filterword != "":
                files = [k for k in files if self.filterword in k]


        print('Number of files found ' + str(len(files)))

        #if not self.readDataFromHDF5:
        files = natsorted(files, alg=ns.IGNORECASE)
        files = files[self.startFileID:min(self.startFileID + self.maxFiles, +len(files))]


        self.totalLength = len(files)

        # if enabled, shuffle the input data
        # if enable_shuffle:
        #     for i in range(self.shuffle):
        #         random.shuffle(files)

        if self.mode == self.TRAIN:
            self.loaded_files = files

        if self.mode == self.TEST:
            self.loaded_files = files


    def copyFromHDDToArrays(self, i, iDirs, npFile):
        currChannel = 0
        if self.useRawInput:
            self.inputs[i - iDirs, currChannel: currChannel + 2] = npFile['a'][0:2, :, :]
            currChannel += 2
        if self.useGradientOfInput:
            self.inputs[i - iDirs, currChannel: currChannel + 2] = npFile['a'][0:1, :, :]  # np.gradient(self.inputs[i - iDirs, currChannel -2])
            self.inputs[i - iDirs, currChannel + 2: currChannel + 4] = npFile['a'][1:2, :, :]  # np.gradient(self.inputs[i - iDirs, currChannel - 1])
            currChannel += 4
        if self.useLinearInterpInput:
            self.inputs[i - iDirs, currChannel: currChannel + 1] = np.mean(npFile['a'][0:2, :, :], axis=0)

            currChannel += 1

        if self.readRawInpNevertheless:
            self.inputsRaw[i - iDirs, 0: 0 + 2] = npFile['a'][0:2, :, :]
        if self.useWarpedInput:
            self.inputs[i - iDirs, currChannel: currChannel + 2] = npFile['imgWarped'][0:2, :, :]
            currChannel += 2
        if self.useRawFeatureMaps:
            self.inputs[i - iDirs, currChannel: currChannel + 2] = npFile['feat'][0:2, :, :]
            self.nrFeatureMaps = 2
            currChannel += 2
        if self.useWarpedFeatureMaps:
            self.inputs[i - iDirs, currChannel: currChannel + 2] = npFile['featWarped'][0:2, :, :]
            self.nrWarpedFeatureMaps = 2
            currChannel += 2

        self.targets[i - iDirs] = npFile['a'][2, :, :]

        currLength = i - iDirs + 1
        return currLength

    # This functions crops input fields based on 0's everywhere. So, if only a small item is placed somewhere in the input, the randomly sampled
    # patch is more likely to sample from data instead of 0's
    def generateCropValues(self, currLength):
        r = 1
        l = 0
        t = 0
        b = 1
        inputShape = self.inputs.shape
        for i in range(0,currLength):
            while t < inputShape[2] and np.all(self.inputs[i, self.cropInputBasedOnChannel, t] == 0):
                t += 1
            while b < inputShape[2] and np.all(self.inputs[i, self.cropInputBasedOnChannel, -b] == 0) :
                b += 1
            while l < inputShape[3] and np.all(self.inputs[i, self.cropInputBasedOnChannel,:, l] == 0):
                l += 1
            while r < inputShape[3] and np.all(self.inputs[i, self.cropInputBasedOnChannel,:, -r] == 0):
                r += 1
            # The maximal cropping values are known now. If too much is cropped, they have to be relaxed.
            b -= 1
            r -= 1
            if inputShape[2] - t -b < self.xResNet:
                if inputShape[2] - b < 0:
                    t = 0
                    b = inputShape[2] - self.xResNet
                else:
                    t = inputShape[2] - b - self.xResNet
            if inputShape[3] - r - l < self.yResNet:
                if inputShape[3] - r - l < 0:
                    l = 0
                    r = inputShape[3] - self.yResNet
                else:
                    l = inputShape[3] - r - self.yResNet
            self.cropVals[i,:] = [t,b, l ,r]



    def __loadFrameData(self, idx = [], readOnlyValidation = False, printAllUsedFilenames = False):
        allFiles = []
        if self.mode == self.TRAIN and idx != []:
            data_dir = self.dataDir
            allFiles = self.loaded_files[idx]

        elif (self.mode == self.TRAIN):
            data_dir = self.dataDir
            allFiles = self.loaded_files
        else:
            data_dir = self.dataDirTest
            #allFiles = listdir(data_dir)
            allFiles = self.loaded_files # This should also work correctly here, since the right files should be contained in the list...
        enable_shuffle = self.shuffle > 0

        files = []

        if idx == []:
            print("{} directory, where dataset is loaded from".format(data_dir))
            if self.readDataFromHDF5:
                self.filesNPZ = [] # The filelist should already be complete in self.loaded_files
                files = self.loaded_files
            else:
                for f in allFiles:
                    if f.endswith(".npz"):
                        files += [f]
                self.filesNPZ = files
        else:
            if self.readDataFromHDF5:
                files = self.loaded_files
            else:
                files = self.filesNPZ


        # Do not shuffle etc. HDF5 data.
        if idx == [] and not self.readDataFromHDF5:
            files.natsorted(files, alg=ns.IGNORECASE)

            if len(self.fileSequence):
                files = files[max(self.fileSequence[0],0):min(self.fileSequence[1], len(files))]

                if enable_shuffle:
                    for i in range(self.shuffle):
                        random.shuffle(files)
            else:
                if enable_shuffle:
                    for i in range(self.shuffle):
                        random.shuffle(files)

                files = files[self.startFileID:min(self.startFileID + self.maxFiles, len(files))]
        elif idx == []:
            if enable_shuffle:
                random.shuffle(files)
            else:
                files = natsorted(files, alg=ns.IGNORECASE)

        if not self.readDataFromHDF5:
            self.totalLength = len(files)

        # Count the number of files (not directories) available in total.
        self.nrFiles = len(files)
## ToDo Thursday: Check targetLength (80p of 80 when 100 files, check length of files when reading validation set etc...)
        if self.readDataOnTheFly:
            if not readOnlyValidation:
                self.inputs = np.empty((1,) + self.inputShape, dtype=np.float32)
                self.maskFromDepth = np.empty((1,) + (1,self.inputShape[1], self.inputShape[2]), dtype=np.float32)
                self.cropVals = np.empty((1,) + (4,))
                # target channel (vx, vy)
                self.targets = np.empty((1,) + self.outputShape, dtype=np.float32)
                self.inputsRaw = []
                if self.readRawInpNevertheless:
                    self.inputsRaw = np.empty((1 , 2, self.inputShape[1], self.inputShape[2]))
                if self.readDataFromHDF5:
                    files = {files[idx]}
                else:
                    files = {files[idx]}

            else:
                targetLength = self.totalLength - min(int(self.totalLength * self.splitFactor),
                                                      self.maxSplitNum * self.numStamps)
                # input channel (lev0, lev2)
                self.inputs = np.empty((len(files)-targetLength,) + self.inputShape, dtype=np.float32)
                self.maskFromDepth = np.empty((len(files)-targetLength,) + (1,self.inputShape[1],self.inputShape[2]), dtype=np.float32)
                self.cropVals = np.empty((len(files)-targetLength,) +  (4,))
                # target channel (vx, vy)
                self.targets = np.empty((len(files)-targetLength,) + self.outputShape, dtype=np.float32)
                self.inputsRaw = []
                if self.readRawInpNevertheless:
                    self.inputsRaw = np.empty((len(files) - targetLength , 2, self.inputShape[1], self.inputShape[2]))
        else:
            # input channel (lev0, lev2)
            self.inputs = np.empty((len(files),) + self.inputShape, dtype=np.float32)
            self.maskFromDepth = np.empty((len(files),) + (1,self.inputShape[1], self.inputShape[2]), dtype=np.float32)
            self.cropVals = np.empty((len(files),) + (4,))
            # target channel (vx, vy)
            self.targets = np.empty((len(files),) + self.outputShape, dtype=np.float32)
            self.inputsRaw = []
            if self.readRawInpNevertheless:
                self.inputsRaw = np.empty((len(files) , 2, self.inputShape[1], self.inputShape[2]))

        if idx == [] and self.readDataOnTheFly and readOnlyValidation:
            # Select the files which should be used as validation dataset
            files = files[targetLength:]
            # Remove the validation files from the file list, which will be used to load data on the fly.
            if not self.readDataFromHDF5:
                self.filesNPZ = self.filesNPZ[0:targetLength]

        currLength = 0
        iDirs = 0
        for i, file in enumerate(files):
            if printAllUsedFilenames:
                print(file)
            if self.readDataFromHDF5 or not path.isdir(data_dir + file):
                if not self.readDataOnTheFly and (i % 50 == 0):
                    if self.readDataFromHDF5:
                        print(file[1] + " which is i: {}".format(i))
                    else:
                        print(file + " which is i: {}".format(i))
                npFile = []
                if self.readDataFromHDF5:
                    currhdfFile = h5py.File(file[0], "r", swmr=True)
                    npFile = currhdfFile[file[1]]
                else:
                    if includeSubDirs:
                        npFile = np.load(file)
                    else:
                        npFile = np.load(data_dir + file)

                currLength = self.copyFromHDDToArrays(i,iDirs,npFile)
                if self.cropInputBasedOnChannel >= 0:
                    self.generateCropValues(currLength)

                # currChannel = 0
                # if self.useRawInput:
                #     self.inputs[i - iDirs, currChannel : currChannel+2] = npFile['a'][0:2, :, :]
                #     currChannel += 2
                # if self.useGradientOfInput:
                #     self.inputs[i - iDirs, currChannel: currChannel + 2] = npFile['a'][0:1, :, :]#np.gradient(self.inputs[i - iDirs, currChannel -2])
                #     self.inputs[i - iDirs, currChannel + 2: currChannel + 4] = npFile['a'][1:2, :, :]#np.gradient(self.inputs[i - iDirs, currChannel - 1])
                #     currChannel += 4
                # if self.useLinearInterpInput:
                #     self.inputs[i - iDirs, currChannel: currChannel + 1] = np.mean(npFile['a'][0:2, :, :], axis=0)
                #
                #     currChannel += 1
                #
                # if self.readRawInpNevertheless:
                #     self.inputsRaw[i - iDirs, 0 : 0 + 2] = npFile['a'][0:2, :, :]
                # if self.useWarpedInput:
                #     self.inputs[i - iDirs, currChannel : currChannel+2] = npFile['imgWarped'][0:2, :, :]
                #     currChannel += 2
                # if self.useRawFeatureMaps:
                #     self.inputs[i - iDirs, currChannel : currChannel+2] = npFile['feat'][0:2, :, :]
                #     self.nrFeatureMaps = 2
                #     currChannel += 2
                # if self.useWarpedFeatureMaps:
                #     self.inputs[i - iDirs, currChannel : currChannel+2] = npFile['featWarped'][0:2, :, :]
                #     self.nrWarpedFeatureMaps = 2
                #     currChannel += 2
                #
                # self.targets[i - iDirs] = npFile['a'][2, :,  :]

                currLength = i - iDirs + 1

                if i - iDirs == self.nrFiles - 1:
                    break
            else:
                iDirs += 1

        # Reduce the size of inputs and targets arrays to the actual size
        self.inputs = self.inputs[0 : currLength]
        self.cropVals = self.cropVals[0 : currLength]
        self.targets = self.targets[0 : currLength]

        # If data is read on the fly, the totalLength after this method will be one batch size.
        if not self.readDataFromHDF5:
            if self.readDataOnTheFly:
                self.totalLength = len(self.filesNPZ)
            else:
                self.totalLength = currLength

                print("{} files with stamps loaded.".format(self.totalLength))


    # Generates the ranges for channels which should be normalized together
    def generateChannelChunkRanges(self):
        channelRangeStart = []
        channelRangeEnd = []
        currStart = 0
        currAdd = 0
        if self.useRawInput:
            currAdd += 2
        if self.useWarpedInput:
            currAdd += 2
        channelRangeStart.append(0)
        channelRangeEnd.append(currAdd)

        currStart += currAdd
        currAdd = 0
        if self.useGradientOfInput:
            currAdd += 4
            channelRangeStart.append(currStart)
            channelRangeEnd.append(currStart + currAdd)
        currStart += currAdd
        currAdd = 0
        if self.useLinearInterpInput:
            currAdd += 1
            channelRangeStart.append(currStart)
            channelRangeEnd.append(currStart + currAdd)

        currStart += currAdd
        currAdd = 0
        if self.useRawFeatureMaps:
            currAdd += self.nrFeatureMaps
        if self.useWarpedFeatureMaps:
            currAdd += self.nrWarpedFeatureMaps
        if currAdd > 0:
            channelRangeStart.append(currStart)
            channelRangeEnd.append(currStart + currAdd)
        return (channelRangeStart, channelRangeEnd)

    def generateChannelTargetChunkRanges(self):
        return ([],[])

    def __normalizeFrameData(self, idxes = []):

        if makeDimLess:
            # Implement to normalize each channel independently. Therefore, the normMax has to be stored for every channel as well.
            # NOTE: The above comment is outdated...

            if idxes == [] and self.normMax == []:

                # Get the channels, which have to be normalized together / separately.
                # ChannelRangeStart/End contain the indices in self.input which have to be normalized together
                self.channelRangeStart, self.channelRangeEnd = self.generateChannelChunkRanges()
                self.channelTargetRangeStart, self.channelTargetRangeEnd = self.generateChannelTargetChunkRanges()

                self.normMax = np.empty((self.totalLength, len(self.channelRangeStart)))
                self.normMin = np.empty((self.totalLength, len(self.channelRangeStart)))
                if self.channelTargetRangeStart == []:
                    self.normMaxTarg = np.empty((self.totalLength))
                    self.normMinTarg = np.empty((self.totalLength))
                else:
                    self.normMaxTarg = np.empty((self.totalLength, len(self.channelTargetRangeStart)))
                    self.normMinTarg = np.empty((self.totalLength, len(self.channelTargetRangeStart)))


        # make targets dimensionless
        if self.readDataOnTheFly and idxes != []:
            #if makeDimLess:
            # ToDo: only initiailize the arrays once, then use them and successively update the values, when a batch is being loaded.
            # How is the validation dataset handled? also check, whether the for loop over totalLength is still correct...
            # Preliminary result: The validation dataset is never reconstructed, hence, the vales may be overwritten. If that should be changed
            # they would have to be copied when initializing the validation class object.


            #for i in range(len(idxes)):
            # So far, idxes is only one value. that might change maybe...



            ####!!!!!!!!!!!!!!!!!!!#######
            # Implement a version where the target is scaled using the input channels (the same ranges etc.) to normalize. In inference, the input will have range 0-1
            # but the target most probably has a different range. The network should be able to exceed the range of the input for the output.



            i = idxes

            for ir in range(0, len(self.channelRangeStart)):
                self.normMax[i][ir] = np.max(np.amax(np.amax(self.inputs[0, self.channelRangeStart[ir]:self.channelRangeEnd[ir], :, :], axis=1), axis=1))
                self.normMin[i][ir] = np.min(np.amin(np.amin(self.inputs[0, self.channelRangeStart[ir]:self.channelRangeEnd[ir], :, :], axis=1), axis=1))

                for j in range(self.channelRangeStart[ir], self.channelRangeEnd[ir]):
                    self.inputs[0, j, :, :] = (self.inputs[0, j, :, :] - self.normMin[i][ir]) / (
                            self.normMax[i][ir] - self.normMin[i][ir])

            # self.normMaxTarg[i] = np.amax(np.amax(self.targets[0, :, :, :], axis=1), axis=1)
            # self.normMinTarg[i] = np.amin(np.amin(self.targets[0, :, :, :], axis=1), axis=1)
            # self.targets[0] = (self.targets[0] - self.normMinTarg[i]) / (self.normMaxTarg[i] - self.normMinTarg[i])
            if self.channelTargetRangeStart == []:
                # Normalize Targets like first 2 channels of input
                self.normMaxTarg[i] = self.normMax[i][0]
                self.normMinTarg[i] = self.normMin[i][0]
                self.targets[0] = (self.targets[0] - self.normMinTarg[i]) / (self.normMaxTarg[i] - self.normMinTarg[i])
            else:
                for ir in range(0, len(self.channelTargetRangeStart)):
                    self.normMaxTarg[i][ir] = self.normMax[i][ir]
                    self.normMinTarg[i][ir] = self.normMin[i][ir]
                    divider = (self.normMaxTarg[i][ir] - self.normMinTarg[i][ir])
                    if np.isnan(self.normMaxTarg[i][ir] - self.normMinTarg[i][ir]) or (self.normMaxTarg[i][ir] - self.normMinTarg[i][ir]) == 0:
                        self.damaged = True
                        divider = 1.0

                    self.targets[0, self.channelTargetRangeStart[ir]:self.channelTargetRangeEnd[ir], :, :] = \
                        (self.targets[0, self.channelTargetRangeStart[ir]:self.channelTargetRangeEnd[ir]] - self.normMinTarg[i][ir]) / \
                        divider



            normalizeEachChannelSeparately = False # This is the old code. Just there in case it should be changed back.
            if normalizeEachChannelSeparately:
                self.normMax[i] = np.amax(np.amax(self.inputs[0, :, :, :], axis=1), axis=1)
                self.normMin[i] = np.amin(np.amin(self.inputs[0, :, :, :], axis=1), axis=1)

                self.normMaxTarg[i] = np.amax(np.amax(self.targets[0, :, :, :], axis=1), axis=1)
                self.normMinTarg[i] = np.amin(np.amin(self.targets[0, :, :, :], axis=1), axis=1)

                for j in range(self.normMax.shape[1]):
                    self.inputs[0, j, :, :] = (self.inputs[0, j, :, :] - self.normMin[i][j]) / (
                                self.normMax[i][j] - self.normMin[i][j])

                self.targets[0] = (self.targets[0] - self.normMinTarg[i]) / (self.normMaxTarg[i] - self.normMinTarg[i])
        else:
            #if makeDimLess:

            # Implement to normalize each channel independently. Therefore, the normMax has to be stored for every channel as well.
            # As above: Normalize chunks of channels together now
            channelRangeStart, channelRangeEnd = self.generateChannelChunkRanges()


            for i in range(self.targets.shape[0]):

                for ir in range(0, len(self.channelRangeStart)):
                    self.normMax[i][ir] = np.max(np.amax(np.amax(self.inputs[i, self.channelRangeStart[ir]:self.channelRangeEnd[ir], :, :], axis=1), axis=1))
                    self.normMin[i][ir] = np.min(np.amin(np.amin(self.inputs[i, self.channelRangeStart[ir]:self.channelRangeEnd[ir], :, :], axis=1), axis=1))

                    for j in range(self.channelRangeStart[ir], self.channelRangeEnd[ir]):
                        self.inputs[i, j, :, :] = (self.inputs[i, j, :, :] - self.normMin[i][ir]) / (
                                self.normMax[i][ir] - self.normMin[i][ir])

                if self.channelTargetRangeStart == []:
                    self.normMaxTarg[i] = np.amax(np.amax(self.targets[i, :, :, :], axis=1), axis=1)
                    self.normMinTarg[i] = np.amin(np.amin(self.targets[i, :, :, :], axis=1), axis=1)
                    self.targets[i] = (self.targets[i] - self.normMinTarg[i]) / (self.normMaxTarg[i] - self.normMinTarg[i])
                else:
                    for ir in range(0, len(self.channelTargetRangeStart)):
                        self.normMaxTarg[i][ir] = np.max(np.amax(np.amax(self.targets[i, self.channelTargetRangeStart[ir]:self.channelTargetRangeEnd[ir], :, :], axis=1), axis=1))
                        self.normMinTarg[i][ir] = np.min(np.amin(np.amin(self.targets[i, self.channelTargetRangeStart[ir]:self.channelTargetRangeEnd[ir], :, :], axis=1), axis=1))
                        self.targets[i, self.channelTargetRangeStart[ir]:self.channelTargetRangeEnd[ir]] = \
                            (self.targets[i, self.channelTargetRangeStart[ir]:self.channelTargetRangeEnd[ir]]
                            - self.normMinTarg[i][ir]) / (self.normMaxTarg[i][ir] - self.normMinTarg[i][ir])



            normalizeEachChannelSeparately = False  # This is the old code. Just there in case it should be changed back.
            if normalizeEachChannelSeparately:
                if self.normMax == []:
                    self.normMax = np.empty((self.targets.shape[0] , self.inputShape[0]))
                    self.normMin = np.empty((self.targets.shape[0] , self.inputShape[0]))
                    self.normMaxTarg = np.empty((self.targets.shape[0]))
                    self.normMinTarg = np.empty((self.targets.shape[0]))

                for i in range(self.targets.shape[0]):
                    self.normMax[i] = np.amax(np.amax(self.inputs[i, :, :, :], axis=1), axis=1)
                    self.normMin[i] = np.amin(np.amin(self.inputs[i, :, :, :], axis=1), axis=1)

                    self.normMaxTarg[i] = np.amax(np.amax(self.targets[i, :, :, :], axis=1), axis=1)
                    self.normMinTarg[i] = np.amin(np.amin(self.targets[i, :, :, :], axis=1), axis=1)

                    for j in range(self.normMax.shape[1]):
                        self.inputs[i,j,:,:] = (self.inputs[i,j,:,:] - self.normMin[i][j]) / (self.normMax[i][j] - self.normMin[i][j])

                    self.targets[i] = (self.targets[i] - self.normMinTarg[i]) / (self.normMaxTarg[i] - self.normMinTarg[i])


    def getChannelRange(self):
        return self.channelRangeStart, self.channelRangeEnd, self.channelTargetRangeStart, self.channelTargetRangeEnd

    def denormalizeFrame(self, data, idxes, mode = 'inp'):
        # Modes can be 'inp' and 'tar' for target
        denorm_data = data.copy()
        if (mode == 'inp'):
            for i in range(0, data.shape[0]):
                for ir in range(len(self.channelRangeStart)):
                    for j in range(self.channelRangeStart[ir], self.channelRangeEnd[ir]):
                        denorm_data[i, j, :, :] = denorm_data[i, j, :, :] * (
                                self.normMax[idxes[i]][ir] - self.normMin[idxes[i]][ir]) + self.normMin[idxes[i]][ir]


            normalizeEachChannelSeparately = False  # This is the old code. Just there in case it should be changed back.
            if normalizeEachChannelSeparately:
                for i in range(0, data.shape[0]):
                    for j in range(denorm_data.shape[1]):
                        denorm_data[i, j, :, :] = denorm_data[i, j, :, :] * (self.normMax[idxes[i]][j] - self.normMin[idxes[i]][j]) + self.normMin[idxes[i]][j]
        elif (mode == 'tar'):
            if self.channelTargetRangeStart == []:
                for i in range(0, data.shape[0]):
                    for j in range(denorm_data.shape[1]):
                        denorm_data[i, j, :, :] = denorm_data[i, j, :, :] * (self.normMaxTarg[idxes[i]] - self.normMinTarg[idxes[i]]) + self.normMinTarg[idxes[i]]
            else:
                for i in range(0, data.shape[0]):
                    for ir in range(len(self.channelTargetRangeStart)):
                        for j in range(self.channelTargetRangeStart[ir], self.channelTargetRangeEnd[ir]):
                            denorm_data[i, j, :, :] = denorm_data[i, j, :, :] * (self.normMaxTarg[idxes[i]][ir] - self.normMinTarg[idxes[i]][ir]) + self.normMinTarg[idxes[i]][ir]


        return denorm_data

    def getRawInput(self, idxes, usedXRange, usedYRange):
        if self.readDataOnTheFly:
            currIdxes = []   # List for the indice tuples
            resultArray = []
            i = 0
            for idx in idxes:
                self.__loadFrameData(idx, False)

                currIdxs = np.ix_([0], range(0, self.inputsRaw.shape[1]), usedXRange[:, i], usedYRange[:, i])
                if i == 0:
                    resultArray = self.inputsRaw[currIdxs]
                    i += 1
                else:
                    resultArray = np.concatenate((resultArray , self.inputsRaw[currIdxs]), axis = 0)
            return resultArray
        else:
            currIdxes = []   # List for the indice tuples
            resultArray = []
            i = 0
            for idx in idxes:
                currIdxs = np.ix_([idx], range(0, self.inputsRaw.shape[1]), usedXRange[:, i], usedYRange[:, i])
                if i == 0:
                    resultArray = self.inputsRaw[currIdxs]
                    i += 1
                else:
                    resultArray = np.concatenate((resultArray , self.inputsRaw[currIdxs]), axis = 0)
            return resultArray




        #self.inputsRaw[idxes][1, :, :, :]


        #return (self.inputsRaw[idxes][:,])


    def generateFrameValidationData(self):
        # split for train / validation sets (80 / 20)
        if not self.readDataOnTheFly:
            targetLength = self.totalLength - min(int(self.totalLength * self.splitFactor),
                                                  self.maxSplitNum * self.numStamps)

            # take entries from [x, len - 1]
            self.valiInputs = self.inputs[targetLength:]
            self.valiTargets = self.targets[targetLength:]
            self.valiLength = self.totalLength - targetLength

            # take entries from [0, targetLength - 1]
            self.inputs = self.inputs[:targetLength]
            self.targets = self.targets[:targetLength]
            self.totalLength = self.inputs.shape[0]
        else: # Just take all the data which was read in and 'hope' that it was all read in for validation purposes.
            self.valiInputs = np.copy(self.inputs[:])
            self.valiTargets = np.copy(self.targets[:])
            self.valiLength = self.valiTargets.shape[0]

            # clear the arrays. They have to be read in on the fly...
            self.inputs = []
            self.targets = []



    def __preprocess(self):
        self.loadFilesFrame()
        if not self.readDataOnTheFly:
            self.__loadFrameData()

            self.__normalizeFrameData()

            self.generateFrameValidationData()
        else:
            # Only reads validation data
            self.__loadFrameData([], True)

            self.__normalizeFrameData()

            self.generateFrameValidationData()

        #sys.exit()



class ValiDatasetFrame(FrameDataset):
    def __init__(self, dataset, centerValidationPatch=True):
        #super().__init__(dataset)
        self.inputs = dataset.valiInputs
        self.inputsRaw = dataset.inputsRaw
        self.targets = dataset.valiTargets
        self.maskFromDepth = dataset.maskFromDepth
        self.cropVals = dataset.cropVals
        self.cropInputBasedOnChannel = dataset.cropInputBasedOnChannel
        self.totalLength = dataset.valiLength
        self.xResData = dataset.xResData
        self.yResData = dataset.yResData
        self.xResNet = dataset.xResNet
        self.yResNet = dataset.yResNet
        self.centerValidationPatch = centerValidationPatch
        self.damaged = False

    def __len__(self):
        return self.totalLength

    def __getitem__(self, idx, flipXNow = True, flipYNow = True):

        # The flip parameters control, whether in this specific call the variable should be flipped or not.
        # If the value is true, it is still dependent on the global settings in the beginning of this file!

        #return self.inputs[idx], self.targets[idx]
        startRandX = []
        startRandY = []

        if self.cropInputBasedOnChannel >= 0:
            # specify the range in which the random might sample.
            startRandX = round(random.uniform(self.cropVals[idx, 0], self.xResData - self.cropVals[idx, 1] - self.xResNet))
            startRandY = round(random.uniform(self.cropVals[idx, 2], self.yResData - self.cropVals[idx, 3] - self.yResNet))
        else:
            startRandX = round(random.uniform(0,self.xResData - self.xResNet))
            startRandY = round(random.uniform(0,self.yResData - self.yResNet))
        if self.centerValidationPatch:
            startRandX = math.floor((self.xResData - self.xResNet)/2)
            startRandY = math.floor((self.yResData - self.yResNet)/2)

        # If augmentXFlip is active, random between -1 and 1. Then use ::-1/::1 on the corresponding dimension
        # The same has to
        flipx = -(augmentXFlip * flipXNow * bool(random.getrandbits(1)) * 2 - 1)
        flipy = -(augmentYFlip * flipYNow * bool(random.getrandbits(1)) * 2 - 1)

        # new syntax then:  end:start:-1   vs start:end:1
        xIdxes = range(startRandX,startRandX + self.xResNet)
        yIdxes = range(startRandY,startRandY + self.yResNet)

        # Here, also a mirror / turn action could be included for further data augmentation
        matIdxInp = np.ix_([idx], range(0,self.inputs.shape[1]), xIdxes[::flipx], yIdxes[::flipy]  )
        matIdxTar = np.ix_([idx], range(0, self.targets.shape[1]), xIdxes[::flipx], yIdxes[::flipy])
        matIdxMask = np.ix_([idx], range(0, self.maskFromDepth.shape[1]), xIdxes[::flipx], yIdxes[::flipy])

        finalXRange = xIdxes[::flipx]
        finalYRange = yIdxes[::flipy]
        damaged = self.damaged
        self.damaged = False
        return self.inputs[matIdxInp].reshape(self.inputs.shape[1], len(xIdxes[::flipx]), len(yIdxes[::flipy] )), \
               self.targets[matIdxTar].reshape(self.targets.shape[1], len(xIdxes[::flipx]), len(yIdxes[::flipy] )),\
               finalXRange, finalYRange, idx, damaged, self.maskFromDepth[matIdxMask].reshape(self.maskFromDepth.shape[1], len(xIdxes[::flipx]), len(yIdxes[::flipy] ))

    def getRawInputValidation(self, idxes, usedXRange, usedYRange):
        resultArray = []
        i = 0
        for idx in idxes:
            currIdxs = np.ix_([idx], range(0, self.inputsRaw.shape[1]), usedXRange[:, i], usedYRange[:, i])
            if i == 0:
                resultArray = self.inputsRaw[currIdxs]
                i += 1
            else:
                resultArray = np.concatenate((resultArray, self.inputsRaw[currIdxs]), axis=0)
        return resultArray


class PCRDataset(FrameDataset):

    def readAddParams(self, opts):
        self.opts = opts
        self.useNormals = False
        self.useDepth = False
        if opts.useNormals:
            self.useNormals = True
        if opts.useDepth:
            self.useDepth = True

        self.useTargetNormals = False
        self.useTargetDepth = False
        self.useLearnDepthMask = False
        self.useGTMask = False

        if opts.useTargetNormals:
            self.useTargetNormals = True
        if opts.useTargetDepth:
            self.useTargetDepth = True
        if opts.useLearnDepthMask:
            self.useLearnDepthMask = True
        if opts.useGTMask:
            self.useGTMask = True


        addInpChannels = 3 * self.useNormals +  self.useDepth + self.useGTMask
        addOutChannels = 3 * self.useTargetNormals + self.useTargetDepth + self.useLearnDepthMask

        if opts.keysToLoad != []:
            addInpChannels = 0
            addOutChannels = 0
            # Count the number of in channels
            for item in opts.keysToLoad:
                addInpChannels += int(item[2] == 'inp') * item[1]

            # Count the number of tar channels (contains'tar')
            for item in opts.keysToLoad:
                addOutChannels += int(item[2] == 'tar') * item[1]

#        self.inputShape = (addInpChannels+1, self.xResData, self.yResData)
        self.inputShape = (addInpChannels, self.xResData, self.yResData)
        self.outputShape = (addOutChannels, self.xResData, self.yResData)

        assert(addInpChannels > 0 and addOutChannels > 0)



    def computeDepthLearnMask1(self, dpTar, dpIn):
        return np.less_equal( abs(dpTar - dpIn), 0.0001) *  np.not_equal(1, dpIn)

    #def computeDepthLearnedMask1(self, dpTar, dpIn):


    def copyFromHDDToArrays(self, i, iDirs, npFile):
        currChannel = 0
        currTargetChannel = 0
        self.maskFromDepth[i - iDirs][0:1] = (npFile['PointsSparseDepth'].value != 1).astype(int)
        if self.opts.keysToLoad == []:
            if self.useNormals:
                self.inputs[i - iDirs, currChannel: currChannel + 3] = npFile['normals'][0:3, :, :]
                currChannel += 3
            if self.useDepth:
                self.inputs[i - iDirs, currChannel: currChannel + 1] = npFile['depths'][0:1, :, :]  # np.gradient(self.inputs[i - iDirs, currChannel -2])
                currChannel += 1
            if self.useGTMask:
                self.inputs[i - iDirs, currChannel: currChannel + 1] = self.computeDepthLearnMask1(npFile['depthsTar'][0:1, :, :], npFile['depths'][0:1, :, :])
                currChannel += 1


            if self.useTargetNormals:
                self.targets[i - iDirs,currTargetChannel:currTargetChannel + 3] = npFile['normalsTar'][0:3, :, :]
                currTargetChannel +=3
            if self.useTargetDepth:
                self.targets[i - iDirs, currTargetChannel:currTargetChannel + 1] = npFile['depthsTar'][0:1, :, :]
                currTargetChannel += 1
            if self.useLearnDepthMask:
                #self.targets[i - iDirs, currTargetChannel:currTargetChannel + 1] = np.less_equal(0.00001, npFile['depthsTar'][0:1, :, :] - npFile['depths'][0:1, :, :]) * \
                #                                                                   npFile['depthsTar'][0:1, :, :] * np.not_equal(1, npFile['depths'][0:1, :, :])
                self.targets[i - iDirs, currTargetChannel:currTargetChannel + 1] = self.computeDepthLearnMask1(npFile['depthsTar'][0:1, :, :], npFile['depths'][0:1, :, :])
                #self.targets[i - iDirs, currTargetChannel:currTargetChannel + 1] = np.less_equal(0.00001, npFile['depthsTar'][0:1, :, :] - npFile['depths'][0:1, :, :]) * \
                #                                                                       np.not_equal(1, npFile['depths'][0:1, :, :])
                currTargetChannel += 1
        else:
            for item in self.opts.keysToLoad:
                if item[2] == 'inp':
                    if item[0] == 'useGTMask':
                        self.inputs[i - iDirs, currChannel: currChannel + 1] = self.computeDepthLearnMask1(np.moveaxis(npFile['SplatsDepth'][:, :, 0:1], -1 , 0),
                                                                                                           np.moveaxis(npFile['PointsSparseDepth'][:, :, 0:1], -1, 0))
                        currChannel += 1
                    else:
                        currFile = npFile[item[0]]
                        if len(currFile.shape) == 2:
                            currFile = np.expand_dims(currFile, 2)
                        self.inputs[i - iDirs, currChannel: currChannel + item[1]] = np.moveaxis(currFile[:, :, 0:item[1]], -1, 0)
                        currChannel += item[1]

                if item[2] == 'tar':
                    if item[0] == 'useLearnDepthMask':
                        self.targets[i - iDirs, currTargetChannel:currTargetChannel + 1]  = self.computeDepthLearnMask1(np.moveaxis(np.expand_dims(npFile['PointsDepth'],2)[:, :, 0:1], -1 , 0),
                                                                                                           np.moveaxis(np.expand_dims(npFile['PointsSparseDepth'],2)[:, :, 0:1], -1, 0))
                        currChannel += 1
                    elif item[0] == 'useLearnDepthMaskSplats':
                        self.targets[i - iDirs, currTargetChannel:currTargetChannel + 1]  = self.computeDepthLearnMask1(np.moveaxis(np.expand_dims(npFile['SplatsDepth'],2)[:, :, 0:1], -1 , 0),
                                                                                                           np.moveaxis(np.expand_dims(npFile['PointsSparseDepth'],2)[:, :, 0:1], -1, 0))
                        currChannel += 1
                    else:
                        currFile = npFile[item[0]]
                        if len(currFile.shape) == 2:
                            currFile = np.expand_dims(currFile, 2)
                        self.targets[i - iDirs, currTargetChannel: currTargetChannel + item[1]] = np.moveaxis(currFile[:, :, 0:item[1]], -1, 0)
                        currTargetChannel += item[1]

                #self.inputs[i - iDirs][-1] = self.maskFromDepth[i - iDirs]

        currLength = i - iDirs + 1
        return currLength

    def generateChannelChunkRanges(self):
        # Channels which are normalized together

        channelRangeStart = []
        channelRangeEnd = []
        currStart = 0
        currAdd = 0

        if self.opts.keysToLoad == []:
            if self.useNormals:
                currAdd += 3
            channelRangeStart.append(currStart)
            channelRangeEnd.append(currStart + currAdd)

            currStart += currAdd
            currAdd = 0
            if self.useDepth:
                currAdd += 1

            channelRangeStart.append(currStart)
            channelRangeEnd.append(currStart + currAdd)

            if self.useGTMask:
                currAdd += 1

            channelRangeStart.append(currStart)
            channelRangeEnd.append(currStart + currAdd)
        else:
            for item in self.opts.keysToLoad:
                if item[2] == 'inp' and item[3] == 'normalize':
                    currAdd = item[1]
                    channelRangeStart.append(currStart)
                    channelRangeEnd.append(currStart + currAdd)
                    currStart += currAdd

        return (channelRangeStart, channelRangeEnd)

    def generateChannelTargetChunkRanges(self):
        channelRangeStart = []
        channelRangeEnd = []
        currStart = 0
        currAdd = 0

        if self.opts.keysToLoad == []:
            if self.useTargetNormals:
                currAdd += 3
            channelRangeStart.append(currStart)
            channelRangeEnd.append(currStart + currAdd)

            currStart += currAdd
            currAdd = 0
            if self.useTargetDepth:
                currAdd += 1

            channelRangeStart.append(currStart)
            channelRangeEnd.append(currStart + currAdd)
        else:
            for item in self.opts.keysToLoad:
                if item[2] == 'tar' and item[3] == 'normalize':
                    currAdd = item[1]
                    channelRangeStart.append(currStart)
                    channelRangeEnd.append(currStart + currAdd)
                    currStart += currAdd

        return (channelRangeStart, channelRangeEnd)




if __name__ == '__main__':
    data = TurbDataset()