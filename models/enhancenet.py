# From Sebastian Weiß

import torch
import torch.nn as nn
import torch.nn.functional as F

# EnhanceNet: Single Image Super-Resolution through Automated Texture Synthesis - Sajjadi et.al.
# https://github.com/geonm/EnhanceNet-Tensorflow
class EnhanceNet(nn.Module):
    def __init__(self, upscale_factor, input_channels, output_channels, opt, inner_channels = 64, keysToLoad = []):
        '''
        Additional options:
        upsample: nearest, bilinear, or pixelShuffler
        recon_type: residual or direct
        use_bn: for batch normalization of the residual blocks
        '''
        super(EnhanceNet, self).__init__()
        assert(upscale_factor==4 or upscale_factor==1)

        self.keysToLoad = keysToLoad
        self.upscale_factor = upscale_factor
        self.upsample = opt.upsample
        self.recon_type = opt.reconType
        self.use_bn = opt.useBn

        self.specInpChannelsForRes = -1

        self._enhancenet(input_channels, output_channels, inner_channels)
        self._initialize_weights()

    def setInputChannelForResidual(self, specChannel):
        self.specInpChannelsForRes = specChannel

    def _preprocess(self, images):
        #pp_images = images / 255.0
        ## simple mean shift
        images = images * 2.0 - 1.0

        return images
    
    def _postprocess(self, images):
        pp_images = ((images + 1.0) / 2.0)# * 255.0
        
        return pp_images

    def _upsample(self, factor):
        if self.upsample == 'nearest':
            return nn.Upsample(scale_factor=factor, mode='nearest')
        elif self.upsample == 'bilinear':
            return nn.Upsample(scale_factor=factor, mode='bilinear')
        elif self.upsample == 'bicubic':
            return nn.Upsample(scale_factor=factor, mode='bicubic')
        else: #pixelShuffle
            return nn.PixelShuffle(self.factor)

    def _recon_image(self, inputs, outputs):
        '''
        LR to HR -> inputs: LR, outputs: HR
        HR to LR -> inputs: HR, outputs: LR
        '''
        resized_inputs = []
        if self.upscale_factor > 1:
            resized_inputs = F.interpolate(inputs[:,0:3,:,:],
                                       size=[outputs.shape[2], 
                                             outputs.shape[3]], 
                                       mode=self.upsample)
        else:
            resized_inputs = inputs

        if self.recon_type == 'residual':
            if self.specInpChannelsForRes == -1:
                recon_outputs = resized_inputs + outputs
            else:
                recon_outputs = resized_inputs[:,self.specInpChannelsForRes:self.specInpChannelsForRes+1] + outputs
        else:
            recon_outputs = outputs
        
        #resized_inputs = self._postprocess(resized_inputs)
        #resized_inputs = tf.cast(tf.clip_by_value(resized_inputs, 0, 255), tf.uint8)
        #tf.summary.image('4_bicubic image', resized_inputs)

        #recon_outputs = self._postprocess(recon_outputs)
        
        return recon_outputs, outputs
        
    def _enhancenet(self, input_channels, output_channels, inner_channels = 64):
        self.preblock = nn.Sequential(
            nn.Conv2d(input_channels, inner_channels, 3, padding=1),
            nn.ReLU())
            
        self.blocks = []
        for idx in range(10):
            if self.use_bn:
                self.blocks.append(nn.Sequential(
                    nn.Conv2d(inner_channels, inner_channels, 3, padding=1),
                    nn.BatchNorm2d(inner_channels),
                    nn.ReLU(),
                    nn.Conv2d(inner_channels, inner_channels, 3, padding=1),
                    nn.BatchNorm2d(inner_channels)
                    ))
            else:
                self.blocks.append(nn.Sequential(
                    nn.Conv2d(inner_channels, inner_channels, 3, padding=1),
                    nn.ReLU(),
                    nn.Conv2d(inner_channels, inner_channels, 3, padding=1),
                    ))
        self.blocks = nn.ModuleList(self.blocks)

        self.postblock = []
        if self.upscale_factor == 4:
            self.postblock = nn.Sequential(
                self._upsample(2),
                nn.Conv2d(inner_channels, inner_channels, 3, padding=1),
                nn.ReLU(),
                self._upsample(2),
                nn.Conv2d(inner_channels, inner_channels, 3, padding=1),
                nn.ReLU(),
                nn.Conv2d(inner_channels, inner_channels, 3, padding=1),
                nn.ReLU(),
                nn.Conv2d(inner_channels, output_channels, 3, padding=1),
            )
        elif self.upscale_factor == 1:
            self.postblock = nn.Sequential(

                nn.Conv2d(inner_channels, inner_channels, 3, padding=1),
                nn.ReLU(),

                nn.Conv2d(inner_channels, inner_channels, 3, padding=1),
                nn.ReLU(),
                nn.Conv2d(inner_channels, inner_channels, 3, padding=1),
                nn.ReLU(),
                nn.Conv2d(inner_channels, output_channels, 3, padding=1),
            )

    def _initialize_weights(self):
        def init(m):
            classname = m.__class__.__name__
            if classname.find('Conv') != -1:
                torch.nn.init.orthogonal_(m.weight, torch.nn.init.calculate_gain('relu'))
        for block in self.blocks:
            block.apply(init)      

    def forward(self, inputs):
        #inputs = self._preprocess(inputs)

        features = self.preblock(inputs)
        for block in self.blocks:
            features = features + block(features)
        outputs = self.postblock(features)

        outputs, residual = self._recon_image(inputs, outputs)
        outputs = torch.clamp(outputs, 0, 1)
        return outputs, residual
