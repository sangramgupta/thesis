
import os, sys, random
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torch.optim as optim
import matplotlib.pyplot as plt

from DfpNet import TurbNetG, weights_init
from VecLearnNet import TurbNetGVec
from VecLearnNet import TurbNetGVecPoly
import dataset
import utils
from tensorboardX import SummaryWriter


# mode can be 'stamped vec', 'stamped scalar', 'noise poly',....
# only 'noise poly' and 'stamped vec' are implemented so far...
modeForTraining = 'stamped vec'# 'noise poly'
#if modeForTraining == 'noise poly':


# data set config
prop=None # by default, use all from "../data/train"

#### Stamp out 2D Vector dataset ####
locationOfUnstampedData = 'data/10mWind/'
locationOfUnstampedTestData = 'data/10mWindTest/'
locationStampedOut = 'stampData/10mWind_kernel_15x15/'
locationStampedOutTest = 'stampData/10mWind_kernel_15x15_test/'

# Stamp out train
if False:
    data = dataset.TurbDataset(prop,
                           shuffle=1,
                           dataDir = locationOfUnstampedData,
                           outputFolderStamps = locationStampedOut,
                           bPolynom = False,
                           use_prestamped_data = False,
                           stampSize = 15,
                           numStamps = 10
                           )
if True:
    data = dataset.TurbDataset(prop,
                               shuffle=1,
                               dataDir=locationOfUnstampedTestData,
                               outputFolderStamps=locationStampedOutTest,
                               bPolynom=False,
                               use_prestamped_data=False,
                               stampSize=15,
                               numStamps=10
                               )